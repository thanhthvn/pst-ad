package com.cncsoftgroup.panstage.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cncsoftgroup.panstage.activity.JoinLyricMediaActivity;
import com.cncsoftgroup.panstage.activity.JoinRecordActivity;
import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnCreateSessionResult;
import com.cncsoftgroup.panstage.manager.SessionManager.OnLyricSession;
import com.nmd.libs.DebugLog;

public class LyricView extends FrameLayout implements OnClickListener {
	Context context;
	
	TextView bt_NotNow, bt_AddLyric;
	EditText addLyric, addTitle;
	String authu_token;
	
	public LyricView(Context context) {
		super(context);
		this.context = context;
		initView();
	}
	
	@SuppressLint("InflateParams")
	private void initView(){
		View rootView = LayoutInflater.from(context).inflate(R.layout.add_lyric, null);
        addView(rootView);	
        
        authu_token = SharedPreference.loadAuthu_token(context);
		bt_NotNow = (TextView) findViewById(R.id.bt_NotNow_AddLyric);
		bt_AddLyric = (TextView) findViewById(R.id.bt_AddLyric_Add);

		bt_NotNow.setOnClickListener(this);
		bt_AddLyric.setOnClickListener(this);

		addTitle = (EditText) findViewById(R.id.tv_AddLyric_Title);
		addLyric = (EditText) findViewById(R.id.tv_AddLyric_lyric);
		
		if(CacheData.getInstant().is_update_session()){
			addTitle.setText(CacheData.getInstant().getLyrics().getLyric_title());
			addLyric.setText(CacheData.getInstant().getLyrics().getLyric());
		}
	}
	
	void back(){
		if(CacheData.getInstant().isJoinLyrics()){
			((JoinLyricMediaActivity)context).onBackPressed();
		}else{
			((JoinRecordActivity)context).onBackPressed();
		}
	}
	
	Dialog dialog = null;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bt_NotNow_AddLyric :
				//TODO
				back();
				break;
			case R.id.bt_AddLyric_Add :

				if (!Utils.isNullOrEmpty(addTitle.getText().toString())) {
					if (!Utils.isNullOrEmpty(addLyric.getText().toString())) {
						dialog = new Dialog(context);
						Utils.showDialog2(context, dialog, "", context.getString(R.string.publish_lyrics), "Yes", "", new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								dialog.dismiss();
								if (CacheData.getInstant().is_update_session()) {
									Utils.showProgressDialog(context, "Update Lyric.........");
									new SessionManager().updateLyricRecord(authu_token, Integer.toString(CacheData.getInstant().getSid_update()), addTitle.getText().toString(), addLyric.getText().toString(), new OnLyricSession() {
										
										@Override
										public void OnUpdateLyricMethod(boolean success) {
											Utils.dismissCurrentDialog();
											if (success) {
												//TODO
												back();
											} else {
												Utils.dismissCurrentDialog();
												Utils.showToast(context, "Update Fail!");
											}
										}
									});
									
								} else {
									Utils.showProgressDialog(context, "Upload Lyric.........");
									new SessionManager().createSession(authu_token, addTitle.getText().toString(), 0, "", addTitle.getText().toString(), addLyric.getText().toString(), new OnCreateSessionResult() {
										
										@Override
										public void onCreateSessionMethod(boolean isSuccess, int song_id) {
											Utils.dismissCurrentDialog();
											if (isSuccess) {
												DebugLog.logd("Success!");
												//TODO
												back();
											} else {
												DebugLog.loge("Error!");
												Utils.showToast(context, "Upload Lyric Failed!");
											}
										}
									});
								}
								
							}
						}, new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						
					} else {
						Utils.showErrorNullOrEmpty(addLyric, "Lyric is null!");
					}
				} else {
					if (Utils.isNullOrEmpty(addLyric.getText().toString())) {
						Utils.showErrorNullOrEmpty(addLyric, "Lyric is null!");
						Utils.showErrorNullOrEmpty(addTitle, "Lyric is null!");
					} else {
						Utils.showErrorNullOrEmpty(addTitle, "Lyric is null!");
					}
				}

				break;
		}
	}
}
