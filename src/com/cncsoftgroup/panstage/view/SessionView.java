package com.cncsoftgroup.panstage.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.cncsoftgroup.panstage.activity.AboutActivity;
import com.cncsoftgroup.panstage.activity.AddLyricActivity;
import com.cncsoftgroup.panstage.activity.DownloadRecordFromSessionActivity;
import com.cncsoftgroup.panstage.activity.HomePageActivity;
import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.activity.SessionPagerActivity;
import com.cncsoftgroup.panstage.activity.UserPageActivity;
import com.cncsoftgroup.panstage.adapter.ListJoinSessionAdapter;
import com.cncsoftgroup.panstage.bean.Lyrics;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.bean.SessionDetail;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnJoinSessionResult;
import com.cncsoftgroup.panstage.manager.SessionManager.OnUserLikeSession;
import com.nmd.libs.DebugLog;
import com.nmd.libs.UtilLibs;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

@SuppressWarnings("unused")
public class SessionView extends FrameLayout implements OnClickListener, OnSeekBarChangeListener, OnCompletionListener, OnBufferingUpdateListener, OnPreparedListener {
	Context context;
	public int position;
	public LinearLayout profile, load, content;

	public TextView tv_song_name, tv_full_name, tv_role, tv_description, tv_lyric, title_lyric_song, bt_about_homeCreator, tv_speed_load;
	public TextView tv_Duration_Playing, tv_Duration_Finish;
	public ImageView img_avatar, icon;
	public ImageView img_Like_Session, img_Like_Session_, img_Share_Session, img_Add_Lyric, img_Lock_Session, img_Join_Session, bt_about_info;
	public ImageView img_Play_Pause, img_BackwardTime, img_ForwardTime;
	public View avatar_view, media_play;
	public SeekBar seekbarPrgress, seekbar_Volume;

	ImageTagFactory imageTagFactory = ImageTagFactory.newInstance();
	ImageManager imageManager = AppController.getImageManager();
	int THUMB_IMAGE_SIZE = 160;

	public static boolean isLocked, isCheckLock, isLike;
	public int sid, pageIntent;
	public int creator_user_id;
	public int play = 0;
	public boolean isStart = false;
	public int duration;
	public int seekForwardTime = 3000;
	public int seekBackwardTime = 3000;
	// private int sbPercent = 0;
	private final static int MAX_VOLUME = 100;
	public String url_img, song_name, lyric_title, lyric, full_name, role, role_description, media_url, urlAPI;
	public ListView list_Join_Session;
	public ArrayList<Session> arrSesion;
	public ListJoinSessionAdapter list_join_adapter;
	ProgressBar proges_get_join_session;
	public Boolean iscanjoin = true;
	public Boolean isjoinfull = false;
	public Boolean isyoursession = true;
	public String authu_token;
	public int uid = 0;
	public Session obj;
	public View view;

	public String fullname_join, description, avata_join, role_join;
	public int sid_join;

	public SessionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public void initView() {
		authu_token = SharedPreference.loadAuthu_token(context);
		uid = SharedPreference.loadUid(context);
		String a = full_name;

		getControl();
		setDataForView();
		getEventForControl();
		getListJoinSession();
	}

	public void getControl() {
		tv_full_name = (TextView) view.findViewById(R.id.tv_Fullname);
		tv_song_name = (TextView) view.findViewById(R.id.tv_song_name);
		tv_role = (TextView) view.findViewById(R.id.tv_Role);
		title_lyric_song = (TextView) view.findViewById(R.id.title_lyric_song);
		tv_description = (TextView) view.findViewById(R.id.tv_Description);
		tv_lyric = (TextView) view.findViewById(R.id.tv_Lyric);
		img_Lock_Session = (ImageView) view.findViewById(R.id.img_Lock_Session);
		img_Like_Session = (ImageView) view.findViewById(R.id.img_Like_Session);
		img_Like_Session_ = (ImageView) view.findViewById(R.id.img_Like_Session_);
		img_Share_Session = (ImageView) view.findViewById(R.id.img_Share_Session);
		img_Add_Lyric = (ImageView) view.findViewById(R.id.img_Add_Lyric);
		img_Join_Session = (ImageView) view.findViewById(R.id.img_Join_Session);
		bt_about_homeCreator = (TextView) view.findViewById(R.id.bt_about_homeCreator);
		bt_about_info = (ImageView) view.findViewById(R.id.bt_about_info);

		load = (LinearLayout) view.findViewById(R.id.load);
		content = (LinearLayout) view.findViewById(R.id.content);

		load.setVisibility(View.VISIBLE);
		content.setVisibility(View.GONE);

		proges_get_join_session = (ProgressBar) view.findViewById(R.id.proges_get_join_session);

		tv_description.setSelected(true);
		tv_song_name.setSelected(true);

		avatar_view = (View) view.findViewById(R.id.imgUserSessionPage);
		img_avatar = (ImageView) avatar_view.findViewById(R.id.im_Home_Avata_List);
		icon = (ImageView) avatar_view.findViewById(R.id.icon_type);

		media_play = (View) view.findViewById(R.id.play_Media);
		tv_Duration_Finish = (TextView) media_play.findViewById(R.id.tv_Duration_Finish);
		tv_Duration_Playing = (TextView) media_play.findViewById(R.id.tv_Duration_Playing);
		img_Play_Pause = (ImageView) media_play.findViewById(R.id.img_Play_Pause);
		tv_speed_load = (TextView) media_play.findViewById(R.id.tv_speed_load);
		img_BackwardTime = (ImageView) media_play.findViewById(R.id.img_BackwardTime);
		img_ForwardTime = (ImageView) media_play.findViewById(R.id.img_ForwardTime);
		seekbarPrgress = (SeekBar) media_play.findViewById(R.id.seekbar_Progress);
		seekbar_Volume = (SeekBar) media_play.findViewById(R.id.seekbar_Volume);
		profile = (LinearLayout) view.findViewById(R.id.profile);
	}

	public void setDataForView() {
		imageTagFactory.setWidth(THUMB_IMAGE_SIZE);
		imageTagFactory.setHeight(THUMB_IMAGE_SIZE);
		imageTagFactory.setDefaultImageResId(R.drawable.no_ava);
		imageTagFactory.setErrorImageId(R.drawable.no_ava);
		imageTagFactory.setSaveThumbnail(true);
		imageTagFactory.usePreviewImage(THUMB_IMAGE_SIZE, THUMB_IMAGE_SIZE, true);
		img_avatar.setTag(imageTagFactory.build(url_img, context));
		imageManager.getLoader().load(img_avatar);

		tv_full_name.setText(full_name);

		if (role.equals("Writer")) {
			tv_song_name.setText(song_name);
		} else {
			tv_song_name.setText(song_name);
		}

		if (role.equals("Writer")) {
			tv_role.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_page_icon_writer_text, 0, 0, 0);
			tv_role.setText(role);
//			icon.setImageResource(R.drawable.icon_writer);
		} else if (role.equals("Singer")) {
			tv_role.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_page_icon_mic_text, 0, 0, 0);
			tv_role.setText(role);
//			icon.setImageResource(R.drawable.icon_singer);
		} else if (role.equals("Player")) {
			tv_role.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_page_icon_mic_text, 0, 0, 0);
			tv_role.setText(role);
//			icon.setImageResource(R.drawable.icon_player);
		}
		
		if (role_description.equals("null")) {
			tv_description.setText("");
		} else {
			tv_description.setText(role_description);
		}

		if (isLocked) {
			img_Lock_Session.setImageResource(R.drawable.pan_stage_locked_);
		} else {
			img_Lock_Session.setImageResource(R.drawable.pan_stage_unlock_);
		}
		
		if(isLike){
			img_Like_Session.setVisibility(View.GONE);
			img_Like_Session_.setVisibility(View.VISIBLE);
		}else{
			img_Like_Session_.setVisibility(View.GONE);
			img_Like_Session.setVisibility(View.VISIBLE);
		}

		if (creator_user_id == uid) {
			return;
		} else {
			img_Lock_Session.setAlpha((float) 0.3);
		}

		DebugLog.loge("duration ---------->" + duration);
		tv_Duration_Finish.setText("" + Utils.milliSecondsToTimer(duration * 1000));

	}

	private void checkJoin() {
		if (uid != ((SessionPagerActivity) context).getCurrentPage().creator_user_id) {
			isyoursession = false;
			((SessionPagerActivity) context).getCurrentPage().img_Lock_Session.setVisibility(View.GONE);
			CacheData.getInstant().setIs_update_session(false);
		} else {
			CacheData.getInstant().setIs_update_session(true);
		}
		if (arrSesion != null && arrSesion.size() >= 7) {
			isjoinfull = true;
		}
		if (arrSesion != null && arrSesion.size() > 0) {
			for (int i = 0; i < arrSesion.size(); i++) {
				if (uid == arrSesion.get(i).getUid()) {
					iscanjoin = false;
					break;
				}
			}
		}
	}

	protected void getListJoinSession() {

		list_Join_Session = (ListView) findViewById(R.id.list_Join_Session);
		new SessionManager().getJoinSessionDetails(authu_token, sid, new OnJoinSessionResult() {

			@Override
			public void OnJoinSessionResultMethod(boolean isSuccess, ArrayList<Session> arraylist, SessionDetail obj) {
				arrSesion = new ArrayList<Session>();
				if (isSuccess) {
					if (obj.getLyric().equals("null")) {
						tv_lyric.setText("");
					} else {
						tv_lyric.setText(obj.getLyric());
					}
					if (obj.getLyric_title().equals("null")) {
						title_lyric_song.setText("");
					} else {
						title_lyric_song.setText(obj.getLyric_title());
					}

					arrSesion = arraylist;

					list_join_adapter = new ListJoinSessionAdapter(context, R.layout.list_join_session_item, arrSesion);
					updateListViewHeight(list_join_adapter, list_Join_Session);
					list_Join_Session.setAdapter(list_join_adapter);
				} else {
					Utils.showToast(context, "Get List Join Session Fail!");
				}
				proges_get_join_session.setVisibility(View.GONE);
				checkJoin();

				((SessionPagerActivity) context).getCurrentPage().load.setVisibility(View.GONE);
				((SessionPagerActivity) context).getCurrentPage().content.setVisibility(View.VISIBLE);
			}
		});

		list_Join_Session.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (SharedPreference.loadRoleRegister(context).equals(context.getString(R.string.listener))) {
					Utils.showToast(context, "Please sign up!");
					return;
				}
				if (arg2 < arrSesion.size()) {
					obj = arrSesion.get(arg2);
					fullname_join = obj.getFull_name();
					sid_join = obj.getUid();
					avata_join = obj.getAvatar_url();
					description = obj.getRole_description();
					role_join = obj.getRole();
					CacheData.getInstant().setUserPage(true);
					CacheData.getInstant().setDuration(((SessionPagerActivity) context).getCurrentPage().duration);
					Intent intent = new Intent(context, UserPageActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("sid", sid_join);
					bundle.putString("full_name", fullname_join);
					bundle.putString("avatar", avata_join);
					bundle.putString("description", description);
					bundle.putString("role", role_join);
					intent.putExtra("session_page", bundle);
					((SessionPagerActivity) context).startActivity(intent);
					((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
				}
			}
		});

	}

	public void getEventForControl() {
		img_Like_Session.setOnClickListener(this);
		img_Share_Session.setOnClickListener(this);
		img_Add_Lyric.setOnClickListener(this);
		img_Lock_Session.setOnClickListener(this);
		img_Join_Session.setOnClickListener(this);
		img_Play_Pause.setOnClickListener(this);
		img_BackwardTime.setOnClickListener(this);
		img_ForwardTime.setOnClickListener(this);
		seekbarPrgress.setMax(100);
		seekbar_Volume.setMax(MAX_VOLUME);
		seekbar_Volume.setProgress(MAX_VOLUME);
		seekbar_Volume.setOnSeekBarChangeListener(this);
		seekbarPrgress.setOnSeekBarChangeListener(this);
		bt_about_homeCreator.setOnClickListener(this);
		bt_about_info.setOnClickListener(this);
		profile.setOnClickListener(this);
		profile.setClickable(true);
	}

	private void updateListViewHeight(ListJoinSessionAdapter myListAdapter, ListView myListView) {
		if (myListAdapter == null || myListView == null) {
			return;
		}

		int totalHeight = 0;
		int adapterCount = myListAdapter.getCount();
		for (int size = 0; size < adapterCount; size++) {
			View listItem = myListAdapter.getView(size, null, myListView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = myListView.getLayoutParams();
		params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
		myListView.setLayoutParams(params);
	}

	@SuppressWarnings("static-access")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.profile:
			if (SharedPreference.loadRoleRegister(context).equals(context.getString(R.string.listener))) {
				// Utils.showLongToast(context, "Please sign up!");
				return;
			}
			CacheData.getInstant().setUserPage(true);
			Intent intent = new Intent(context, UserPageActivity.class);
			Bundle bundle = new Bundle();
			bundle.putInt("sid", sid);
			bundle.putInt("uid", creator_user_id);
			bundle.putString("full_name", full_name);
			bundle.putString("avatar", url_img);
			bundle.putString("description", role_description);
			bundle.putString("role", role);
			bundle.putInt("notMe", 1);
			intent.putExtra("session_page", bundle);
			((SessionPagerActivity) context).startActivity(intent);
			((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			break;
		case R.id.img_Like_Session:
			if (SharedPreference.loadRoleRegister(context).equals(context.getString(R.string.listener))) {
				Utils.showToast(context, "Please sign up!");
				return;
			}
			Utils.showProgressDialog(context, context.getString(R.string.loading));
			like_Session();
			break;
		case R.id.img_Share_Session:
			shareFacebook(((SessionPagerActivity) context).getCurrentPage().sid);
			break;
		case R.id.img_Add_Lyric:
//			if (SharedPreference.loadRoleRegister(context).equals(context.getString(R.string.listener))) {
//				Utils.showToast(context, "Please sign up!");
//				return;
//			}
//			if (CacheData.getInstant().is_update_session()) {
//				CacheData.getInstant().setLyrics(new Lyrics("" + sid, ((SessionPagerActivity) context).getCurrentPage().title_lyric_song.getText().toString(), ((SessionPagerActivity) context).getCurrentPage().tv_lyric.getText().toString()));
//				((SessionPagerActivity) context).startActivityForResult(new Intent(context, AddLyricActivity.class), 1001);
//				((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
//			} else {
				Utils.showToast(context, "Coming soon");
//			}
			break;
		case R.id.img_Lock_Session:
			lock_Session();
			break;
		case R.id.img_Join_Session:
//			Utils.showToast(context, ""+((SessionPagerActivity) context).getCurrentPage().song_name);
			if (SharedPreference.loadRoleRegister(context).equals(context.getString(R.string.listener))) {
				Utils.showToast(context, "Please sign up!");
				return;
			}
//			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//			if (currentapiVersion >= 21) {
//				Utils.showToast(context, "Current version does not support Android Lollipop!");
//				 return;
//			}
			CacheData.getInstant().setLyrics(new Lyrics("" + sid, ((SessionPagerActivity) context).getCurrentPage().title_lyric_song.getText().toString(), ((SessionPagerActivity) context).getCurrentPage().tv_lyric.getText().toString()));
			joinSession();
			break;
		case R.id.img_Play_Pause:
			DebugLog.loge("img_Play_Pause click");
			if (((SessionPagerActivity) context).getCurrentPage().isLoadDone) {
				if (((SessionPagerActivity) context).media.isPlaying()) {
					((SessionPagerActivity) context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
					((SessionPagerActivity) context).media.pause();
					((SessionPagerActivity) context).handler.removeCallbacks(mUpdateTimeTask);
				} else {
					// if(((SessionPagerActivity)context).getCurrentPage().isDownloadDone
					// &&
					// ((SessionPagerActivity)context).getCurrentPage().isPlayDone){
					// ((SessionPagerActivity)context).getCurrentPage().isDownloadDone
					// = false;
					// playSong(CacheData.getInstant().getCurrSid());
					// }else{
					((SessionPagerActivity) context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.bt_pause);
					((SessionPagerActivity) context).media.start();
					((SessionPagerActivity) context).handler.postDelayed(mUpdateTimeTask, 100);
					// }
				}
			} else {
				Utils.showToast(context, context.getString(R.string.loading));
			}
			break;
		case R.id.img_ForwardTime:
			int pos = position + 1;
			((SessionPagerActivity) context).pager.setCurrentItem(pos, true);
			// int currentPosition =
			// ((SessionPagerActivity)context).media.getCurrentPosition();
			// if(currentPosition + seekForwardTime <=
			// ((SessionPagerActivity)context).media.getDuration()){
			// ((SessionPagerActivity)context).media.seekTo(currentPosition +
			// seekForwardTime);
			// }else{
			// ((SessionPagerActivity)context).media.seekTo(((SessionPagerActivity)context).media.getDuration());
			// }

			break;
		case R.id.img_BackwardTime:
			int pos2 = position - 1;
			((SessionPagerActivity) context).pager.setCurrentItem(pos2, true);
			// int currentPosition2 =
			// ((SessionPagerActivity)context).media.getCurrentPosition();
			// if(currentPosition2 - seekBackwardTime >= 0){
			// ((SessionPagerActivity)context).media.seekTo(currentPosition2 -
			// seekBackwardTime);
			// }else{
			// ((SessionPagerActivity)context).media.seekTo(0);
			// }

			break;
		case R.id.bt_about_homeCreator:
			CacheData.getInstant().setAboutView(true);
			((SessionPagerActivity) context).startActivity(new Intent(context, AboutActivity.class));
			((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			break;

		}
	}

	private void joinSession() {
		Utils.showProgressDialog(context, context.getString(R.string.loading));
		Thread join = new Thread(new Runnable() {

			@Override
			public void run() {
				new SessionManager().getJoinSessionDetails(authu_token, sid, new OnJoinSessionResult() {

					@Override
					public void OnJoinSessionResultMethod(boolean isSuccess, ArrayList<Session> arraylist, SessionDetail obj) {
						if (isSuccess) {
							isCheckLock = obj.getLocked();
							if (isCheckLock) {
								img_Lock_Session.setImageResource(R.drawable.pan_stage_locked_);
								Utils.dismissCurrentDialog();
								Utils.showToast(context, "Session locked");
							} else if (!isyoursession) {
								if (iscanjoin) {
									if (isjoinfull) {
										Utils.dismissCurrentDialog();
										Utils.showToast(context, "Session has full!");
									} else {
										Utils.dismissCurrentDialog();

										// if(((SessionPagerActivity)context).getCurrentPage().downloadTask!=null
										// &&
										// !((SessionPagerActivity)context).getCurrentPage().isPlay){
										// DebugLog.loge("downloadTask cancel");
										// ((SessionPagerActivity)context).getCurrentPage().downloadTask.cancel(true);
										// }

										Intent intent = new Intent(context, DownloadRecordFromSessionActivity.class);
										Bundle bundle = new Bundle();
										bundle.putString("song_name", song_name);
										bundle.putInt("sid", sid);
										bundle.putString("media_url", media_url);
										intent.putExtra("Url_media", bundle);
										((SessionPagerActivity) context).startActivityForResult(intent, 1000);
										((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
										((SessionPagerActivity) context).finish();
									}
								} else {
									Utils.dismissCurrentDialog();
									Utils.showToast(context, "you already join this session!");
								}
							} else {
								Utils.dismissCurrentDialog();
								Intent intent = new Intent(context, DownloadRecordFromSessionActivity.class);
								Bundle bundle = new Bundle();
								bundle.putString("song_name", song_name);
								bundle.putInt("sid", sid);
								bundle.putString("media_url", media_url);
								intent.putExtra("Url_media", bundle);
								((SessionPagerActivity) context).startActivityForResult(intent, 1000);
								((SessionPagerActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
								((SessionPagerActivity) context).finish();
							}
						}
					}
				});
			}
		});
		join.start();
	}

	public void resume() {
		DebugLog.loge("onResume");
		getListJoinSession();
	}

	public void like_Session() {
		new SessionManager().Like_Join_Lock_Session(authu_token, sid, Url.API_LIKE_SESSION, new OnUserLikeSession() {

			@Override
			public void OnUserLikeSessionMethod(boolean success) {
				Utils.dismissCurrentDialog();
				if (success) {
					((SessionPagerActivity) context).getCurrentPage().isLike = true;
					Utils.showToast(context, "Like session successfully !");
					img_Like_Session.setVisibility(View.GONE);
					img_Like_Session_.setVisibility(View.VISIBLE);
				} else {
					DebugLog.loge("Like session failed!");
					// Utils.showToast(context, "Like session failed !");
				}
			}
		});
	}

	public void lock_Session() {
		if (creator_user_id == uid) {
			if (isLocked) {
				Utils.showProgressDialog(context, context.getString(R.string.loading));
				new SessionManager().Like_Join_Lock_Session(authu_token, sid, Url.API_UNLOCK_SESSION, new OnUserLikeSession() {

					@Override
					public void OnUserLikeSessionMethod(boolean success) {
						Utils.dismissCurrentDialog();
						if (success) {
							Utils.showToast(context, "UnLock Session Success !");
							((SessionPagerActivity) context).getCurrentPage().img_Lock_Session.setImageResource(R.drawable.pan_stage_unlock_);
						} else {
							Utils.showToast(context, "UnLock Session Failed !");
						}
					}
				});
				isLocked = false;
			} else {
				Utils.showProgressDialog(context, context.getString(R.string.loading));
				new SessionManager().Like_Join_Lock_Session(authu_token, sid, Url.API_LOCK_SESSION, new OnUserLikeSession() {

					@Override
					public void OnUserLikeSessionMethod(boolean success) {
						Utils.dismissCurrentDialog();
						if (success) {
							Utils.showToast(context, "Lock Session Success !");
							((SessionPagerActivity) context).getCurrentPage().img_Lock_Session.setImageResource(R.drawable.pan_stage_locked_);
						} else {
							Utils.showToast(context, "Lock Session Failed !");
						}
					}
				});
				isLocked = true;
			}
		} else {
			Utils.showToast(context, "You haven't permission for the Session of this !");
		}
	}

	public void shareFacebook(int songID) {
		DebugLog.loge("shareid  : " + songID);
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, "http://s.pan-stage.com/songs/" + songID);

		PackageManager packManager = ((SessionPagerActivity) context).getPackageManager();
		List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		boolean resolved = false;
		for (ResolveInfo resolveInfo : resolvedInfoList) {
			if (resolveInfo.activityInfo.packageName.startsWith("com.facebook.katana")) {
				intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
				resolved = true;
				break;
			}
		}
		if (resolved) {
			((SessionPagerActivity) context).startActivity(intent);
		} else {
			Utils.showToast(context, "You need to install a Facebook application to use this function");
		}
	}

	// boolean isDownloading = false;

	// public DownloadTask downloadTask;
	// public void download(String url, int sid){
	// CacheData.getInstant().setIs_Done(false);
	// isPlayDone = false;
	// isDownloadDone = false;
	// DebugLog.loge("DownloadTask: "+url+"\nsid: " + sid);
	// downloadTask = new DownloadTask(context, sid);
	// downloadTask.execute(url);
	//
	// }

	public void playSong(int pos, int sid, String songname, String media_url) {
		// Utils.showProgressDialog(context,
		// context.getString(R.string.loading);

		DebugLog.loge("\npos:" + pos + "\nsid: " + sid + "\nsongname:" + songname + "\nmedia_url:" + media_url);

		((SessionPagerActivity) context).media = new MediaPlayer();
		((SessionPagerActivity) context).media.reset();
		((SessionPagerActivity) context).media.setAudioStreamType(AudioManager.STREAM_MUSIC);
		try {
			((SessionPagerActivity) context).media.setDataSource(media_url);
			((SessionPagerActivity) context).media.setOnPreparedListener(this);
			((SessionPagerActivity) context).media.setOnBufferingUpdateListener(this);
			// ((SessionPagerActivity)context).media.prepare();
			((SessionPagerActivity) context).media.prepareAsync();
			((SessionPagerActivity) context).media.setOnCompletionListener(this);
			seekbarPrgress.setProgress(0);
			seekbarPrgress.setMax(100);
			tv_speed_load.setText("Loading...");
			DebugLog.loge("key media: " + ((SessionPagerActivity) context).media.getAudioSessionId());
			CacheData.getInstant().setAudio_session(((SessionPagerActivity) context).media.getAudioSessionId());
			CacheData.getInstant().setLoading(true);
			updateProgressBar();
		} catch (IllegalArgumentException e) {
			DebugLog.loge(e.getMessage());
			e.printStackTrace();
		} catch (SecurityException e) {
			DebugLog.loge(e.getMessage());
			e.printStackTrace();
		} catch (IllegalStateException e) {
			DebugLog.loge(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			DebugLog.loge(e.getMessage());
			e.printStackTrace();
		}
	}

	public boolean isPlay = false;

	// public void playSong(int currentSid){
	// // DebugLog.loge("\npos:" + pos + "\nsid: " + sid + "\nsongname:" +
	// songname + "\nmedia_url:" + media_url);
	// if(((SessionPagerActivity)context).media.isPlaying()){
	// ((SessionPagerActivity)context).media.stop();
	// }
	// if(!CacheData.getInstant().isSessionDetail()){
	// return;
	// }
	// ((SessionPagerActivity)context).media.reset();
	// ((SessionPagerActivity)context).media.setAudioStreamType(AudioManager.STREAM_MUSIC);
	// try {
	// ((SessionPagerActivity)context).media.setOnCompletionListener(this);
	// ((SessionPagerActivity)context).media.setDataSource(Utils.path_Download+"/"+currentSid+".mp3");
	// ((SessionPagerActivity)context).media.prepare();
	// ((SessionPagerActivity)context).media.start();
	// DebugLog.loge("playSong: "+sid);
	// ((SessionPagerActivity)context).getCurrentPage().isPlay = true;
	// ((SessionPagerActivity)context).getCurrentPage().isLoadDone = true;
	// ((SessionPagerActivity)context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.bt_pause);
	// ((SessionPagerActivity)context).getCurrentPage().seekbarPrgress.setProgress(0);
	// ((SessionPagerActivity)context).getCurrentPage().seekbarPrgress.setMax(100);
	// updateProgressBar();
	//
	// DebugLog.loge("key media: " +
	// ((SessionPagerActivity)context).media.getAudioSessionId());
	// CacheData.getInstant().setAudio_session(((SessionPagerActivity)context).media.getAudioSessionId());
	// CacheData.getInstant().setLoading(true);
	// } catch (IllegalArgumentException e) {
	// DebugLog.loge(e.getMessage());
	// e.printStackTrace();
	// } catch (SecurityException e) {
	// DebugLog.loge(e.getMessage());
	// e.printStackTrace();
	// } catch (IllegalStateException e) {
	// DebugLog.loge(e.getMessage());
	// e.printStackTrace();
	// } catch (IOException e) {
	// DebugLog.loge(e.getMessage());
	// e.printStackTrace();
	// }
	// }

	public Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			if (((SessionPagerActivity) context).media != null && ((SessionPagerActivity) context).getCurrentPage().isBufferApart) {
				if (((SessionPagerActivity) context).media.isPlaying()) {
					long totalDuration = ((SessionPagerActivity) context).media.getDuration();
					long currentDuration = ((SessionPagerActivity) context).media.getCurrentPosition();

					((SessionPagerActivity) context).getCurrentPage().tv_Duration_Finish.setText("" + Utils.milliSecondsToTimer(totalDuration));
					((SessionPagerActivity) context).getCurrentPage().tv_Duration_Playing.setText("" + Utils.milliSecondsToTimer(currentDuration));

					int progress = (int) (Utils.getProgressPercentage(currentDuration, totalDuration));
					((SessionPagerActivity) context).getCurrentPage().seekbarPrgress.setProgress(progress);

					((SessionPagerActivity) context).handler.postDelayed(this, 100);
				} else {
					((SessionPagerActivity) context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
					// ((SessionPagerActivity)context).getCurrentPage().tv_Duration_Finish.setText(""+Utils.milliSecondsToTimer(0));
				}
			}
		}
	};

	public boolean isLoadDone = false;

	@Override
	public void onPrepared(final MediaPlayer mp) {
		DebugLog.loge("key media: " + mp.getAudioSessionId());
		if (CacheData.getInstant().getAudio_session() == mp.getAudioSessionId()) {
			if (CacheData.getInstant().isLoading()) {
				CacheData.getInstant().setLoading(false);
				if (!mp.isPlaying()) {
					mp.start();
					((SessionPagerActivity) context).getCurrentPage().isBufferApart = true;
					((SessionPagerActivity) context).getCurrentPage().isLoadDone = true;
					((SessionPagerActivity) context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.bt_pause);
					((SessionPagerActivity) context).getCurrentPage().seekbarPrgress.setProgress(0);
					((SessionPagerActivity) context).getCurrentPage().seekbarPrgress.setMax(100);
					updateProgressBar();
				}
			}
		}
	}

	// int percent= 0;
	public boolean isBufferApart = false;

	@Override
	public void onBufferingUpdate(MediaPlayer mp, final int percent) {
		// DebugLog.loge("key media: " + mp.getAudioSessionId());
		if (CacheData.getInstant().getAudio_session() == mp.getAudioSessionId()) {
			((SessionPagerActivity) context).getCurrentPage().tv_speed_load.setText("Received : " + percent + "%");
			((SessionPagerActivity) context).getCurrentPage().seekbarPrgress.setSecondaryProgress(percent);
			updateProgressBar();
		}
	}

	public void updateProgressBar() {
		((SessionPagerActivity) context).handler.postDelayed(mUpdateTimeTask, 100);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		((SessionPagerActivity) context).handler.removeCallbacks(mUpdateTimeTask);
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if (media_url.equals("null")) {
			return;
		}
		if (seekBar == seekbarPrgress) {
			((SessionPagerActivity) context).handler.removeCallbacks(mUpdateTimeTask);
			int totalDuration = ((SessionPagerActivity) context).media.getDuration();
			int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);
			((SessionPagerActivity) context).media.seekTo(currentPosition);
			updateProgressBar();
		} else if (seekBar == seekbar_Volume) {
			int soundVolume = seekbar_Volume.getProgress();
			float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));
			((SessionPagerActivity) context).media.setVolume(volume, volume);
		}
	}

	boolean isPlayDone = false;

	@Override
	public void onCompletion(MediaPlayer arg0) {
		((SessionPagerActivity) context).getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
		((SessionPagerActivity) context).getCurrentPage().isPlayDone = true;
	}
	/*
	 * @SuppressWarnings("resource") class DownloadTask extends
	 * AsyncTask<String, Integer, String> {
	 * 
	 * private Context context; private MediaPlayer player; // private
	 * PowerManager.WakeLock mWakeLock;
	 * 
	 * public DownloadTask(Context context, int currentSid) { this.context =
	 * context; CacheData.getInstant().setCurrSid(currentSid);
	 * DebugLog.loge("get currentSid: "+currentSid); }
	 * 
	 * @Override protected String doInBackground(String... sUrl) { InputStream
	 * input = null; OutputStream output = null; HttpURLConnection connection =
	 * null; try { URL url = new URL(sUrl[0]); connection = (HttpURLConnection)
	 * url.openConnection(); connection.connect();
	 * 
	 * if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) { return
	 * "Server returned HTTP " + connection.getResponseCode() + " " +
	 * connection.getResponseMessage(); }
	 * 
	 * int fileLength = connection.getContentLength();
	 * DebugLog.logd("File lenght: " + fileLength);
	 * 
	 * input = connection.getInputStream(); output = new
	 * FileOutputStream(Utils.path_Download+"/file.mp3");
	 * 
	 * 
	 * byte data[] = new byte[4096]; long total = 0; int count; while ((count =
	 * input.read(data)) != -1) { if (isCancelled()) { input.close(); return "";
	 * }
	 * 
	 * total += count; if (fileLength > 0) { output.write(data, 0, count); int
	 * per = (int) (total * 100 / fileLength); publishProgress(per); }
	 * 
	 * } } catch (Exception e) { return e.toString(); } finally { try { if
	 * (output != null) output.close(); if (input != null) input.close(); }
	 * catch (IOException ignored) { }
	 * 
	 * if (connection != null) connection.disconnect(); } return null; }
	 * 
	 * @Override protected void onPreExecute() { super.onPreExecute(); //
	 * PowerManager pm = (PowerManager)
	 * context.getSystemService(Context.POWER_SERVICE); // mWakeLock =
	 * pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName()); //
	 * mWakeLock.acquire(); }
	 * 
	 * @Override protected void onProgressUpdate(Integer... progress) {
	 * super.onProgressUpdate(progress); //
	 * DebugLog.loge(CacheData.getInstant().getCurrSid() +"_"+
	 * ((SessionPagerActivity)context).getCurrentPage().sid);
	 * if(CacheData.getInstant().getCurrSid() ==
	 * ((SessionPagerActivity)context).getCurrentPage().sid){ if (progress[0] >=
	 * 10){ ((SessionPagerActivity)context).getCurrentPage().isBufferApart =
	 * true; }
	 * ((SessionPagerActivity)context).getCurrentPage().tv_speed_load.setText
	 * ("Received : " + progress[0] + "%"); int percent = progress[0];
	 * ((SessionPagerActivity
	 * )context).getCurrentPage().seekbarPrgress.setSecondaryProgress(percent);
	 * 
	 * if(percent == 100){ if(CacheData.getInstant().isSessionDetail()){
	 * Utils.rename(Utils.path_Download+"/file.mp3", Utils.path_Download + "/" +
	 * CacheData.getInstant().getCurrSid() +".mp3"); //
	 * playSong(CacheData.getInstant().getCurrSid());
	 * ((SessionPagerActivity)context).getCurrentPage().isDownloadDone = true;
	 * CacheData.getInstant().setIs_Done(true); } } } }
	 * 
	 * @Override protected void onPostExecute(String result) { //
	 * mWakeLock.release(); } }
	 * 
	 * boolean isDownloadDone = false;
	 */
}
