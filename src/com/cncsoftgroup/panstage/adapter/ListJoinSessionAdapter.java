package com.cncsoftgroup.panstage.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.common.AppController;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class ListJoinSessionAdapter extends ArrayAdapter<Session>{
	private ArrayList<Session> arraylist;
	private Context context;
	private int layoutId;
	
	ImageTagFactory imageTagFactory =  ImageTagFactory.newInstance();
	ImageManager imageManager = AppController.getImageManager();
	int THUMB_IMAGE_SIZE = 80;
	
	public ListJoinSessionAdapter(Context context, int layoutId, ArrayList<Session> arraylist) {
		super(context, layoutId, arraylist);
		this.context = context;
		this.arraylist = arraylist;
		this.layoutId = layoutId; 
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(layoutId, null);
		}

		imageTagFactory.setHeight(THUMB_IMAGE_SIZE);
		imageTagFactory.setWidth(THUMB_IMAGE_SIZE);
		imageTagFactory.setDefaultImageResId(R.drawable.no_ava);
		imageTagFactory.setErrorImageId(R.drawable.no_ava);
		imageTagFactory.setSaveThumbnail(true);
		imageTagFactory.usePreviewImage(THUMB_IMAGE_SIZE, THUMB_IMAGE_SIZE, true);
		
		
		Session obj = arraylist.get(position);
		
		if (obj != null) {
			View view_item=(View)view.findViewById(R.id.list_join_avata_view);
			ImageView im_avata_list=(ImageView)view_item.findViewById(R.id.im_Home_Avata_List);
			ImageView icon=(ImageView)view_item.findViewById(R.id.icon_type);
			
			TextView name = (TextView) view.findViewById(R.id.list_join_name_user);
			TextView role = (TextView) view.findViewById(R.id.list_join_role_user);
			
			if(obj.getRole().equals("Singer")){
				icon.setImageResource(R.drawable.icon_singer_2);
				role.setText(obj.getRole());
			}
			else if(obj.getRole().equals("Writer")){
				icon.setImageResource(R.drawable.icon_writer_2);
				role.setText(obj.getRole());
			}else if(obj.getRole().equals("Player")){
				icon.setImageResource(R.drawable.icon_player_2);
				role.setText(obj.getRole());
			}
//			DebugLog.logd("=============> Roler \n" +obj.getRole());
//			DebugLog.logd("=============> Name   \n " +obj.getFull_name());
			
			if(obj.getAvatar_url()!=null){
				im_avata_list.setTag(imageTagFactory.build(obj.getAvatar_url(), context));
				imageManager.getLoader().load(im_avata_list);	
			}
			
			name.setText(obj.getFull_name());
			name.setSelected(true); 
		}
		return view;
	}
	
	
}
