package com.cncsoftgroup.panstage.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cncsoftgroup.panstage.common.PanstageConst;
import com.cncsoftgroup.panstage.fragment.IntroListenerFragment;
import com.cncsoftgroup.panstage.fragment.IntroPlayerFragment;
import com.cncsoftgroup.panstage.fragment.IntroSingerFragment;

public class PagerIntroAdapter extends FragmentPagerAdapter {

	public PagerIntroAdapter(FragmentManager fm) {
		super(fm);

	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case PanstageConst.FIRST_PAGE:
			return new IntroPlayerFragment();
		case PanstageConst.SECOND_PAGE:
			return new IntroSingerFragment();
		case PanstageConst.THIRD_PAGE:
			return new IntroListenerFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 3;
	}
}
