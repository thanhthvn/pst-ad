package com.cncsoftgroup.panstage.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cncsoftgroup.panstage.activity.HomePageActivity;
import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.activity.RecordMediaListActivity;
import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.nmd.libs.DebugLog;

public class RecordAdapter extends ArrayAdapter<Record> {

	private ArrayList<Record> arrayRecord;
	private Context context;
	private int layoutId;
	private MediaPlayer mPlayer = null;
	private int play = 0;
	private String newName = "";
	String authu_token;

	public RecordAdapter(Context context, int layoutId, ArrayList<Record> arrayRecord) {
		super(context, layoutId, arrayRecord);
		this.context = context;
		this.layoutId = layoutId;
		this.arrayRecord = arrayRecord;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(layoutId, null);
		}

		final Record re = arrayRecord.get(position);

		if (re != null) {
			final LinearLayout ln_viewplay_info = (LinearLayout) view.findViewById(R.id.ln_viewplay_info);
			final LinearLayout ln_viewplayrecord = (LinearLayout) view.findViewById(R.id.ln_viewplayrecord);
			final TextView list_name_record_play = (TextView) view.findViewById(R.id.list_name_record_play);
			TextView tv_long_time_play = (TextView) view.findViewById(R.id.list_long_time_play);
			TextView list_date_create_play = (TextView) view.findViewById(R.id.list_date_create_play);
			final TextView list_time_play = (TextView) view.findViewById(R.id.list_time_play);
			TextView list_longtime_play = (TextView) view.findViewById(R.id.list_longtime_play);
			TextView list_bt_edit = (TextView) view.findViewById(R.id.list_bt_edit);
			TextView list_name_record = (TextView) view.findViewById(R.id.list_name_record);
			TextView list_date_create = (TextView) view.findViewById(R.id.list_date_create);
			TextView list_longtime = (TextView) view.findViewById(R.id.list_longtime);
			ImageView list_bt_delete = (ImageView) view.findViewById(R.id.list_bt_delete);
			final ImageView list_play_button = (ImageView) view.findViewById(R.id.list_play_button_voicememos);
			createPlayRecord(re.getFile_record());

			final SeekBar playProgress = (SeekBar) view.findViewById(R.id.seekbar_list_play_item);
			playProgress.setProgress(0);
			playProgress.setMax(100);

			ImageView list_bt_push = (ImageView) view.findViewById(R.id.list_bt_push);

			// view item gone or visible
			if (re.isPlayShow()) {
				ln_viewplayrecord.setVisibility(View.VISIBLE);
				ln_viewplay_info.setVisibility(View.GONE);
				list_play_button.setImageResource(R.drawable.bt_play_music_list);
			} else {
				ln_viewplayrecord.setVisibility(View.GONE);
				ln_viewplay_info.setVisibility(View.VISIBLE);
			}
			list_name_record_play.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (mPlayer != null) {
						mPlayer.stop();
					}
					((RecordMediaListActivity) context).gonePlayItem(position);
				}
			});
			// event seekbar click
			((RecordMediaListActivity) context).editSeekbarPlay(list_play_button, list_time_play, playProgress, mPlayer);

			// event button
			list_play_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					boolean ischeck = ((RecordMediaListActivity) context).checkPlay();
					if (play == 0 || ischeck) {
						play = 1;
						list_play_button.setImageResource(R.drawable.bt_pause_music_list);
						createPlayRecord(re.getFile_record());
						mPlayer.start();
						((RecordMediaListActivity) context).UpdateprogressBarPlayRecord(list_play_button, list_time_play, playProgress, mPlayer);
					} else if (play == 1) {
						play = 2;
						list_play_button.setImageResource(R.drawable.bt_play_music_list);
						mPlayer.pause();
					} else {
						play = 1;
						mPlayer.start();
						list_play_button.setImageResource(R.drawable.bt_pause_music_list);
					}
				}
			});
			// button edit
			list_bt_edit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showDialog(re.getName_record(), re.getFile_record());
				}
			});

			authu_token = SharedPreference.loadAuthu_token(context);
			// button push
			list_bt_push.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					showDialogPublic(re, mPlayer.getDuration(), list_play_button, list_time_play, playProgress);
					/*
					 * CacheData.getInstant().setRecord(re);
					 * CacheData.getInstant
					 * ().setDuration(mPlayer.getDuration());
					 * CacheData.getInstant().setCreateSession(true);
					 * ((RecordMediaListActivity)context).startActivity(new
					 * Intent(context, HomePageActivity.class));
					 * ((RecordMediaListActivity
					 * )context).overridePendingTransition(R.anim.push_right_in,
					 * R.anim.push_right_out);
					 * ((RecordMediaListActivity)context).finish();
					 */
					// Utils.showProgressDialog(context,
					// context.getString(R.string.uploading));
					// new SessionManager().createSessionRecord(authu_token,
					// re.getName_record(),Utils.milliSecondsToSeconds(mPlayer.getDuration())
					// , re.getFile_record(), "", "",
					// new OnCreateSessionResult() {
					//
					// @Override
					// public void onCreateSessionMethod(
					// boolean isSuccess) {
					// if (isSuccess) {
					// ((RecordMediaListActivity) context).deleteRecord();
					// Utils.dismissCurrentDialog();
					//
					// final Dialog dialog = new Dialog(context);
					// dialog.setContentView(R.layout.custom_dialog_record_media);
					// dialog.setTitle(context.getString(R.string.invite_friend));
					// dialog.setCancelable(false);
					// final EditText
					// et_name_record=(EditText)dialog.findViewById(R.id.tv_name_record_dialog);
					// Button
					// bt_ok=(Button)dialog.findViewById(R.id.bt_ok_dialog);
					// TextView
					// tv_dialog_row_note=(TextView)dialog.findViewById(R.id.tv_dialog_row_note);
					// tv_dialog_row_note.setText(context.getString(R.string.invite_content));
					// TextView
					// tv_dialog_row_newname=(TextView)dialog.findViewById(R.id.tv_dialog_row_newname);
					// tv_dialog_row_newname.setVisibility(View.GONE);
					//
					// Button
					// bt_cancel=(Button)dialog.findViewById(R.id.bt_cancel_dialog);
					// et_name_record.setVisibility(View.GONE);
					// bt_ok.setOnClickListener(new OnClickListener() {
					// @Override
					// public void onClick(View v) {
					// dialog.cancel();
					// shareFacebook(re.getName_record());
					// }
					// });
					//
					// bt_cancel.setOnClickListener(new OnClickListener() {
					//
					// @Override
					// public void onClick(View v) {
					// dialog.cancel();
					// ((RecordMediaListActivity) context).backHomePage();
					// }
					// });
					// dialog.show();
					//
					// } else {
					// Utils.dismissCurrentDialog();
					// Utils.showToast(context, "Upload failed.");
					// }
					// }
					// });
				}
			});

			list_bt_delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final Dialog dialog = new Dialog(context);
					dialog.setContentView(R.layout.custom_dialog_record_media);
					dialog.setTitle("Delete");
					dialog.setCancelable(false);
					final EditText et_name_record = (EditText) dialog.findViewById(R.id.tv_name_record_dialog);
					Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
					TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
					tv_dialog_row_note.setText(context.getString(R.string.delete_note));
					TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);
					tv_dialog_row_newname.setVisibility(View.GONE);

					Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);
					et_name_record.setVisibility(View.GONE);
					bt_ok.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							File file = new File(re.getFile_record());
							boolean deleted = file.delete();
							if (deleted) {
								((RecordMediaListActivity) context).removeRecord(position);
								dialog.cancel();
								Utils.showToast(context, "Delete record file: " + re.getName_record());
							}
						}
					});

					bt_cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dialog.cancel();
						}
					});
					dialog.show();
				}
			});

			list_name_record_play.setText(re.getName_record());
			list_name_record.setText(re.getName_record());

			list_date_create.setText(re.getDate_create());
			list_date_create_play.setText(re.getDate_create());
			tv_long_time_play.setText(Utils.milliSecondsToTimer2(mPlayer.getDuration()));
			list_longtime_play.setText(Utils.milliSecondsToTimer2(mPlayer.getDuration()));
			list_longtime.setText(Utils.milliSecondsToTimer2(mPlayer.getDuration()));

		}

		return view;
	}

	private void createPlayRecord(String filePath) {
		mPlayer = new MediaPlayer();
		try {
			mPlayer.setDataSource(filePath);
			mPlayer.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void showDialog(final String name, final String path) {
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_record_media);
		dialog.setTitle("Rename");
		dialog.setCancelable(false);
		final EditText et_name_record = (EditText) dialog.findViewById(R.id.tv_name_record_dialog);
		Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
		TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
		tv_dialog_row_note.setText(name);
		TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);
		tv_dialog_row_newname.setText(context.getString(R.string.new_name_record));

		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);
		et_name_record.setHint(context.getString(R.string.edit));
		;
		bt_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				newName = et_name_record.getText().toString();
				if (newName.equals("")) {
					Utils.showErrorNullOrEmpty(et_name_record, "Name Record Not Null.");
				} else if (newName.equals(name)) {
					dialog.cancel();
				} else {
					Utils.showProgressDialog(context, context.getString(R.string.loading));
					renameRecord(newName, path);
					((RecordMediaListActivity) context).getListRecord();
					Utils.dismissCurrentDialog();
					dialog.cancel();
					Utils.showToast(context, "Rename: '" + newName + "' complete.");
				}

			}
		});

		bt_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public void showDialogPublic(final Record re, final int dur, final ImageView list_play_button, final TextView list_time_play, final SeekBar playProgress) {
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_public);
		dialog.setCancelable(false);

		dialog.setTitle(context.getString(R.string.publish_artword));

		// Button bt_listen_again = (Button)
		// dialog.findViewById(R.id.bt_listen_again);
		Button bt_public = (Button) dialog.findViewById(R.id.bt_public);
		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);

		// bt_listen_again.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// dialog.dismiss();
		// boolean ischeck=((RecordMediaListActivity) context).checkPlay();
		// if (play == 0||ischeck) {
		// play = 1;
		// list_play_button.setImageResource(R.drawable.bt_pause_music_list);
		// createPlayRecord(re.getFile_record());
		// mPlayer.start();
		// ((RecordMediaListActivity)
		// context).UpdateprogressBarPlayRecord(list_play_button,
		// list_time_play, playProgress, mPlayer);
		// } else if (play == 1) {
		// play = 2;
		// list_play_button.setImageResource(R.drawable.bt_play_music_list);
		// mPlayer.pause();
		// } else {
		// play = 1;
		// mPlayer.start();
		// list_play_button.setImageResource(R.drawable.bt_pause_music_list);
		// }
		// }
		// });

		bt_public.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				CacheData.getInstant().setRecord(re);
				CacheData.getInstant().setDuration(dur);
				CacheData.getInstant().setCreateSession(true);
				((RecordMediaListActivity) context).startActivity(new Intent(context, HomePageActivity.class));
				((RecordMediaListActivity) context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
				((RecordMediaListActivity) context).finish();
			}
		});

		bt_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		try {
			dialog.show();
		} catch (Exception e) {
		}
	}

	private void renameRecord(String name, String path) {
		File from = new File(path);
		File to = new File(Utils.path_Record + File.separator, name + ".mp3");
		from.renameTo(to);
	}

	public void shareFacebook(String media_url) {
		CacheData.getInstant().setIs_invite(true);
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");

		DebugLog.loge("url: " + CacheData.getInstant().getUrl_upload() + "\n" + context.getString(R.string.invite_facebook_content) + " " + media_url);

		intent.putExtra(Intent.EXTRA_TEXT, CacheData.getInstant().getUrl_upload());
		intent.putExtra(Intent.EXTRA_TITLE, context.getString(R.string.invite_facebook_content) + " " + media_url);

		PackageManager packManager = context.getPackageManager();
		List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		boolean resolved = false;
		for (ResolveInfo resolveInfo : resolvedInfoList) {
			if (resolveInfo.activityInfo.packageName.startsWith("com.facebook.katana")) {
				intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
				resolved = true;
				break;
			}
		}
		if (resolved) {
			((RecordMediaListActivity) context).startActivity(intent);
		} else {
			Utils.showToast(context, "You need to install a Facebook application to use this function");
		}
	}

}
