package com.cncsoftgroup.panstage.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.Utils;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class SessionAdapter extends ArrayAdapter<Session> {
	private ArrayList<Session> arraylist;
	private Context context;
	private int layoutId;

	ImageTagFactory imageTagFactory = ImageTagFactory.newInstance();
	ImageManager imageManager = AppController.getImageManager();
	int THUMB_IMAGE_SIZE = 80;

	public SessionAdapter(Context context, int layoutId, ArrayList<Session> arraylist) {
		super(context, layoutId, arraylist);
		this.context = context;
		this.arraylist = arraylist;
		this.layoutId = layoutId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(layoutId, null);
		}

		imageTagFactory.setHeight(THUMB_IMAGE_SIZE);
		imageTagFactory.setWidth(THUMB_IMAGE_SIZE);
		imageTagFactory.setDefaultImageResId(R.drawable.no_ava);
		imageTagFactory.setErrorImageId(R.drawable.no_ava);
		imageTagFactory.setSaveThumbnail(true);
		imageTagFactory.usePreviewImage(THUMB_IMAGE_SIZE, THUMB_IMAGE_SIZE, true);

		final Session obj = arraylist.get(position);

		if (obj != null) {

			View view_item = (View) view.findViewById(R.id.home_item_avata_view);
			final ImageView im_avata_list = (ImageView) view_item.findViewById(R.id.im_Home_Avata_List);
			ImageView icon = (ImageView) view_item.findViewById(R.id.icon_type);
			TextView time_song = (TextView) view.findViewById(R.id.tv_Home_TimeSong);
			TextView song_name = (TextView) view.findViewById(R.id.tv_Home_NameSong);

			if (obj.getCreator_user_role().equals("Singer")) {
				icon.setImageResource(R.drawable.icon_singer);
				song_name.setText(obj.getSong_name());
			} else if (obj.getCreator_user_role().equals("Writer")) {
				icon.setImageResource(R.drawable.icon_writer);
				song_name.setText(obj.getSong_name());
			} else if (obj.getCreator_user_role().equals("Player")) {
				icon.setImageResource(R.drawable.icon_player);
				song_name.setText(obj.getSong_name());
			}

			if (obj.getDuration() == 0) {
				time_song.setText(context.getString(R.string.lyric));
			} else {
				time_song.setText(Utils.getDuration(obj.getDuration()));
			}

			TextView listeners = (TextView) view.findViewById(R.id.tv_Home_Listen);
			TextView likes = (TextView) view.findViewById(R.id.tv_Home_Like);
			ImageView im_lock = (ImageView) view.findViewById(R.id.im_Home_Lock);
			if (obj.getCreator_user_avatar() != null) {
				im_avata_list.setTag(imageTagFactory.build(obj.getCreator_user_avatar(), context));
				imageManager.getLoader().load(im_avata_list);

			}

			song_name.setSelected(true);

			listeners.setText("" + obj.getListeners());

			likes.setText("" + obj.getLikes());
			if (obj.isLocked()) {
				im_lock.setVisibility(View.VISIBLE);
			} else {
				im_lock.setVisibility(View.GONE);
				// im_lock.setImageResource(R.drawable.im_unlock);
			}
		}
		return view;
	}
}