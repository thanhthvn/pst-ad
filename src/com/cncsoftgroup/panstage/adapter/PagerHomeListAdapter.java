package com.cncsoftgroup.panstage.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cncsoftgroup.panstage.fragment.HomeListMostHeardFragment;
import com.cncsoftgroup.panstage.fragment.HomeListNewestFragment;
import com.cncsoftgroup.panstage.fragment.HomeListUserNameFragment;

public class PagerHomeListAdapter extends FragmentPagerAdapter {
	HomeListUserNameFragment user;
	HomeListNewestFragment newest;
	HomeListMostHeardFragment most;
	
	public PagerHomeListAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		
		switch (index) {
		case 0:
			user = new HomeListUserNameFragment();
			return user;
		case 1:
			newest = new HomeListNewestFragment(); 
			return newest;
		case 2:
			most = new HomeListMostHeardFragment();
			return most;
		}

		return null;
	}
	
	public void refreshData(int pos){
		switch (pos) {
		case 0:
			if(user!=null){
				user.refresh();
			}
		case 1:
			if(newest!=null){
				newest.refresh();
			}
		case 2:
			if(most!=null){
				most.refresh();
			}
		}
	}

	@Override
	public int getCount() {
		return 3;
		
	}

}
