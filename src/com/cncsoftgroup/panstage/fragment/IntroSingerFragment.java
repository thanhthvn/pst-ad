package com.cncsoftgroup.panstage.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.cncsoftgroup.panstage.activity.IntroActivity;
import com.cncsoftgroup.panstage.activity.LoginRegisterActivity;
import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.activity.R.id;
import com.cncsoftgroup.panstage.activity.R.layout;
import com.cncsoftgroup.panstage.activity.R.string;
import com.cncsoftgroup.panstage.common.SharedPreference;

public class IntroSingerFragment extends Fragment {
	private LinearLayout lLayout;
	private Button bt_Login;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		lLayout = (LinearLayout) inflater.inflate(R.layout.intro_singer, container, false);
		bt_Login = (Button) lLayout.findViewById(R.id.bt_loginSinger);
		bt_Login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SharedPreference.saveRoleRegister(getActivity(), getString(R.string.singer));
				v.getContext().startActivity(new Intent(getActivity(), LoginRegisterActivity.class));
				((IntroActivity) v.getContext()).finish();
			}
		});
		return lLayout;
	}
}
