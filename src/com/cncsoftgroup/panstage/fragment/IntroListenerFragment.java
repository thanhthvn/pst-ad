package com.cncsoftgroup.panstage.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.cncsoftgroup.panstage.activity.HomePageActivity;
import com.cncsoftgroup.panstage.activity.IntroActivity;
import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.activity.R.id;
import com.cncsoftgroup.panstage.activity.R.layout;
import com.cncsoftgroup.panstage.activity.R.string;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.nmd.libs.DebugLog;

public class IntroListenerFragment extends Fragment {
	private LinearLayout lLayout;
	private Button bt_login;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		lLayout = (LinearLayout) inflater.inflate(R.layout.intro_listener, container, false);
		bt_login=(Button)lLayout.findViewById(R.id.bt_loginListener);
		bt_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SharedPreference.saveRoleRegister(getActivity(), getString(R.string.listener));
				DebugLog.loge("_0_" + SharedPreference.loadRoleRegister(getActivity()));
				v.getContext().startActivity(new Intent(getActivity(), HomePageActivity.class));
				((IntroActivity)v.getContext()).finish();
			}
		});
		
		return lLayout;
	}
}
