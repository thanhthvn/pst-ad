package com.cncsoftgroup.panstage.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.activity.SessionPagerActivity;
import com.cncsoftgroup.panstage.activity.R.id;
import com.cncsoftgroup.panstage.activity.R.layout;
import com.cncsoftgroup.panstage.adapter.SessionAdapter;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnSessionResult;
import com.cncsoftgroup.panstage.view.LoadMoreListView;
import com.cncsoftgroup.panstage.view.LoadMoreListView.OnLoadMoreListener;
import com.nmd.libs.DebugLog;

public class HomeListMostHeardFragment extends Fragment {
	LoadMoreListView listSession;
	ArrayList<Session> arrayList;
	SessionAdapter sessionAdapter;
	Context context;
	Session obj;
	LinearLayout load;
	FrameLayout fLayout;
	int page=1;
	String authu_token;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		fLayout = (FrameLayout) inflater.inflate(R.layout.homepage_listview, container, false);

		context = getActivity();
		authu_token=SharedPreference.loadAuthu_token(context);
		load=(LinearLayout)fLayout.findViewById(R.id.load);
		listSession = (LoadMoreListView) fLayout.findViewById(R.id.listSession);
		arrayList = new ArrayList<Session>();
//		getListSession(Url.API_LIKE_SESSION_BY_MOST_HEARD);
		
        listSession.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
            	DebugLog.loge("____"+CacheData.getInstant().isLoadMore());
            	if(!CacheData.getInstant().isLoadMore()){
            		if(position<arrayList.size()){
            			CacheData.getInstant().setListSession(arrayList);
        				obj = arrayList.get(position);
                        Intent intent = new Intent(context, SessionPagerActivity.class);
                        Bundle bundle = new Bundle();
                        
                        bundle.putInt("position", position);
                        
                        int pageIntent = 3;
                        bundle.putInt("pageIntent", pageIntent);
                        
                        String urlAPI = Url.API_LIKE_SESSION_BY_MOST_HEARD;
                        bundle.putString("urlAPI", urlAPI);
                        
                        intent.putExtra("myBundle", bundle);
                        startActivityForResult(intent, 1003);
                        getActivity().finish();        
            		}
            	}
            }
            
        });


		listSession.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				if(!isFull){
					page++;
					getMoreListSession(Url.API_LIKE_SESSION_BY_MOST_HEARD, page);
				} else {
//					new Handler().postDelayed(new Runnable() {
//						
//						@Override
//						public void run() {
							listSession.onLoadMoreComplete();							
//						}
//					}, 600);
				}
			}
		});
		return fLayout;
	}

	public void refresh(){
		if (arrayList==null) arrayList = new ArrayList<Session>();
		load.setVisibility(View.VISIBLE);
		getListSession(Url.API_LIKE_SESSION_BY_MOST_HEARD);
	}
	
	private void getListSession(String url) {
		
		arrayList = new ArrayList<Session>();
		new SessionManager().getListSession(authu_token, 1, 10, url, new OnSessionResult() {

			@Override
			public void onSessionMethod(boolean isSuccess, ArrayList<Session> list) {
				CacheData.getInstant().setLoadMore(true);
				if (CacheData.getInstant().isHomeActivity() && isSuccess) {
					arrayList.addAll(list);
					sessionAdapter = new SessionAdapter(getActivity(),
							R.layout.homepage_item_list, arrayList);
					listSession.setAdapter(sessionAdapter);
					load.setVisibility(View.GONE);
					CacheData.getInstant().setLoadMore(false);
				} else {
					CacheData.getInstant().setLoadMore(false);
//					Utils.showToast(context, "Get List Session Failed");
				}
			}
		});
	}
	
	boolean isFull = false;
	private void getMoreListSession(String url, int page) {
		if(page == 2){
			isFull = false;
		}
		new SessionManager().getListSession(authu_token, page, 10, url, new OnSessionResult() {

			@Override
			public void onSessionMethod(boolean isSuccess,
					ArrayList<Session> list) {
				if (CacheData.getInstant().isHomeActivity() && isSuccess) {
					if(list.size() < 10){
						isFull = true;
					}
					arrayList.addAll(list);
					sessionAdapter.notifyDataSetChanged();
				} else {
//					Utils.showToast(context, "Get List Session Failed");
				}
				listSession.onLoadMoreComplete();
			}
		});
	}
	
	
}
