package com.cncsoftgroup.panstage.bean;

public class SessionDetail {
	private int create_user_uid;
	private String create_user_full_name;
	private String create_user_email;
	private String create_user_role;
	private String create_user_role_description;
	private String create_user_avatar_url;
	private Boolean locked;
	
	private String lyric_title;
	
	private String lyric;
	
	public String getLyric_title() {
		return lyric_title;
	}
	public void setLyric_title(String lyric_title) {
		this.lyric_title = lyric_title;
	}
	public String getLyric() {
		return lyric;
	}
	public void setLyric(String lyric) {
		this.lyric = lyric;
	}
	
	public Boolean getLocked() {
		return locked;
	}
	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	public int getCreate_user_uid() {
		return create_user_uid;
	}
	public void setCreate_user_uid(int create_user_uid) {
		this.create_user_uid = create_user_uid;
	}
	public String getCreate_user_full_name() {
		return create_user_full_name;
	}
	public void setCreate_user_full_name(String create_user_full_name) {
		this.create_user_full_name = create_user_full_name;
	}
	public String getCreate_user_email() {
		return create_user_email;
	}
	public void setCreate_user_email(String create_user_email) {
		this.create_user_email = create_user_email;
	}
	public String getCreate_user_role() {
		return create_user_role;
	}
	public void setCreate_user_role(String create_user_role) {
		this.create_user_role = create_user_role;
	}
	public String getCreate_user_role_description() {
		return create_user_role_description;
	}
	public void setCreate_user_role_description(String create_user_role_description) {
		this.create_user_role_description = create_user_role_description;
	}
	public String getCreate_user_avatar_url() {
		return create_user_avatar_url;
	}
	public void setCreate_user_avatar_url(String create_user_avatar_url) {
		this.create_user_avatar_url = create_user_avatar_url;
	}
}
