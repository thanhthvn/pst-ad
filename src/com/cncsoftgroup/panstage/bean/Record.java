package com.cncsoftgroup.panstage.bean;

public class Record {
	String name_record;
	String date_create;
	String long_time;
	String file_record;
	boolean isPlayShow;
	
	public boolean isPlayShow() {
		return isPlayShow;
	}
	public void setPlayShow(boolean isPlayShow) {
		this.isPlayShow = isPlayShow;
	}
	public String getFile_record() {
		return file_record;
	}
	public void setFile_record(String file_record) {
		this.file_record = file_record;
	}
	public String getName_record() {
		return name_record;
	}
	public void setName_record(String name_record) {
		this.name_record = name_record;
	}
	public String getDate_create() {
		return date_create;
	}
	public void setDate_create(String date_create) {
		this.date_create = date_create;
	}
	public String getLong_time() {
		return long_time;
	}
	public void setLong_time(String long_time) {
		this.long_time = long_time;
	}
	
}
