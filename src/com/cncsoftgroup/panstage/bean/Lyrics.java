package com.cncsoftgroup.panstage.bean;

public class Lyrics {
	private String sid = "";
	private String lyric_title = "";
	private String lyric = "";

	public Lyrics() {
	}

	public Lyrics(String sid, String lyric_title, String lyric) {
		this.sid = sid;
		this.lyric_title = lyric_title;
		this.lyric = lyric;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getLyric_title() {
		return lyric_title;
	}

	public void setLyric_title(String lyric_title) {
		this.lyric_title = lyric_title;
	}

	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

}
