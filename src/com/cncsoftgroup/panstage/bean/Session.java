package com.cncsoftgroup.panstage.bean;

public class Session {
	private int id;
	private int creator_user_id;
	private int listeners;
	private String song_name;
	private int duration;
	private String lyric_title;
	private int lyric_composer_id;
	private String lyric;
	private String created_at;
	private String updated_at;
	private String upload_url;
	private String creator_full_name;
	private String upload_s3_url;
	private String last_lyric_updator;
	private boolean locked = false;
	private int likes = 0;
	private boolean is_like = false;
	private String creator_user_avatar;
	private String creator_user_role;
	private String creator_user_role_description;
	private int sid;

	private int uid;
	private String full_name;
	private String email;
	private String role;
	private String role_description;
	private String avatar_url;


	public String getAvatar_url() {
		return avatar_url;
	}

	public void setAvatar_url(String avatar_url) {
		this.avatar_url = avatar_url;
	}

	public String getRole_description() {
		return role_description;
	}

	public void setRole_description(String role_description) {
		this.role_description = role_description;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getCreator_user_role_description() {
		return creator_user_role_description;
	}

	public void setCreator_user_role_description(
			String creator_user_role_description) {
		this.creator_user_role_description = creator_user_role_description;
	}

	public String getCreator_user_role() {
		return creator_user_role;
	}

	public void setCreator_user_role(String creator_user_role) {
		this.creator_user_role = creator_user_role;
	}

	public String getCreator_user_avatar() {
		return creator_user_avatar;
	}

	public void setCreator_user_avatar(String creator_user_avatar) {
		this.creator_user_avatar = creator_user_avatar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreator_user_id() {
		return creator_user_id;
	}

	public void setCreator_user_id(int creator_user_id) {
		this.creator_user_id = creator_user_id;
	}

	public int getListeners() {
		return listeners;
	}

	public void setListeners(int listeners) {
		this.listeners = listeners;
	}

	public String getSong_name() {
		return song_name;
	}

	public void setSong_name(String song_name) {
		this.song_name = song_name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getLyric_title() {
		return lyric_title;
	}

	public void setLyric_title(String lyric_title) {
		this.lyric_title = lyric_title;
	}

	public int getLyric_composer_id() {
		return lyric_composer_id;
	}

	public void setLyric_composer_id(int lyric_composer_id) {
		this.lyric_composer_id = lyric_composer_id;
	}

	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpload_url() {
		return upload_url;
	}

	public void setUpload_url(String upload_url) {
		this.upload_url = upload_url;
	}

	public String getCreator_full_name() {
		return creator_full_name;
	}

	public void setCreator_full_name(String creator_full_name) {
		this.creator_full_name = creator_full_name;
	}

	public String getUpload_s3_url() {
		return upload_s3_url;
	}

	public void setUpload_s3_url(String upload_s3_url) {
		this.upload_s3_url = upload_s3_url;
	}

	public String getLast_lyric_updator() {
		return last_lyric_updator;
	}

	public void setLast_lyric_updator(String last_lyric_updator) {
		this.last_lyric_updator = last_lyric_updator;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public boolean isIs_like() {
		return is_like;
	}

	public void setIs_like(boolean is_like) {
		this.is_like = is_like;
	}

}
