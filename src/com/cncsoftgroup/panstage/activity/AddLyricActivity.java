package com.cncsoftgroup.panstage.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.common.Utils.Keys;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnCreateSessionResult;
import com.cncsoftgroup.panstage.manager.SessionManager.OnLyricSession;
import com.nmd.libs.DebugLog;

public class AddLyricActivity extends Activity implements OnClickListener {
	TextView bt_NotNow, bt_AddLyric;
	EditText addLyric, addTitle;
	Context context;
	String authu_token;
	
	FrameLayout intro;
	Button btn_continue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_lyric);
		context = this;
		
		intro = (FrameLayout) findViewById(R.id.intro);
		btn_continue = (Button) findViewById(R.id.btn_continue);
		btn_continue.setOnClickListener(this);
		
//		com.nmd.libs.SharedPreference.set(AddLyricActivity.this, Keys.INTRO_NEW_WRITER.toString(), "0");
		
		if(com.nmd.libs.SharedPreference.get(AddLyricActivity.this, Keys.INTRO_NEW_WRITER.toString(), "0").equals("1")){
			intro.setVisibility(View.GONE);
		}else{
			intro.setVisibility(View.VISIBLE);
		}
		
		authu_token = SharedPreference.loadAuthu_token(context);
		bt_NotNow = (TextView) findViewById(R.id.bt_NotNow_AddLyric);
		bt_AddLyric = (TextView) findViewById(R.id.bt_AddLyric_Add);

		bt_NotNow.setOnClickListener(this);
		bt_AddLyric.setOnClickListener(this);

		addTitle = (EditText) findViewById(R.id.tv_AddLyric_Title);
		addLyric = (EditText) findViewById(R.id.tv_AddLyric_lyric);
		
		if(CacheData.getInstant().is_update_session()){
			addTitle.setText(CacheData.getInstant().getLyrics().getLyric_title());
			addLyric.setText(CacheData.getInstant().getLyrics().getLyric());
		}
	}
	
	Dialog dialog = null;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bt_NotNow_AddLyric :
				if (CacheData.getInstant().is_update_session()) {
					onBackPressed();
					finish();
				} else {
					startActivity(new Intent(AddLyricActivity.this, HomePageActivity.class));
					overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
					finish();
				}
				break;
			case R.id.bt_AddLyric_Add :

				if (!Utils.isNullOrEmpty(addTitle.getText().toString())) {
					if (!Utils.isNullOrEmpty(addLyric.getText().toString())) {
						dialog = new Dialog(AddLyricActivity.this);
						Utils.showDialog2(AddLyricActivity.this, dialog, "", getString(R.string.publish_lyrics), "Yes", "", new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								dialog.dismiss();
								if (CacheData.getInstant().is_update_session()) {
									Utils.showProgressDialog(AddLyricActivity.this, "Update Lyric.........");
									new SessionManager().updateLyricRecord(authu_token, Integer.toString(CacheData.getInstant().getSid_update()), addTitle.getText().toString(), addLyric.getText().toString(), new OnLyricSession() {
										
										@Override
										public void OnUpdateLyricMethod(boolean success) {
											if (success) {
												Utils.dismissCurrentDialog();
//										onBackPressed();
												Intent returnIntent = getIntent();
												setResult(RESULT_OK, returnIntent);
												finish();
											} else {
												Utils.dismissCurrentDialog();
												Utils.showToast(context, "Update Fail!");
											}
										}
									});
									
								} else {
									Utils.showProgressDialog(AddLyricActivity.this, "Upload Lyric.........");
									new SessionManager().createSession(authu_token, addTitle.getText().toString(), 0, "", addTitle.getText().toString(), addLyric.getText().toString(), new OnCreateSessionResult() {
										
										@Override
										public void onCreateSessionMethod(boolean isSuccess, int song_id) {
											if (isSuccess) {
												DebugLog.logd("Success!");
												Utils.dismissCurrentDialog();
												startActivity(new Intent(AddLyricActivity.this, HomePageActivity.class));
												overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
												finish();
											} else {
												DebugLog.loge("Error!");
												Utils.dismissCurrentDialog();
												Utils.showToast(AddLyricActivity.this, "Upload Lyric Failed!");
											}
										}
									});
								}
								
							}
						}, new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						
					} else {
						Utils.showErrorNullOrEmpty(addLyric, "Lyric is null!");
					}
				} else {
					if (Utils.isNullOrEmpty(addLyric.getText().toString())) {
						Utils.showErrorNullOrEmpty(addLyric, "Lyric is null!");
						Utils.showErrorNullOrEmpty(addTitle, "Lyric is null!");
					} else {
						Utils.showErrorNullOrEmpty(addTitle, "Lyric is null!");
					}
				}

				break;
				
				
			case R.id.btn_continue :
				com.nmd.libs.SharedPreference.set(AddLyricActivity.this, Keys.INTRO_NEW_WRITER.toString(), "1");
				Animation hide = AnimationUtils.loadAnimation(this, R.anim.push_bottom_out);
				hide.setDuration(800);
				intro.startAnimation(hide);
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						intro.setVisibility(View.GONE);
					}
				}, 795);
				break;
				
		}
	}
}
