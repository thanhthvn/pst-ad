package com.cncsoftgroup.panstage.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.cncsoftgroup.panstage.adapter.RecordAdapter;
import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.common.Utils.Keys;
import com.nmd.libs.DebugLog;
import com.nmd.libs.SharedPreference;

public class RecordMediaListActivity extends Activity implements OnClickListener {
	private ListView lv_ListRecord;
	private RecordAdapter recordAdapter;
	private Context context;
	ArrayList<Record> arrayList;
	private int poiCheck = -1;
	private TextView bt_cancel, bt_addlyric, tv_time_play_record;
	private ImageView bt_record, bt_play;
	private Handler mHandler = new Handler();
	private MediaPlayer mPlayer = null;
	private SeekBar progressBarPlayRecord;
	private boolean isPlayerStop = true;
	
	FrameLayout intro;
	Button got_it;

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		try {
			View view = getCurrentFocus();
			boolean ret = super.dispatchTouchEvent(event);

			if (view instanceof EditText) {
				View w = getCurrentFocus();
				int scrcoords[] = new int[2];
				w.getLocationOnScreen(scrcoords);
				float x = event.getRawX() + w.getLeft() - scrcoords[0];
				float y = event.getRawY() + w.getTop() - scrcoords[1];

				if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
				}
			}
			return ret;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.voice_memos_record_list);

		context = this;
		bt_record = (ImageView) findViewById(R.id.bt_Start_Record_List);
		bt_cancel = (TextView) findViewById(R.id.bt_Cancel_list);
		bt_addlyric = (TextView) findViewById(R.id.bt_AddLyric_List);
		
		intro = (FrameLayout) findViewById(R.id.intro);
		got_it = (Button) findViewById(R.id.got_it);
		
		if(SharedPreference.get(RecordMediaListActivity.this, Keys.INTRO_NEW_RECORDLIST.toString(), "0").equals("1")){
			intro.setVisibility(View.GONE);
		}else{
			intro.setVisibility(View.VISIBLE);
		}

		lv_ListRecord = (ListView) findViewById(R.id.list_voicememos_record);
		getListRecord();
		lv_ListRecord.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (mPlayer != null) {
					mPlayer.stop();
					mPlayer = null;
				}
				showPlayItem(poiCheck, position);
				poiCheck = position;
				isPlayerStop = true;
			}
		});

		bt_record.setOnClickListener(this);
		bt_cancel.setOnClickListener(this);
		bt_addlyric.setOnClickListener(this);
		intro.setOnClickListener(this);
		got_it.setOnClickListener(this);

	}

	public void getListRecord() {
		if (Utils.loadRecord(Utils.path_Record) != null) {
			arrayList = new ArrayList<Record>();
			arrayList = Utils.loadRecord(Utils.path_Record);

			recordAdapter = new RecordAdapter(context, R.layout.voice_memos_record_list_item, sortListRoom(arrayList));
			lv_ListRecord.setAdapter(recordAdapter);
		}
	}

	public ArrayList<Record> sortListRoom(ArrayList<Record> originList) {
		Collections.sort(originList, new Comparator<Record>() {

			@Override
			public int compare(Record arg0, Record arg1) {
				String id1 = ((Record) arg0).getName_record();
				String id2 = ((Record) arg1).getName_record();

				return id1.compareTo(id2);
			}
		});
		return originList;
	}

	public void showPlayRecord() {
		for (int i = 0; i < arrayList.size(); i++) {
			arrayList.get(i).setPlayShow(false);
		}
		recordAdapter.notifyDataSetChanged();
	}

	public void removeRecord(int pos) {
		arrayList.remove(pos);
		recordAdapter.notifyDataSetChanged();
	}

	private void showPlayItem(int poiShow, int position) {
		if (poiShow == -1) {
			arrayList.get(position).setPlayShow(true);
		} else if (arrayList.size() > poiShow) {
			arrayList.get(poiShow).setPlayShow(false);
			arrayList.get(position).setPlayShow(true);
		} else {
			arrayList.get(position).setPlayShow(true);
		}
		recordAdapter.notifyDataSetChanged();
	}

	public void gonePlayItem(int poi) {
		arrayList.get(poi).setPlayShow(false);
		if (mPlayer != null) {
			mPlayer.stop();
		}
		recordAdapter.notifyDataSetChanged();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bt_Cancel_list :
				startActivity(new Intent(RecordMediaListActivity.this, HomePageActivity.class));
				overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
				finish();
				break;
			case R.id.bt_Start_Record_List :
				CacheData.getInstant().setIs_join_record(false);
				startActivity(new Intent(RecordMediaListActivity.this, RecordMediaActivity.class));
				overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
				finish();
				break;
			case R.id.bt_AddLyric_List :
				CacheData.getInstant().setIs_update_session(false);
				startActivity(new Intent(RecordMediaListActivity.this, AddLyricActivity.class));
				overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
				finish();
				break;
				
			case R.id.got_it :
				SharedPreference.set(RecordMediaListActivity.this, Keys.INTRO_NEW_RECORDLIST.toString(), "1");
				Animation hide = AnimationUtils.loadAnimation(this, R.anim.push_bottom_out);
				hide.setDuration(800);
				intro.startAnimation(hide);
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						intro.setVisibility(View.GONE);
					}
				}, 795);
				break;
				
			case R.id.intro :
				break;

		}
	}

	public void playThis(String url) {
		CacheData.getInstant().setUrl_test(url);
		bt_record.performClick();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startActivityForResult(new Intent(RecordMediaListActivity.this, HomePageActivity.class), 1000);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		finish();
	}

	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			if (mPlayer != null) {
				long totalDuration = mPlayer.getDuration();
				long currentDuration = mPlayer.getCurrentPosition();

				tv_time_play_record.setText(Utils.milliSecondsToTimer2(currentDuration));

				int progress = (int) (Utils.getProgressPercentage(currentDuration, totalDuration));
				progressBarPlayRecord.setProgress(progress);
				mHandler.postDelayed(this, 100);
				if (progress == 100) {
					bt_play.setImageResource(R.drawable.bt_play_music_list);
					mPlayer.stop();
					isPlayerStop = true;
				}
			}
		}
	};

	public void UpdateprogressBarPlayRecord(ImageView button_play, TextView tv, SeekBar sb, MediaPlayer mp) {
		isPlayerStop = false;
		bt_play = button_play;
		tv_time_play_record = tv;
		progressBarPlayRecord = sb;
		mPlayer = mp;
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}
	public void editSeekbarPlay(ImageView button_play, TextView tv, SeekBar sb, MediaPlayer mp) {
		bt_play = button_play;
		tv_time_play_record = tv;
		progressBarPlayRecord = sb;
		mPlayer = mp;
		progressBarPlayRecord.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);
				if (mPlayer.isPlaying()) {
					int totalDuration = mPlayer.getDuration();
					int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);
					mPlayer.seekTo(currentPosition);
					UpdateprogressBarPlayRecord(bt_play, tv_time_play_record, progressBarPlayRecord, mPlayer);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			}
		});
	}

	public boolean checkPlay() {
		return isPlayerStop;
	}

	public void deleteRecord() {
		ArrayList<Record> arrlList = new ArrayList<Record>();
		arrlList = Utils.loadRecord(Utils.path_Record);
		DebugLog.loge("Delete Record: \n");
		for (int i = 0; i < arrlList.size(); i++) {
			File file = new File(arrlList.get(i).getFile_record());
			file.delete();
			DebugLog.loge(arrlList.get(i).getFile_record() + "\n");
		}
	}

	public void backHomePage() {
		startActivity(new Intent(RecordMediaListActivity.this, HomePageActivity.class));
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		finish();
	}

	@Override
	protected void onResume() {
		CacheData.getInstant().setUrl_test("");
		if (CacheData.getInstant().isIs_invite()) {
			CacheData.getInstant().setIs_invite(false);
			// backHomePage();
		}
		super.onResume();
	}

}
