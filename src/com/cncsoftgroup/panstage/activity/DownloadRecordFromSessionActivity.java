package com.cncsoftgroup.panstage.activity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;

import javazoom.jl.converter.Converter;
import javazoom.jl.converter.Converter.ProgressListener;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.Obuffer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.Utils;
import com.nmd.libs.DebugLog;
import com.uraroji.garage.android.lame.SoxManager;
import com.uraroji.garage.android.lame.SoxManager.OnConvertFileResult;

public class DownloadRecordFromSessionActivity extends Activity {
	
	boolean isTest = false;
	
	private String url_recording, song_name;
	private int sid;
	private Context context;
	private ProgressBar progress_download;
	private TextView tv_progress_downlad, bt_cancel_download, songname_download;
	private DownloadFileFromURL asyntaskDownload;
	
	boolean isExistMp3File = false;
	
	String pathFileDownLoad	=  "";
	String pathFileConvert1	=  "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join_session_player_or_singer);
		context = this;

		Intent intent = getIntent();
		Bundle bundle = intent.getBundleExtra("Url_media");
		url_recording = bundle.getString("media_url");
		song_name = bundle.getString("song_name");
		sid = bundle.getInt("sid");

		pathFileDownLoad = Utils.path_Download + "/" + sid +".mp3";
		pathFileConvert1 = Utils.path_Download + "/" + sid +".wav";
		
		CacheData.getInstant().setUrl_mix(pathFileConvert1);
		CacheData.getInstant().setSid_join(sid);
		
		DebugLog.loge("url_recording: " + url_recording);
		
		if(url_recording == null || url_recording.equals("null") || url_recording.equals("")){
			CacheData.getInstant().setJoinLyrics(true);
			finishActivity(true);
			return;
		}
		
		CacheData.getInstant().setJoinLyrics(false);
		
		progress_download = (ProgressBar) findViewById(R.id.progress_download);
		tv_progress_downlad = (TextView) findViewById(R.id.tv_isdownload);
		bt_cancel_download = (TextView) findViewById(R.id.bt_cancel_download);
		songname_download = (TextView) findViewById(R.id.songname_download);
		songname_download.setText(song_name);

		
//		if(CacheData.getInstant().isDownload()){
//			Utils.rename(Utils.path_Download+"/file.mp3", pathFileDownLoad);
//			progress_download.setProgress(95);
//			tv_progress_downlad.setText("95");
//			new Handler().postDelayed(new Runnable() {
//				
//				@Override
//				public void run() {
//					progress_download.setProgress(99);
//					tv_progress_downlad.setText("99");
//					convertFile();
//				}
//			}, 1000);
//		}else{
//			asyntaskDownload = new DownloadFileFromURL();
//			asyntaskDownload.execute(url_recording);			
//		}
		
		
		bt_cancel_download.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				asyntaskDownload.cancel(true);
				if(!pathFileDownLoad.equals("")){
//					Utils.delete(pathFileDownLoad);
				}
				startActivity(new Intent(context, HomePageActivity.class));
				overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
				finish();
			}
		});
		
		if(isTest){
			asyntaskDownload = new DownloadFileFromURL();
			asyntaskDownload.execute(url_recording);		
			return;
		}

		ArrayList< Record> arrlList=new ArrayList<Record>();
		arrlList=loadRecord(Utils.path_Download);
		
		DebugLog.loge("size: "+arrlList.size());
		
		String wavFile = sid+".wav";
		String mp3File = sid+".mp3";
		
		for(int i=0; i<arrlList.size(); i++){
			DebugLog.loge("file: " + Utils.getFileNameAndExtension(arrlList.get(i).getFile_record()));
			if(wavFile.equals(Utils.getFileNameAndExtension(arrlList.get(i).getFile_record()))){
				finishActivity(false);
				return;
			}
			if(mp3File.equals(Utils.getFileNameAndExtension(arrlList.get(i).getFile_record()))){
				isExistMp3File = true;
			}
		}
		
		if(isExistMp3File){
			progress_download.setProgress(95);
			tv_progress_downlad.setText("95");
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					progress_download.setProgress(99);
					tv_progress_downlad.setText("99");
					convertFile();
				}
			}, 1000);
		}else{
			asyntaskDownload = new DownloadFileFromURL();
			asyntaskDownload.execute(url_recording);			
		}
	}
	
	public File[] listValidFiles(File file) {
	    return file.listFiles(new FilenameFilter() {

	        @Override
	        public boolean accept(File dir, String filename) {
	            File file2 = new File(dir, filename);
	            return (filename.contains(".wav"))|| 
	            		filename.contains(".mp3")
	                    && !file2.isHidden()
	                    && !filename.startsWith(".");

	        }
	    });
	}
	
	public ArrayList<Record> loadRecord(String path) {
		File f = new File(path);
		File[] files = listValidFiles(f);
		if (files != null) {
			ArrayList<Record> recordArray = new ArrayList<Record>();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				Record re = new Record();
				String name = file.getName();
				if(!name.contains("mixed.mp3")){
					int pos = name.lastIndexOf(".");
					if (pos > 0) {
						name = name.substring(0, pos);
					}

					Date lastModDate = new Date(file.lastModified());
					if (name != null) {
						re.setName_record(name);
						re.setDate_create(lastModDate + "");
						re.setFile_record(file.getPath());
						re.setPlayShow(false);
						recordArray.add(re);
					}
				}
			}
			return recordArray;
		} else
			return null;
	}

	class DownloadFileFromURL extends AsyncTask<String, String, String> {
		Boolean isdownload = true;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress_download.setMax(100);
		}

		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				int lenghtOfFile = conection.getContentLength();

				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				OutputStream output = new FileOutputStream(pathFileDownLoad);

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));

					output.write(data, 0, count);
				}

				output.flush();

				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
				isdownload = false;
			}

			return null;
		}
		
		protected void onProgressUpdate(String... progress) {
			progress_download.setProgress(Integer.parseInt(progress[0]));
			tv_progress_downlad.setText("" + Integer.parseInt(progress[0]));
		}

		@Override
		protected void onPostExecute(String file_url) {
			if (isdownload) {
				convertFile();
			} else {
				Utils.showToast(context, "Download fail!");
			}
		}
		
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(asyntaskDownload!=null){
			asyntaskDownload.cancel(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		asyntaskDownload.cancel(true);
		if(!pathFileDownLoad.equals("")){
//			Utils.delete(pathFileDownLoad);		
		}
		startActivity(new Intent(context, HomePageActivity.class));
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
		finish();
	}

	@Override
	protected void onPause() {
		isExistMp3File = false;
		super.onPause();
	}
	
	private void finishActivity(boolean isJoinLyric) {
		Intent intent = null;
		if(isJoinLyric){
			intent = new Intent(context, JoinLyricMediaActivity.class);
		}else{
			intent = new Intent(context, JoinRecordActivity.class);
		}
		Bundle bundle = new Bundle();
		bundle.putInt("sid", sid);
		bundle.putString("song_name", song_name);
		bundle.putString("url_mix", pathFileConvert1);
		intent.putExtra("session_media", bundle);
		CacheData.getInstant().setUrl_mix(pathFileConvert1);
		CacheData.getInstant().setIs_join_record(true);
		CacheData.getInstant().setMix(true);
		startActivityForResult(intent, 1100);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		finish();
	}

	private void convertFile() {
		if(isTest){
			pathFileConvert1 = pathFileDownLoad;
			finishActivity(false);
			return;
		}
		if (CacheData.getInstant().is_update_session()) {
			CacheData.getInstant().setIs_join_record(false);
		} else {
			CacheData.getInstant().setIs_join_record(true);
		}

		try {
			new Converter().convert(pathFileDownLoad,  Utils.path_FileA1, new ProgressListener() {
				
				@Override
				public void readFrame(int arg0, Header arg1) {
				}
				
				@Override
				public void parsedFrame(int arg0, Header arg1) {
				}
				
				@Override
				public void decodedFrame(int arg0, Header arg1, Obuffer arg2) {
				}
				
				@Override
				public void converterUpdate(int arg0, int arg1, int arg2) {
//					DebugLog.loge("_converterUpdate_" + "\n" + arg0 + "\n" + arg1 + "\n" + arg2);
					if(arg2>1){
//						Utils.delete(pathFileDownLoad);		
						SoxManager.convertFile(DownloadRecordFromSessionActivity.this, Utils.path_FileA1, pathFileConvert1, Utils.path_Mix, new OnConvertFileResult() {
							
							@Override
							public void onConvertFileMethod(boolean isSuccess, String message) {
								if(isSuccess){
									finishActivity(false);
								}else{
									Utils.showToast(DownloadRecordFromSessionActivity.this, "error " + message);
					        		onBackPressed();
								}
							}
						});
					}
				}
				
				@Override
				public boolean converterException(Throwable arg0) {
					return false;
				}
			});
		}catch (Exception e) {
			if(e!=null) DebugLog.loge(e.getMessage());
		}
	}

}
