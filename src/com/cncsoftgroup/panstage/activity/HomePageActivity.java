package com.cncsoftgroup.panstage.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cncsoftgroup.panstage.adapter.PagerHomeListAdapter;
import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.common.Utils.Keys;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnCreateSessionResult;
import com.cncsoftgroup.panstage.manager.SessionManager.OnUpdateSession;
import com.cncsoftgroup.panstage.manager.SessionManager.OnUserLikeSession;
import com.cncsoftgroup.panstage.manager.UserManager;
import com.cncsoftgroup.panstage.manager.UserManager.OnLoginAndRegisterResult;
import com.nmd.libs.DebugLog;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class HomePageActivity extends FragmentActivity implements OnClickListener {
	TextView name, logout, bt_new;
	TextView by_username, by_newest, by_most_heard;
	ImageView avatar, icon, next_userpage;
	int intpage = 1;

	View view_avata;
	LinearLayout profile;
	LinearLayout loading, menu;
	private String authu_token, full_name, avatar_url;
	ImageTagFactory imageTagFactory = ImageTagFactory.newInstance();
	ImageManager imageManager = AppController.getImageManager();
	int THUMB_IMAGE_SIZE = 160;

	private ViewPager mPager;
	private PagerHomeListAdapter mAdapter;
	PageIndicator mIndicator;
	private Context context;
	
	FrameLayout intro;
	Button got_it;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homepage);

		context = this;
		
		intro = (FrameLayout) findViewById(R.id.intro);
		got_it = (Button) findViewById(R.id.got_it);
		got_it.setOnClickListener(this);
		
//		if(com.nmd.libs.SharedPreference.get(context, Keys.INTRO_NEW_HOMEPAGE.toString(), "0").equals("1")){
			intro.setVisibility(View.GONE);
//		}else{
//			intro.setVisibility(View.VISIBLE);
//		}
		
		
		name = (TextView) findViewById(R.id.name);

		if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
			name.setText(getString(R.string.guest));
		} else {
			getValue();
			name.setText(full_name);
		}

		bt_new = (TextView) findViewById(R.id.homepage_Bt_New);

		loading = (LinearLayout) findViewById(R.id.loading);
		menu = (LinearLayout) findViewById(R.id.menu);

		loading.setVisibility(View.GONE);
		menu.setVisibility(View.VISIBLE);

		if (CacheData.getInstant().isCreateSession()) {
			CacheData.getInstant().setCreateSession(false);
			createSesstion();
		}

		if (CacheData.getInstant().isUploadSession()) {
			CacheData.getInstant().setUploadSession(false);
			updateSession();
		}

		bt_new.setOnClickListener(this);

		if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
			avatar_url = "";
		}
		imageTagFactory.setHeight(THUMB_IMAGE_SIZE);
		imageTagFactory.setWidth(THUMB_IMAGE_SIZE);
		imageTagFactory.setDefaultImageResId(R.drawable.no_ava);
		imageTagFactory.setErrorImageId(R.drawable.no_ava);
		imageTagFactory.setSaveThumbnail(true);
		imageTagFactory.usePreviewImage(THUMB_IMAGE_SIZE, THUMB_IMAGE_SIZE, true);
		view_avata = (View) findViewById(R.id.home_avata_view);
		avatar = (ImageView) view_avata.findViewById(R.id.im_Home_Avata_List);
		icon = (ImageView) view_avata.findViewById(R.id.icon_type);

		avatar.setTag(imageTagFactory.build(avatar_url, HomePageActivity.this));
		// avatar.setOnClickListener(this);
		imageManager.getLoader().load(avatar);
		profile = (LinearLayout) findViewById(R.id.profile);
		profile.setOnClickListener(this);
		profile.setClickable(true);

		if (SharedPreference.loadRole(HomePageActivity.this).equals("Writer")) {
			icon.setImageResource(R.drawable.icon_writer);
		} else if (SharedPreference.loadRole(HomePageActivity.this).equals("Player")) {
			icon.setImageResource(R.drawable.icon_player);
		} else if (SharedPreference.loadRole(HomePageActivity.this).equals("Singer")) {
			icon.setImageResource(R.drawable.icon_singer);
		}

		by_username = (TextView) findViewById(R.id.by_username);
		by_username.setOnClickListener(this);
		by_newest = (TextView) findViewById(R.id.by_newest);
		by_newest.setOnClickListener(this);
		by_most_heard = (TextView) findViewById(R.id.by_most_heard);
		by_most_heard.setOnClickListener(this);

		next_userpage = (ImageView) findViewById(R.id.imgNextUserPage);
		next_userpage.setOnClickListener(this);

		logout = (TextView) findViewById(R.id.logout);
		if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
			logout.setText(getString(R.string.sign_up));
		}
		logout.setOnClickListener(this);

		mPager = (ViewPager) findViewById(R.id.pager_home);

		mAdapter = new PagerHomeListAdapter(getSupportFragmentManager());
		mPager.setAdapter(mAdapter);
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator_homepage);
		mIndicator.setViewPager(mPager);

		mPager.setCurrentItem(intpage);
		by_username.setBackgroundResource(R.drawable.homepage_bt_username);
		by_newest.setBackgroundResource(R.drawable.homepage_bt_newest_is_select);
		by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard);

		CacheData.getInstant().setHomeActivity(true);

		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				tabSelect(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

		switch (CacheData.getInstant().getPage_position()) {
			case 0 :
				// mPager.setCurrentItem(0);
				by_username.setBackgroundResource(R.drawable.homepage_bt_username_is_select);
				by_newest.setBackgroundResource(R.drawable.homepage_bt_newest);
				by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard);

				by_username.setTextColor(Color.parseColor("#ffffff"));
				by_newest.setTextColor(Color.parseColor("#248AB0"));
				by_most_heard.setTextColor(Color.parseColor("#248AB0"));
				// mAdapter.refreshData(CacheData.getInstant().getPage_position());
				tabSelect(0);
				break;
			case 1 :
				// mPager.setCurrentItem(1);
				by_username.setBackgroundResource(R.drawable.homepage_bt_username);
				by_newest.setBackgroundResource(R.drawable.homepage_bt_newest_is_select);
				by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard);
				by_username.setTextColor(Color.parseColor("#248AB0"));
				by_newest.setTextColor(Color.parseColor("#ffffff"));
				by_most_heard.setTextColor(Color.parseColor("#248AB0"));
				// mAdapter.refreshData(CacheData.getInstant().getPage_position());
				tabSelect(1);
				break;
			case 2 :
				// mPager.setCurrentItem(2);
				by_username.setBackgroundResource(R.drawable.homepage_bt_username);
				by_newest.setBackgroundResource(R.drawable.homepage_bt_newest);
				by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard_is_select);

				by_username.setTextColor(Color.parseColor("#248AB0"));
				by_newest.setTextColor(Color.parseColor("#248AB0"));
				by_most_heard.setTextColor(Color.parseColor("#ffffff"));
				// mAdapter.refreshData(CacheData.getInstant().getPage_position());
				tabSelect(2);
				break;
		}

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				tabSelect(1);
			}
		}, 500);
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void onPause() {
		CacheData.getInstant().setHomeActivity(false);
		DebugLog.loge("onPause");
		super.onPause();
	}

	@Override
	protected void onResume() {
		CacheData.getInstant().setHomeActivity(true);
		DebugLog.loge("onResume");
		super.onResume();
	}

	private void getValue() {
		if (SharedPreference.loadAuthu_token(context).equals("") && !SharedPreference.loadAuthu_token(context).equals(CacheData.getInstant().getAuth_token())) {
			SharedPreference.saveAuthu_token(context, CacheData.getInstant().getAuth_token());
			SharedPreference.saveAvatar(context, CacheData.getInstant().getAvatar());
			SharedPreference.saveFullName(context, CacheData.getInstant().getFull_name());
			SharedPreference.saveUid(context, CacheData.getInstant().getUid());
			SharedPreference.saveRoleDescription(context, CacheData.getInstant().getRole_description());
			SharedPreference.saveRole(context, CacheData.getInstant().getRole());
		}
		authu_token = SharedPreference.loadAuthu_token(context);
		avatar_url = SharedPreference.loadAvata(context);
		full_name = SharedPreference.loadFullName(context);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.homepage_Bt_New :
				if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
					Utils.showToast(HomePageActivity.this, "Please sign up!");
					return;
				}
				if (SharedPreference.loadRole(HomePageActivity.this).equals("Writer")) {
					startActivity(new Intent(HomePageActivity.this, AddLyricActivity.class));
					overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
					finish();
				} else {
					startActivity(new Intent(HomePageActivity.this, RecordMediaListActivity.class));
					overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
					finish();
				}
				break;
			case R.id.by_username :
				tabSelect(0);
				break;

			case R.id.by_newest :
				tabSelect(1);
				break;

			case R.id.by_most_heard :
				tabSelect(2);
				break;

			case R.id.logout :
				if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
					startActivity(new Intent(HomePageActivity.this, IntroActivity.class));
					finish();
				} else {
					logoutDialog();
				}
				break;
			case R.id.imgNextUserPage :
				if (SharedPreference.loadRole(HomePageActivity.this).equals("Listener")) {
				} else {
					Intent intent = new Intent(HomePageActivity.this, UserPageActivity.class);
					intent.putExtra("intpage", intpage + 1);
					startActivity(intent);
					overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
					finish();
				}
				break;

			case R.id.profile :
				if (SharedPreference.loadRoleRegister(HomePageActivity.this).equals(getString(R.string.listener))) {
					Utils.showToast(HomePageActivity.this, "Please sign up!");
					return;
				}
				DebugLog.loge("profile click");
				if (SharedPreference.loadRole(HomePageActivity.this).equals("Listener")) {
				} else {
					showUserPage();
				}
				break;
				
			case R.id.got_it :
				com.nmd.libs.SharedPreference.set(HomePageActivity.this, Keys.INTRO_NEW_HOMEPAGE.toString(), "1");
				Animation hide = AnimationUtils.loadAnimation(this, R.anim.push_bottom_out);
				hide.setDuration(800);
				intro.startAnimation(hide);
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						intro.setVisibility(View.GONE);
					}
				}, 795);
				break;
				
		}
	}

	void showUserPage() {
		Intent intent = new Intent(HomePageActivity.this, UserPageActivity.class);
		intent.putExtra("intpage", intpage + 1);
		startActivity(intent);
		overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
		finish();
	}

	private void tabSelect(int pos) {
		if (pos == CacheData.getInstant().getPage_position()) {

		} else if (pos == 0) {
			by_username.setBackgroundResource(R.drawable.homepage_bt_username_is_select);
			by_newest.setBackgroundResource(R.drawable.homepage_bt_newest);
			by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard);

			by_username.setTextColor(Color.parseColor("#ffffff"));
			by_newest.setTextColor(Color.parseColor("#248AB0"));
			by_most_heard.setTextColor(Color.parseColor("#248AB0"));
		} else if (pos == 1) {
			by_username.setBackgroundResource(R.drawable.homepage_bt_username);
			by_newest.setBackgroundResource(R.drawable.homepage_bt_newest_is_select);
			by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard);

			by_username.setTextColor(Color.parseColor("#248AB0"));
			by_newest.setTextColor(Color.parseColor("#ffffff"));
			by_most_heard.setTextColor(Color.parseColor("#248AB0"));
		} else if (pos == 2) {
			by_username.setBackgroundResource(R.drawable.homepage_bt_username);
			by_newest.setBackgroundResource(R.drawable.homepage_bt_newest);
			by_most_heard.setBackgroundResource(R.drawable.homepage_bt_most_heard_is_select);

			by_username.setTextColor(Color.parseColor("#248AB0"));
			by_newest.setTextColor(Color.parseColor("#248AB0"));
			by_most_heard.setTextColor(Color.parseColor("#ffffff"));
		}
		mAdapter.refreshData(pos);
		intpage = pos;
		CacheData.getInstant().setPage_position(pos);
		mPager.setCurrentItem(pos);
	}

	public void logoutDialog() {
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_record_media);
		dialog.setTitle(R.string.logout_note);
		dialog.setCancelable(false);
		final EditText et_name_record = (EditText) dialog.findViewById(R.id.tv_name_record_dialog);
		Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
		TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
		tv_dialog_row_note.setVisibility(View.GONE);
		TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);
		tv_dialog_row_newname.setVisibility(View.GONE);

		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);
		et_name_record.setVisibility(View.GONE);
		bt_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();
				Utils.showProgressDialog(context, "Wait....");
				new UserManager().logout(authu_token, new OnLoginAndRegisterResult() {
					@Override
					public void onLoginAndRegisterMethod(boolean isSuccess) {
						if (isSuccess) {
							DebugLog.loge("Logout Success");
						} else {
							DebugLog.loge("Logout Failed");
						}
						Utils.dismissCurrentDialog();
						SharedPreference.hasLoginFacebook(context, false);
						SharedPreference.hasAlreadyLoginFacebook(context, false);
						CacheData.getInstant().setAcces_token("");
						SharedPreference.saveEmailAndPassword(context, "", "");
						SharedPreference.saveRole(HomePageActivity.this, "");
						SharedPreference.saveAuthu_token(context, "");
						SharedPreference.saveAvatar(context, "");
						SharedPreference.saveFullName(context, "");
						SharedPreference.saveUid(context, 0);
						SharedPreference.saveRoleDescription(context, "");
						SharedPreference.saveRole(context, "");
						startActivity(new Intent(HomePageActivity.this, IntroActivity.class));
						finish();
					}
				});

			}
		});

		bt_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();

	}

	public void shareFacebook(int song_id) {
		CacheData.getInstant().setIs_invite(true);
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");

		// DebugLog.loge("url: "+CacheData.getInstant().getUrl_upload() + "\n" +
		// context.getString(R.string.invite_facebook_content) + " "+
		// media_url);

		intent.putExtra(Intent.EXTRA_TEXT, CacheData.getInstant().getUrl_upload());
		// intent.putExtra(Intent.EXTRA_TITLE,
		// context.getString(R.string.invite_facebook_content) + " "+
		// media_url);
		intent.putExtra(Intent.EXTRA_TEXT, "http://s.pan-stage.com/songs/" + song_id);

		PackageManager packManager = context.getPackageManager();
		List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		boolean resolved = false;
		for (ResolveInfo resolveInfo : resolvedInfoList) {
			if (resolveInfo.activityInfo.packageName.startsWith("com.facebook.katana")) {
				intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
				resolved = true;
				break;
			}
		}

		if (resolved) {
			((HomePageActivity) context).startActivity(intent);
		} else {
			Utils.showToast(context, "You need to install a Facebook application to use this function");
		}
	}

	Record re = null;
	public void createSesstion() {
		if (CacheData.getInstant().getRecord() != null) {
			if (com.nmd.libs.SharedPreference.get(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "0").equals("1")) {
				return;
			} else {
				com.nmd.libs.SharedPreference.set(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "1");
			}
			DebugLog.loge("createSesstion");
			// Utils.showProgressDialog(context,
			// context.getString(R.string.uploading));
			loading.setVisibility(View.VISIBLE);
			menu.setVisibility(View.GONE);
			CacheData.getInstant().setCreateSession(false);
			re = CacheData.getInstant().getRecord();
			new SessionManager().createSessionRecord(authu_token, re.getName_record(), Utils.milliSecondsToSeconds(Utils.getDurationFile(re.getFile_record())), re.getFile_record(), "", "", new OnCreateSessionResult() {

				@Override
				public void onCreateSessionMethod(final boolean isSuccess, final int song_id) {
					com.nmd.libs.SharedPreference.set(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "0");

					if(isSuccess){
						new SessionManager().Like_Join_Lock_Session(authu_token, song_id, Url.API_LOCK_SESSION, new OnUserLikeSession() {

							@Override
							public void OnUserLikeSessionMethod(boolean success) {
								if (CacheData.getInstant().isHomeActivity()) {
									loading.setVisibility(View.GONE);
									menu.setVisibility(View.VISIBLE);
								}
								DebugLog.loge("createSesstion: " + isSuccess + " id: " + song_id);

								if (CacheData.getInstant().isHomeActivity() && CacheData.getInstant().getPage_position() == 1) {
									// mAdapter.refreshData(CacheData.getInstant().getPage_position());
									by_newest.performClick();
								}

								if (isSuccess) {
									deleteRecord();

									final Dialog dialog = new Dialog(context);
									dialog.setContentView(R.layout.custom_dialog_record_media);
									dialog.setTitle(context.getString(R.string.invite_friend));
									dialog.setCancelable(false);
									final EditText et_name_record = (EditText) dialog.findViewById(R.id.tv_name_record_dialog);
									Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
									TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
									tv_dialog_row_note.setText(context.getString(R.string.invite_content));
									TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);
									tv_dialog_row_newname.setVisibility(View.GONE);

									Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);
									et_name_record.setVisibility(View.GONE);
									bt_ok.setOnClickListener(new OnClickListener() {
										@Override
										public void onClick(View v) {
											dialog.cancel();
											shareFacebook(song_id);
										}
									});

									bt_cancel.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											dialog.cancel();
										}
									});
									try {
										if (CacheData.getInstant().isHomeActivity()) {
											dialog.show();
										}
									} catch (Exception e) {
									}

								}
							}
						});
					} else {
						Utils.showToast(context, "Upload failed.");
					}
				}
			});
			CacheData.getInstant().setRecord(null);
		}
	}

	private void updateSession() {
		if (com.nmd.libs.SharedPreference.get(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "0").equals("1")) {
			return;
		} else {
			com.nmd.libs.SharedPreference.set(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "1");
		}
		DebugLog.loge("updateSession");
		loading.setVisibility(View.VISIBLE);
		menu.setVisibility(View.GONE);
		String file = "";
		if (CacheData.getInstant().isJoinLyrics()) {
			file = CacheData.getInstant().getName_record();
		} else {
			file = Utils.path_FileOutMp3;
		}

		new SessionManager().updateSessionRecord(SharedPreference.loadAuthu_token(context), Integer.toString(CacheData.getInstant().getSid()), Utils.milliSecondsToSeconds(Utils.getDurationFile(file)), file, new OnUpdateSession() {

			@Override
			public void OnUpdateSessionMethod(boolean success) {
				DebugLog.loge("updateSession: " + success);
				com.nmd.libs.SharedPreference.set(HomePageActivity.this, Keys.IS_UPLOAD.toString(), "0");
				if (success) {
					deleteRecord();

					Utils.rename(Utils.path_Download + "/" + CacheData.getInstant().getSid_join() + ".mp3", Utils.path_Download + "/" + CacheData.getInstant().getSid_join() + "_.mp3");
					Utils.rename(Utils.path_Download + "/" + CacheData.getInstant().getSid_join() + ".wav", Utils.path_Download + "/" + CacheData.getInstant().getSid_join() + "_.wav");
					if (CacheData.getInstant().isHomeActivity() && CacheData.getInstant().getPage_position() == 1) {
						by_newest.performClick();
					}
					loading.setVisibility(View.GONE);
					menu.setVisibility(View.VISIBLE);
//					if (CacheData.getInstant().isJoin()) {
//						CacheData.getInstant().setJoin(false);
//						new SessionManager().Like_Join_Lock_Session(SharedPreference.loadAuthu_token(context), CacheData.getInstant().getSid(), Url.API_JOIN_SESSION, new OnUserLikeSession() {
//
//							@Override
//							public void OnUserLikeSessionMethod(boolean success) {
//								if (CacheData.getInstant().isHomeActivity()) {
//									loading.setVisibility(View.GONE);
//									menu.setVisibility(View.VISIBLE);
//								}
//
//								if (success) {
//								} else {
//									if (CacheData.getInstant().isHomeActivity()) {
//										loading.setVisibility(View.GONE);
//										menu.setVisibility(View.VISIBLE);
//									}
//								}
//							}
//						});

//					} else {
//						if (CacheData.getInstant().isHomeActivity()) {
//							loading.setVisibility(View.GONE);
//							menu.setVisibility(View.VISIBLE);
//						}
//					}
				} else {
					if (CacheData.getInstant().isHomeActivity()) {
						loading.setVisibility(View.GONE);
						menu.setVisibility(View.VISIBLE);
					}
				}
			}
		});
	}

	private ArrayList<Record> loadRecord() {
		File f = new File(Utils.path_Record);
		File[] files = f.listFiles();
		if (files != null) {
			ArrayList<Record> recordArray = new ArrayList<Record>();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				Record re = new Record();
				String name = file.getName();
				int pos = name.lastIndexOf(".");
				if (pos > 0) {
					name = name.substring(0, pos);
				}

				Date lastModDate = new Date(file.lastModified());
				if (name != null) {
					re.setName_record(name);
					re.setDate_create(lastModDate + "");
					re.setFile_record(file.getPath());
					re.setPlayShow(false);
					recordArray.add(re);
				}
			}
			return recordArray;
		} else
			return null;
	}

	private ArrayList<Record> loadMix() {
		File f = new File(Utils.path_Record);
		File[] files = f.listFiles();
		if (files != null) {
			ArrayList<Record> recordArray = new ArrayList<Record>();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				Record re = new Record();
				String name = file.getName();
				int pos = name.lastIndexOf(".");
				if (pos > 0) {
					name = name.substring(0, pos);
				}

				Date lastModDate = new Date(file.lastModified());
				if (name != null) {
					re.setName_record(name);
					re.setDate_create(lastModDate + "");
					re.setFile_record(file.getPath());
					re.setPlayShow(false);
					recordArray.add(re);
				}
			}
			return recordArray;
		} else
			return null;
	}

	public void deleteRecord() {
		ArrayList<Record> arrlList = new ArrayList<Record>();
		arrlList = loadRecord();
		DebugLog.loge("Delete Record: \n");
		for (int i = 0; i < loadRecord().size(); i++) {
			File file = new File(arrlList.get(i).getFile_record());
			file.delete();
			DebugLog.loge(arrlList.get(i).getFile_record() + "\n");
		}

		ArrayList<Record> arrlList2 = new ArrayList<Record>();
		arrlList2 = loadMix();
		DebugLog.loge("Delete Record: \n");
		for (int i = 0; i < loadRecord().size(); i++) {
			File file = new File(arrlList2.get(i).getFile_record());
			file.delete();
			DebugLog.loge(arrlList2.get(i).getFile_record() + "\n");
		}
	}

}
