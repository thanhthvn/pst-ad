package com.cncsoftgroup.panstage.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.TextView;

import com.cncsoftgroup.panstage.adapter.PagerIntroAdapter;
import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.UserManager;
import com.cncsoftgroup.panstage.manager.UserManager.OnLoginAndRegisterResult;
import com.nmd.libs.DebugLog;
import com.nmd.libs.UtilLibs;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class IntroActivity extends FragmentActivity {
	PagerIntroAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;
	TextView buildday;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		SharedPreference.saveAuthu_token(IntroActivity.this, "");
		try {
			new File(Utils.path).mkdirs();
			new File(Utils.path_Record).mkdirs();
			new File(Utils.path_Mix).mkdirs();
			new File(Utils.path_Download).mkdirs();
			
			new File(Utils.path_Record_temp).mkdirs();
			new File(Utils.path_Record_temp).delete();
			new File(Utils.path_Record_temp).mkdirs();


			UtilLibs.writeToFileInSDCard(Utils.path_Download, ".nomedia", "0");
			UtilLibs.writeToFileInSDCard(Utils.path_Record, ".nomedia", "0");
			UtilLibs.writeToFileInSDCard(Utils.path_Record_temp, ".nomedia", "0");
			UtilLibs.writeToFileInSDCard(Utils.path_Mix, ".nomedia", "0");

			File[] files = new File(Utils.path_Download).listFiles();
			DebugLog.loge("num download file: " + files.length);
			if (files.length > 10)
				deleteRecord();
			new File(Utils.path_Download).mkdirs();
		} catch (Exception e) {

		}

		if (SharedPreference.isAlreadyLoginFacebook(IntroActivity.this)) {
			startActivity(new Intent(IntroActivity.this, LoginRegisterActivity.class));
			finish();
			return;
		}

		if (!SharedPreference.loadRoleRegister(IntroActivity.this).equals(getString(R.string.listener))) {
			if (!SharedPreference.loadEmail(this).equals("") && !SharedPreference.loadPassword(this).equals("")) {
				Utils.showProgressDialog(IntroActivity.this, getString(R.string.loading));
				new UserManager().login(SharedPreference.loadEmail(this), SharedPreference.loadPassword(this), new OnLoginAndRegisterResult() {

					@Override
					public void onLoginAndRegisterMethod(boolean isSuccess) {
						if (isSuccess) {
							DebugLog.logd("Success!");
							Utils.dismissCurrentDialog();
							startActivity(new Intent(IntroActivity.this, HomePageActivity.class));
							finish();
							return;
						} else {
							Utils.showToast(IntroActivity.this, "Failed!");
							Utils.dismissCurrentDialog();
						}
					}
				});
			}
		}

		// startActivity(new Intent(this, AudioBufferSize.class));

		setContentView(R.layout.activity_view_slide);
		
		buildday = (TextView) findViewById(R.id.buildday);
		buildday.setText("June 25 2015");

		mAdapter = new PagerIntroAdapter(getSupportFragmentManager());
		CacheData.getInstant().setIntroActivity(true);
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		changePage(0);

		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				changePage(position);
				mIndicator.setCurrentItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

	}
	

	Handler handler;
	Runnable runnable;

	public void changePage(final int position) {
		if (handler == null)
			handler = new Handler();
		if (runnable != null) {
			handler.removeCallbacks(runnable);
		}
		runnable = new Runnable() {

			@Override
			public void run() {
				if (position == 0) {
					mPager.setCurrentItem(1);
				} else if (position == 1) {
					mPager.setCurrentItem(2);
				} else if (position == 2) {
					mPager.setCurrentItem(0);
				}
			}
		};

		if (CacheData.getInstant().isIntroActivity()) {
			handler.postDelayed(runnable, 5000);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		CacheData.getInstant().setIntroActivity(true);
	}

	@Override
	protected void onPause() {
		super.onPause();
		CacheData.getInstant().setIntroActivity(false);
	}

	private ArrayList<Record> loadRecord() {
		File[] files = new File(Utils.path_Download).listFiles();
		if (files != null) {
			ArrayList<Record> recordArray = new ArrayList<Record>();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				Record re = new Record();
				String name = file.getName();
				int pos = name.lastIndexOf(".");
				if (pos > 0) {
					name = name.substring(0, pos);
				}

				Date lastModDate = new Date(file.lastModified());
				if (name != null) {
					re.setName_record(name);
					re.setDate_create(lastModDate + "");
					re.setFile_record(file.getPath());
					re.setPlayShow(false);
					recordArray.add(re);
				}
			}
			return recordArray;
		} else
			return null;
	}

	private void deleteRecord() {
		ArrayList<Record> arrlList = new ArrayList<Record>();
		arrlList = loadRecord();
		DebugLog.loge("Delete Record: \n");
		for (int i = 0; i < arrlList.size(); i++) {
			File file = new File(arrlList.get(i).getFile_record());
			file.delete();
			DebugLog.loge(arrlList.get(i).getFile_record() + "\n");
		}
	}

}
