package com.cncsoftgroup.panstage.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javazoom.jl.converter.Converter;
import javazoom.jl.converter.Converter.ProgressListener;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.decoder.Obuffer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.soundvisualizer.CDrawer;
import com.cncsoftgroup.panstage.soundvisualizer.WaveformView;
import com.nmd.libs.DebugLog;
import com.nmd.libs.UtilLibs;
import com.nmd.libs.UtilLibs.OnExecuteMethod;
import com.uraroji.garage.android.lame.LameEncodeTask;
import com.uraroji.garage.android.lame.LameEncodeTask.OnEncodeResult;
import com.uraroji.garage.android.lame.ShellUtils.ShellCallback;
import com.uraroji.garage.android.lame.SimpleLame;
import com.uraroji.garage.android.lame.SoxController;

public class JoinRecordActivity0 extends Activity implements OnClickListener, OnCompletionListener, WaveformView.WaveformListener {
	static {
		System.loadLibrary("mp3lame");
	}

	boolean testDelay = false;

	private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".mp3";
	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private AudioRecord recorder = null;
	private int bufferSize = 0;
	private Thread recordingThread = null;
	private boolean isRecording = false;

	private MediaPlayer mPlayer = null;
	private SeekBar volumeSeekbar = null;
	private SeekBar seekbar_latency = null;
	private AudioManager audioManager = null;

	private ImageView bt_play, bt_record, bt_backward, bt_foward;
	private TextView tv_time_record, tv_time_play_record, tv_longtime_play, tv_record_name;
	ImageView bt_done;

	private int seekForwardTime = 5000;
	private int seekBackwardTime = 5000;

	private Handler mHandler = new Handler();

	private String name_record = "";
	private String file_name = "";
	boolean check_record = false;
	int play = 0, record = 0;

	private CDrawer.CDrawThread mDrawThread;
	private CDrawer mdrawer;

	private OnClickListener listener;
	private Boolean m_bStart = Boolean.valueOf(false);
	@SuppressWarnings("unused")
	private static short[] buffer_vusual;

	private SeekBar progressBarPlayRecord;
	private Context context;

	private LinearLayout lLayout_visual;
	private String song_name;
	private int sid;
	private String url_mix = "";
	private Boolean isJoin = false;
	private Boolean isUpdate = false;
	TextView latecy_txt1, latecy_txt2, latency_text_1, latency_text_2;

	String full_name, authu_token;

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		try {
			View view = getCurrentFocus();
			boolean ret = super.dispatchTouchEvent(event);

			if (view instanceof EditText) {
				View w = getCurrentFocus();
				int scrcoords[] = new int[2];
				w.getLocationOnScreen(scrcoords);
				float x = event.getRawX() + w.getLeft() - scrcoords[0];
				float y = event.getRawY() + w.getTop() - scrcoords[1];

				if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
				}
			}
			return ret;
		} catch (Exception e) {
			return false;
		}
	}

	public JoinRecordActivity0() {
		name_record = getFilenameRandom();
		file_name = Long.toString(System.currentTimeMillis());
	}

	private String getFilenameRandom() {
		return (Utils.path_Record_temp + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.voice_memos_record);
		context = this;

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mDensity = displaymetrics.density;
		buildGUI();

		full_name = SharedPreference.loadFullName(context);
		authu_token = SharedPreference.loadAuthu_token(context);

		latecy_txt1 = (TextView) findViewById(R.id.latecy1);
		latecy_txt1.setOnClickListener(this);

		latecy_txt2 = (TextView) findViewById(R.id.latecy2);
		latecy_txt2.setOnClickListener(this);

		if (testDelay) {
			latecy_txt1.setVisibility(View.VISIBLE);
			latecy_txt2.setVisibility(View.VISIBLE);
		} else {
			latecy_txt1.setVisibility(View.GONE);
			latecy_txt2.setVisibility(View.GONE);
		}

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		bt_play = (ImageView) findViewById(R.id.bt_play_voicememos);
		bt_record = (ImageView) findViewById(R.id.bt_record_voidmemos);
		bt_backward = (ImageView) findViewById(R.id.bt_backward_voicememos);
		bt_foward = (ImageView) findViewById(R.id.bt_forward_voidmemos);
		tv_time_record = (TextView) findViewById(R.id.tv_Time_Record);
		tv_longtime_play = (TextView) findViewById(R.id.tv_longtime_play_record);
		tv_time_play_record = (TextView) findViewById(R.id.tv_time_play_record);
		tv_record_name = (TextView) findViewById(R.id.tv_RecordName_voidmemos);
		bt_done = (ImageView) findViewById(R.id.bt_done_voicememos);

		progressBarPlayRecord = (SeekBar) findViewById(R.id.progressbar_play_record);
		isJoin = CacheData.getInstant().is_join_record();
		isUpdate = CacheData.getInstant().is_update_session();
		lLayout_visual = (LinearLayout) findViewById(R.id.ln_visual);
		((LinearLayout) findViewById(R.id.latency_view_title)).setVisibility(View.VISIBLE);
		((LinearLayout) findViewById(R.id.latency_view_bar)).setVisibility(View.VISIBLE);
		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

		DebugLog.loge("duration: " + CacheData.getInstant().getDuration());

		if (isJoin || isUpdate) {
			if (CacheData.getInstant().isMix()) {
				Intent intent = getIntent();
				Bundle bundle = intent.getBundleExtra("session_media");

				song_name = bundle.getString("song_name");
				url_mix = CacheData.getInstant().getUrl_mix();
				DebugLog.loge("url_mix: \n" + url_mix);
				if (url_mix.equals("")) {
					onBackPressed();
					return;
				}
				sid = bundle.getInt("sid");
				DebugLog.logd("=>>>>>>>" + sid);
				tv_record_name.setText(song_name);
			}
		} else {
			tv_record_name.setText(full_name);
		}

		buffer_vusual = new short[bufferSize];
		bt_done.setOnClickListener(this);
		bt_backward.setOnClickListener(this);
		bt_foward.setOnClickListener(this);
		bt_play.setOnClickListener(this);
		bt_record.setOnClickListener(this);
		initControls();
		editSeekbarPlay();
		if (!CacheData.getInstant().isMix()) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					bt_record.performClick();
				}
			}, 500);
		}
	}

	private void soundVisualr() {
		mdrawer = new CDrawer(context, null);
		lLayout_visual.addView(mdrawer);
		m_bStart = Boolean.valueOf(false);
	}

	public void setBuffer(short[] paramArrayOfShort) {
		mDrawThread = mdrawer.getThread();
		mDrawThread.setBuffer(paramArrayOfShort);
	}

	@Override
	protected void onPause() {
		if (mDrawThread != null) {
			mDrawThread.setRun(Boolean.valueOf(false));
			mDrawThread.SetSleeping(Boolean.valueOf(true));
			Boolean.valueOf(false);
		}
		super.onPause();
		if (recorder != null) {
			recorder.release();
			recorder = null;
		}
		/*
		 * if(track != null){ track.release(); track= null; }
		 */
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
		if (CacheData.getInstant().isMix()) {
			CacheData.getInstant().setUrl_mix("");
		}
		CacheData.getInstant().setMix(false);
	}

	int duration_origin_file = 0;
	long duration_record_file = 0;
	long duration_mixed_file = 0;

	@Override
	protected void onRestart() {
		m_bStart = Boolean.valueOf(true);
		System.out.println("onRestart");
		super.onRestart();
	}

	private void runVisual() {

		int i = 0;
		while (true) {
			if (isRecording && (mdrawer.GetDead2().booleanValue())) {
				if (!m_bStart.booleanValue())
					mdrawer.Restart(Boolean.valueOf(true));
				mDrawThread.SetSleeping(Boolean.valueOf(false));
				m_bStart = Boolean.valueOf(false);
				return;
			}
			updateDisplay();
			try {
				Thread.sleep(50);
				System.out.println("Hang on..");
				i++;
				if (!mdrawer.GetDead2().booleanValue()) {
					System.out.println("mDrawer not DeAD!!");
					mdrawer.SetRun(Boolean.valueOf(false));
				}
				if (i <= 4)
					continue;
				mDrawThread.SetDead2(Boolean.valueOf(true));
			} catch (InterruptedException localInterruptedException) {
				localInterruptedException.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void run() {

		try {
			if (mDrawThread == null) {
				mDrawThread = mdrawer.getThread();
			}
			Context localContext = getApplicationContext();
			Display localDisplay = getWindowManager().getDefaultDisplay();
			Toast localToast = Toast.makeText(localContext, "Start Recording..", Toast.LENGTH_LONG);
			localToast.setGravity(48, 0, localDisplay.getHeight() / 8);
			localToast.show();
			mdrawer.setOnClickListener(listener);

		} catch (NullPointerException e) {
			Log.e("Main_Run", "NullPointer: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ====================Method Recording================================

	// AudioTrack track = null;

	private void startRecording(final boolean b) {
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize * 2);
		// track = new AudioTrack(AudioManager.STREAM_MUSIC,
		// RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO,
		// RECORDER_AUDIO_ENCODING, bufferSize * 2, AudioTrack.MODE_STREAM);

		isRecording = true;
		run();
		recorder.startRecording();
		// track.play();
		runVisual();
		recordingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				writeAudioDataToFile(b);
			}
		}, "AudioRecorder Thread");

		recordingThread.start();

	}

	SoxController soxController, soxController2;
	List<String> listFiles = Arrays.asList(Utils.path_FileA2, Utils.path_FileB2);

	// name_record

	int delayCurr = 0;

	private void stopRecording(boolean b) {
		if (recorder != null) {
			isRecording = false;

			duration_record_file = Utils.getDurationFile(name_record);

			delay = duration_record_file - currentTimePlay;
			delayCurr = (int) delay;
			DebugLog.loge("\ncurrent_stop_time: " + currentTimePlay + "\nduration_record_file: " + duration_record_file + "\ndelay: " + delay);

			recorder.stop();
			recorder.release();

			recorder = null;
			recordingThread = null;
			/*
			 * try { track.stop(); track.release(); track = null; } catch
			 * (Exception e) { }
			 */

			startMix();
		}
	}

	boolean isConvert = false;
	String latecy1 = "0";
	String latecy2 = "0";
	int timePause = 0;
	// TODO
	private void startMix() {
		play = 0;
		DebugLog.loge("---> isClickReplay: " + isClickReplay);
		if (isClickReplay) {
			if (mPlayer.isPlaying()) {
				bt_play.setImageResource(R.drawable.play_music_play);
				timePause = mPlayer.getCurrentPosition();
				DebugLog.loge("timePause: " + timePause);
				pausePlaying();
			}
		}
		if (CacheData.getInstant().isMix()) {
			latency_value_temp = SharedPreference.loadLatency(JoinRecordActivity0.this);
			Utils.showProgressDialog(JoinRecordActivity0.this, "mixing sessions...");

			try {
				soxController = new SoxController(JoinRecordActivity0.this, new File(""), new ShellCallback() {

					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}

					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							if (!isConvert) {
								isConvert = true;
								soxController.combineMix(listFiles, Utils.path_FileOut);
							} else {
								wavToMp3();
							}
							return;
						}
						if (exitValue != 0) {
							Utils.showToast(JoinRecordActivity0.this, "error " + exitValue);
							onBackPressed();
						}
					}
				});

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						mp3ToWav();
					}
				}, 1000);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void mp3ToWav() {
		try {
			soxController2 = new SoxController(JoinRecordActivity0.this, new File(""), new ShellCallback() {

				@Override
				public void shellOut(String shellLine) {
					DebugLog.loge("shellLine:\n" + shellLine);
				}

				@Override
				@SuppressLint("UseValueOf")
				public void processComplete(int exitValue) {
					if (exitValue == 0) {
						if (testDelay) {
							soxController.convertChFile(Utils.path_FileB1, Utils.path_FileB2, String.valueOf((Double.parseDouble(latecy_txt2.getText().toString().replaceAll("ms", "")) / 1000)));
						} else {
							// soxController.convertChFile(Utils.path_FileB1,
							// Utils.path_FileB2, "0");
							soxController.convertChFile(Utils.path_FileB1, Utils.path_FileB2, getLatency2());
						}
					}
					if (exitValue != 0) {
						Utils.showToast(JoinRecordActivity0.this, "error " + exitValue);
						onBackPressed();
					}
				}
			});

			new Converter().convert(name_record, Utils.path_FileB1, new ProgressListener() {

				@Override
				public void readFrame(int arg0, Header arg1) {
				}

				@Override
				public void parsedFrame(int arg0, Header arg1) {
				}

				@Override
				public void decodedFrame(int arg0, Header arg1, Obuffer arg2) {
				}

				@Override
				public void converterUpdate(int arg0, int arg1, int arg2) {
					// DebugLog.loge("_Mix_" + "\n" + arg0 + "\n" + arg1 + "\n"
					// + arg2);
					if (arg2 > 1) {
						long duration_record_file_wav = Utils.getDurationFile(Utils.path_FileB1);
						double latecy_time = duration_origin_file - duration_record_file_wav;
						latecy1 = "0";
						// if(isFull && latecy_time > 0){
						// DecimalFormat decimalFormat = new
						// DecimalFormat("#0.000");
						// latecy1 =
						// String.valueOf(decimalFormat.format((double)
						// latecy_time/1000).replaceAll(",", "."));
						// latecy2 =
						// String.valueOf(decimalFormat.format((double)
						// (latecy_time/1000)*0.9).replaceAll(",", "."));
						// }
						DebugLog.loge("\nduration_origin_file: " + duration_origin_file + "\nduration_record_file_wav: " + duration_record_file_wav + "\nlatecy: " + latecy_time);
						if (testDelay) {
							soxController2.padFile(CacheData.getInstant().getUrl_mix(), Utils.path_FileA2, String.valueOf((Double.parseDouble(latecy_txt1.getText().toString().replaceAll("ms", "")) / 1000)));
						} else {
							// soxController2.padFile(CacheData.getInstant().getUrl_mix(),
							// Utils.path_FileA2, "0.280");
							soxController2.padFile(CacheData.getInstant().getUrl_mix(), Utils.path_FileA2, getLatency1());
						}
					}
				}

				@Override
				public boolean converterException(Throwable arg0) {
					return false;
				}
			});
		} catch (JavaLayerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private short[] resize(short[] b) {
		short[] c = new short[b.length / 70];
		for (int i = 0; i < b.length / 70; i++) {
			c[i] = b[i];
		}
		return c;
	}

	int sample = 0;
	private void writeAudioDataToFile(boolean b) {
		try {
			String filename = name_record;
			short[] buffer = new short[RECORDER_SAMPLERATE * (16 / 8) * 1 * 5];
			byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];

			FileOutputStream os = null;
			try {
				os = new FileOutputStream(filename, b);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			SimpleLame.init(RECORDER_SAMPLERATE, 1, RECORDER_SAMPLERATE, 128);

			int read = 0;
			if (os != null) {
				while (isRecording) {
					read = recorder.read(buffer, 0, bufferSize);
					try {
						// track.write(buffer, 0, bufferSize);
					} catch (Exception e) {
						// TODO: handle exception
					}
					setBuffer(resize(buffer));
					if (read < 0) {
					} else if (read == 0) {
					} else {
						int encResult = SimpleLame.encode(buffer, buffer, read, mp3buffer);
						sample = read;
						if (encResult < 0) {
						}
						if (encResult != 0) {
							try {
								os.write(mp3buffer, 0, encResult);
							} catch (IOException e) {
							}
						}
					}
				}
				mDrawThread.setRun(Boolean.valueOf(false));
				mDrawThread.SetSleeping(Boolean.valueOf(true));
				Boolean.valueOf(false);

				int flushResult = SimpleLame.flush(mp3buffer);
				if (flushResult < 0) {
				}
				if (flushResult != 0) {
					try {
						os.write(mp3buffer, 0, flushResult);
					} catch (IOException e) {
					}
				}

				try {
					os.close();
				} catch (IOException e) {
				}
			}
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}
	}

	private void wavToMp3() {
		isConvert = false;

		String[] params = new String[]{Utils.path_FileOut.toString(), Utils.path_FileOutMp3.toString()};
		Utils.dismissCurrentDialog();
		Utils.showProgressDialog(context, "converting to MP3 format...");
		new LameEncodeTask(JoinRecordActivity0.this, new OnEncodeResult() {

			@Override
			public void onEncodeMethod(boolean isSuccess, String message) {
				Utils.dismissCurrentDialog();
				if (isSuccess) {
					duration_mixed_file = Utils.getDurationFile(Utils.path_FileOutMp3);
					long duration_origin_file2 = Utils.getDurationFile(Utils.path_FileA2);
					delay = duration_mixed_file - currentTimePlay;
					DebugLog.loge("final: \ncurrent_stop_time: " + currentTimePlay + "\nduration_mixed_file: " + duration_mixed_file + "\ndelay: " + delay);
					DebugLog.loge("final: \nduration_origin_file: " + duration_origin_file2 + "\nduration_mixed_file: " + duration_mixed_file + "\ndelay: " + (duration_origin_file2 - duration_mixed_file));
					// TODO
					progressBarPlayRecord.setProgress(0);
					tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
					bt_play.setImageResource(R.drawable.play_music_play);
					if (mPlayer != null)
						mPlayer.stop();
					play = 0;

					/*
					 * if(timePause>0){ play = 3; bt_play.performClick(); }
					 */
					// Utils.delete(pathFileB);
					// Utils.delete(pathFileOut);
				} else {
					Utils.showToast(JoinRecordActivity0.this, message);
				}
			}
		}).execute(params);
	}

	// ====================Method Recording End================================

	private void startPlaying(String url, int time) {
		mPlayer = new MediaPlayer();
		try {
			mPlayer.reset();
			mPlayer.setDataSource(url);
			mPlayer.prepare();
			mPlayer.setOnCompletionListener(this);
			mPlayer.start();
			progressBarPlayRecord.setProgress(0);
			progressBarPlayRecord.setMax(100);
			if (!isRecording) {
				tv_longtime_play.setText(Utils.milliSecondsToTimer(mPlayer.getDuration()));
			}
			duration_origin_file = mPlayer.getDuration();

			if (time > 0) {
				mPlayer.seekTo(time);
				timePause = 0;
			}
			updateprogressBarPlayRecord();

			// DebugLog.loge("LONG TIME: " + dur);
			// DebugLog.loge(mPlayer.getDuration() + "__" +
			// Utils.getDurationFile(url));
		} catch (IOException e) {
			Log.e("LOG_TAG", "prepare() failed");
		}
	}

	private void resumePlaying() {
		if (mPlayer != null) {
			if (!mPlayer.isPlaying()) {
				mPlayer.start();
				updateprogressBarPlayRecord();
				if (!isFull && CacheData.getInstant().isMix()) {
					// mPlayer.seekTo((int) currtime + delayCurr);
					mPlayer.seekTo((int) currentTimePlay);
				}
			}
			DebugLog.loge("resumePlaying");
		}
	}

	private int currentDurationPlaying = 0;
	private void pausePlaying() {
		if (mPlayer != null) {
			if (mPlayer.isPlaying()) {
				mPlayer.pause();
			}
			DebugLog.loge("pausePlaying");
		}
	}

	private void stopPlaying() {
		if (mPlayer != null) {
			mPlayer.stop();
			mPlayer.release();
			mPlayer = null;
			DebugLog.loge("stopPlaying");
		}
	}

	boolean isFull = false;
	boolean isRecordAgain = false;
	long currentTimePlay = 0;
	long delay;

	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			if (mPlayer != null) {
				if (mPlayer.isPlaying()) {
					if (!isRecording) {
						long totalDuration = mPlayer.getDuration();
						long currentDuration = mPlayer.getCurrentPosition();

						tv_time_play_record.setText(Utils.milliSecondsToTimer(currentDuration));

						int progress = (int) (Utils.getProgressPercentage(currentDuration, totalDuration));
						progressBarPlayRecord.setProgress(progress);
						mHandler.postDelayed(this, 100);
						if (progress == 100) {
							// DebugLog.loge(currentDuration+"\n"+progress);
							progressBarPlayRecord.setProgress(0);
							tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
							bt_play.setImageResource(R.drawable.play_music_play);
							mPlayer.stop();
							play = 0;
							return;
						} else {
							if (currentDuration >= currentTimePlay) {
								bt_play.performClick();
								progressBarPlayRecord.setProgress(0);
								tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
								// bt_play.setImageResource(R.drawable.play_music_play);
								play = 4;
							}
						}
						if (CacheData.getInstant().isMix() && !isFull) {
							if (currentDuration >= currentDurationPlaying && currentDuration != 0 && currentDurationPlaying != 0) {
								DebugLog.loge(currentDuration + "\n" + currentDurationPlaying);
								play = 2;
								bt_play.setImageResource(R.drawable.play_music_play);
								pausePlaying();
								progressBarPlayRecord.setProgress(0);
								tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
								mPlayer.seekTo(0);
							}
						}
					} else {
						currentTimePlay = mPlayer.getCurrentPosition();

						tv_time_record.setText(Utils.milliSecondsToTimer(currentTimePlay));
						mHandler.postDelayed(this, 100);
					}
				}
			}
		}
	};

	@Override
	public void onCompletion(MediaPlayer mp) {
		if (isRecording) {
			delay = duration_origin_file - currentTimePlay;
			DebugLog.loge("stop: \ncurrent_stop_time: " + currentTimePlay + "\nduration_origin_file: " + duration_origin_file + "\ndelay: " + delay);
			tv_time_record.setText(Utils.getDurationLongTime(0));
			stopPlaying();
			play = 0;
			bt_record.performClick();
			isFull = true;
		}
	}

	public void updateprogressBarPlayRecord() {
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	Dialog dialog = null;
	private void showDialogDone() {
		dialog = new Dialog(context);
		Utils.showDialog3(JoinRecordActivity0.this, dialog, getString(R.string.publish_artword), getString(R.string.upmix), "", "", "", new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (isUpdate && check_record) {
					updateSession(file_name);
				} else if (isJoin) {
					updateSession(file_name);
				} else {
					v.getContext().startActivity(new Intent(JoinRecordActivity0.this, HomePageActivity.class));
					overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
					finish();
				}
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				bt_play.performClick();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				v.getContext().startActivity(new Intent(JoinRecordActivity0.this, HomePageActivity.class));
				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
				finish();
			}
		});
		/*
		 * final Dialog dialog = new Dialog(context);
		 * dialog.setContentView(R.layout.custom_dialog_record);
		 * dialog.setCancelable(false); final EditText ed_name_record_dialog =
		 * (EditText) dialog.findViewById(R.id.ed_name_record_dialog); TextView
		 * tv_dialog_row_note
		 * =(TextView)dialog.findViewById(R.id.tv_dialog_row_note); TextView
		 * tv_dialog_row_newname
		 * =(TextView)dialog.findViewById(R.id.tv_dialog_row_newname);
		 * 
		 * if(isJoin){ dialog.setTitle(getString(R.string.publish_artword));
		 * tv_dialog_row_note.setText(getString(R.string.upmix));
		 * ed_name_record_dialog.setVisibility(View.GONE);
		 * tv_dialog_row_newname.setVisibility(View.GONE); }else{
		 * dialog.setTitle(getString(R.string.newsession));
		 * tv_dialog_row_note.setText(getString(R.string.new_name)); }
		 * 
		 * Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
		 * Button bt_cancel = (Button)
		 * dialog.findViewById(R.id.bt_cancel_dialog);
		 * 
		 * ed_name_record_dialog.setText(file_name);
		 * 
		 * if(CacheData.getInstant().isMix()){ //
		 * tv_dialog_row_newname.setVisibility(View.GONE); //
		 * ed_name_record_dialog.setVisibility(View.GONE); }
		 * 
		 * bt_ok.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { String name =
		 * ed_name_record_dialog.getText().toString(); if(name.equals("")){
		 * Utils.showErrorNullOrEmpty(ed_name_record_dialog,
		 * "Name Record Not Null."); return; }else{
		 * Utils.renameRecord(Utils.getFileName(name_record), name); }
		 * 
		 * if(isUpdate&&check_record){ dialog.dismiss(); updateSession(name); }
		 * else if(isJoin){ dialog.dismiss(); updateSession(name); }else{
		 * dialog.dismiss(); v.getContext().startActivity(new
		 * Intent(JoinRecordActivity.this, HomePageActivity.class));
		 * overridePendingTransition(R.anim.push_bottom_in,
		 * R.anim.push_bottom_out); finish(); } } });
		 * 
		 * bt_cancel.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dialog.dismiss();
		 * if(check_record){ // Utils.delete(name_record); }
		 * v.getContext().startActivity(new Intent(JoinRecordActivity.this,
		 * HomePageActivity.class));
		 * overridePendingTransition(R.anim.push_bottom_in,
		 * R.anim.push_bottom_out); finish(); } }); dialog.show();
		 */
	}

	private void showDialogRecord() {
		DebugLog.loge("__" + name_record);
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_record);
		dialog.setTitle(getString(R.string.publish_artword));
		dialog.setCancelable(false);
		final EditText ed_name_record_dialog = (EditText) dialog.findViewById(R.id.ed_name_record_dialog);
		TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
		TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);

		tv_dialog_row_note.setText(getString(R.string.recored_mix));
		tv_dialog_row_newname.setVisibility(View.GONE);
		ed_name_record_dialog.setVisibility(View.GONE);

		Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);

		bt_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isFull = false;
				dialog.cancel();
				if (mPlayer != null && mPlayer.isPlaying()) {
					mPlayer.stop();
					mPlayer.release();
					mPlayer = null;
				}
				onPlay(mWaveformView.secondsToPixels(0.0), 1);
				record = 1;
				check_record = false;
				bt_record.setImageResource(R.drawable.bt_record_start);
				bt_done.setEnabled(false);
				bt_backward.setEnabled(false);
				bt_foward.setEnabled(false);
				bt_play.setEnabled(false);
				currentTimePlay = 0;
				isRecordAgain = true;
				bt_play.setImageResource(R.drawable.play_music_play);
				tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
				progressBarPlayRecord.setProgress(0);
				startRecording(false);
				startPlaying(CacheData.getInstant().getUrl_mix(), 0);
			}
		});

		bt_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	private void updateSession(String name) {
		if (check_record) {
			CacheData.getInstant().setUploadSession(true);
			CacheData.getInstant().setSid(sid);
			CacheData.getInstant().setName_record(name);

			if (isJoin) {
				CacheData.getInstant().setJoin(true);
			}
			startActivity(new Intent(JoinRecordActivity0.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			finish();
		} else {
			Utils.showToast(context, "Not Recording File!");
		}
	}

	private void initControls() {
		try {
			volumeSeekbar = (SeekBar) findViewById(R.id.seekbar_edit_volume);
			audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
					audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
				}
			});

			latency_text_1 = (TextView) findViewById(R.id.latency_text_1);
			latency_text_2 = (TextView) findViewById(R.id.latency_text_2);
			latency_text_1.setText("Vocal match");
			latency_text_2.setText("");
			latency_value = SharedPreference.loadLatency(JoinRecordActivity0.this);
			seekbar_latency = (SeekBar) findViewById(R.id.seekbar_latency);
			seekbar_latency.setProgress(latency_value + 425);
			seekbar_latency.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
					if (!check_record || record == 1) {
						seekbar_latency.setProgress(SharedPreference.loadLatency(JoinRecordActivity0.this) + 425);
						UtilLibs.showToast(JoinRecordActivity0.this, getString(R.string.please_record));
						return;
					}
					if (check_record) {
						UtilLibs.delayRun(200, new OnExecuteMethod() {

							@Override
							public void executeMethod() {
								startMix();
							}
						});
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
					if (!check_record || record == 1) {
						seekbar_latency.setProgress(SharedPreference.loadLatency(JoinRecordActivity0.this) + 425);
						return;
					}
					checkLatency(progress);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void editSeekbarPlay() {
		progressBarPlayRecord.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);
				if (play != 0) {
					int totalDuration = mPlayer.getDuration();
					int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);
					mPlayer.seekTo(currentPosition);

					updateprogressBarPlayRecord();
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			}
		});
	}

	boolean isshow = false, isConverted = false, isClickReplay = false;

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bt_record_voidmemos :
				// DebugLog.loge("\nrecord: " + record +
				// "\ncheck_record: "+check_record+"\nplay: "+play
				// +"\ncurrentDurationPlaying: " +currentDurationPlaying);
				if (play == 1) {
					Utils.showToast(context, "please pause the player!");
					break;
				}
				if (isJoin || isUpdate) {
					if (!handleHeadphonesState() && record != 1) {
						Utils.showToast(context, "please plug earphone!");
						break;
					}
				}
				if (CacheData.getInstant().isMix()) {
					if (isFull) {
						showDialogRecord();
						break;
					}
				}

				if (!isshow) {
					soundVisualr();
					isshow = true;
				}
				if (record == 0) {
					isClickReplay = false;
					DebugLog.loge("___startRecording___");
					if (mPlayer != null && mPlayer.isPlaying()) {
						mPlayer.stop();
					}
					record = 1;
					check_record = false;
					bt_record.setImageResource(R.drawable.bt_record_start);
					bt_done.setEnabled(false);
					bt_backward.setEnabled(false);
					bt_foward.setEnabled(false);
					bt_play.setEnabled(false);
					startRecording(false);
					onPlay(mWaveformView.secondsToPixels(0.0), 1);
					if (CacheData.getInstant().isMix()) {
						startPlaying(CacheData.getInstant().getUrl_mix(), 0);
					}
				} else if (record == 1) {
					DebugLog.loge("___stopRecording___");
					stopRecording(false);
					onPlay(-1, 2);
					check_record = true;
					record = 2;
					play = 0;

					bt_record.setImageResource(R.drawable.bt_record);
					bt_play.setImageResource(R.drawable.play_music_play);

					bt_play.setEnabled(true);
					bt_done.setEnabled(true);

					if (isJoin) {
						pausePlaying();
					}
				} else {
					DebugLog.loge("___resumeRecording___");
					record = 1;
					bt_record.setImageResource(R.drawable.bt_record_start);

					bt_done.setEnabled(false);

					bt_play.setEnabled(false);
					onPlay(mWaveformView.secondsToPixels(currentTimePlay), 1);
					startRecording(true);
					if (isJoin) {
						resumePlaying();
					}
				}
				break;
			case R.id.bt_play_voicememos :
				if (record == 1) {
					UtilLibs.showToast(JoinRecordActivity0.this, getString(R.string.please_record));
					return;
				}
				// DebugLog.loge("\nmixing: " + mixing +
				// "\ntimePause: "+timePause);
				if (check_record) {
					/*
					 * if(latency_value_temp != latency_value){ startMix(true);
					 * play =0; return; }
					 */
					if (play == 4)
						play = 0;
					if (play == 0) {
						isClickReplay = true;
						timePause = 0;
						play = 1;
						if (isFull && CacheData.getInstant().isMix()) {
							record = 0;
							stopRecording(true);
						}
						bt_play.setImageResource(R.drawable.bt_pause);
						bt_backward.setEnabled(true);
						bt_foward.setEnabled(true);
						startPlaying(Utils.path_FileOutMp3, 0);

					} else if (play == 1) {
						play = 2;
						bt_play.setImageResource(R.drawable.play_music_play);
						pausePlaying();
					} else if (play == 2) {
						play = 1;
						if (mPlayer != null) {
							mPlayer.start();
						} else {
							startPlaying(Utils.path_FileOutMp3, 0);
						}
						bt_play.setImageResource(R.drawable.bt_pause);

					} else if (play == 3) {
						play = 1;
						if (isFull && CacheData.getInstant().isMix()) {
							record = 0;
							stopRecording(true);
						}
						bt_play.setImageResource(R.drawable.bt_pause);
						bt_backward.setEnabled(true);
						bt_foward.setEnabled(true);
						startPlaying(Utils.path_FileOutMp3, timePause);
					}
				} else {
					UtilLibs.showToast(JoinRecordActivity0.this, getString(R.string.please_record));
				}
				break;
			case R.id.bt_backward_voicememos :
				if (!check_record || record == 1) {
					UtilLibs.showToast(JoinRecordActivity0.this, getString(R.string.please_record));
					return;
				}
				if (isJoin) {
					// sm.backward();
				}
				if (mPlayer != null && mPlayer.isPlaying()) {
					int currentPosition = mPlayer.getCurrentPosition();
					if (currentPosition + seekForwardTime <= mPlayer.getDuration()) {
						mPlayer.seekTo(currentPosition + seekForwardTime);
					} else {
						mPlayer.seekTo(mPlayer.getDuration());
					}
				}
				break;
			case R.id.bt_forward_voidmemos :
				if (!check_record || record == 1) {
					UtilLibs.showToast(JoinRecordActivity0.this, getString(R.string.please_record));
					return;
				}
				if (isJoin) {
					// sm.forward();
				}
				if (mPlayer != null && mPlayer.isPlaying()) {
					int currentPosition2 = mPlayer.getCurrentPosition();
					if (currentPosition2 - seekBackwardTime >= 0) {
						mPlayer.seekTo(currentPosition2 - seekBackwardTime);
					} else {
						mPlayer.seekTo(0);
					}
				}
				break;
			case R.id.bt_done_voicememos :
				if (CacheData.getInstant().isMix()) {
					if (!isFull) {
						Display localDisplay = getWindowManager().getDefaultDisplay();
						Toast localToast = Toast.makeText(JoinRecordActivity0.this, "Please finish the record", Toast.LENGTH_LONG);
						localToast.setGravity(48, 0, localDisplay.getHeight() / 8);
						localToast.show();
						return;
					}
				}
				stopRecording(true);
				showDialogDone();
				break;
			// TODO
			case R.id.latecy1 :
				final Dialog dialog1 = new Dialog(JoinRecordActivity0.this);
				Utils.showSetLatecy(dialog1, true, new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog1.dismiss();
						latecy_txt1.setText(CacheData.getInstant().getLatecy1() + "ms");
					}
				});
				break;

			case R.id.latecy2 :
				final Dialog dialog2 = new Dialog(JoinRecordActivity0.this);
				Utils.showSetLatecy(dialog2, false, new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog2.dismiss();
						latecy_txt2.setText(CacheData.getInstant().getLatecy2() + "ms");
					}
				});
				break;

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (play != 0) {
			mPlayer = null;
		}
	}

	@Override
	public void onBackPressed() {
		if (isRecording)
			return;
		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.stop();
			mPlayer = null;
			mHandler.removeCallbacks(mDrawThread);
		}
		if (isRecording) {
			stopRecording(true);
		}
		if (isFull) {
			showDialogDone();
		} else {
			startActivity(new Intent(JoinRecordActivity0.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
			finish();
		}
		// startActivity(new Intent(JoinRecordActivity.this,
		// HomePageActivity.class));
		// overridePendingTransition(R.anim.push_bottom_in,
		// R.anim.push_bottom_out);
		// finish();

	}

	int latency_value = 0, latency_value_temp = 0;
	void checkLatency(int value) {
		latency_value = value - 425;
		SharedPreference.saveLatency(JoinRecordActivity0.this, latency_value);
		if (latency_value > 0) {
			latency_text_1.setText("Move vocal later: ");
			latency_text_2.setText(latency_value + "ms");
		} else {
			latency_text_1.setText("Move vocal earlier: ");
			latency_text_2.setText(Math.abs(latency_value) + "ms");
		}
	}

	String getLatency1() {
		String result = "0";
		if (latency_value > 0) {
			return result;
		}
		String temp = String.valueOf(Math.abs(latency_value));
		if (temp.length() == 1) {
			result = "0.00" + temp;
		} else if (temp.length() == 2) {
			result = "0.0" + temp;
		} else if (temp.length() == 3) {
			result = "0." + temp;
		}
		return result;
	}

	String getLatency2() {
		String result = "0";
		if (latency_value < 0) {
			return result;
		}
		String temp = String.valueOf(latency_value);
		if (temp.length() == 1) {
			result = "0.00" + temp;
		} else if (temp.length() == 2) {
			result = "0.0" + temp;
		} else if (temp.length() == 3) {
			result = "0." + temp;
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	public Boolean handleHeadphonesState() {
		AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		if (am.isWiredHeadsetOn()) {
			return true;
		} else {
			return false;
		}
	}

	// TODO
	private WaveformView mWaveformView;
	private boolean mKeyDown;
	private int mWidth;
	private int mStartPos;
	private int mEndPos;
	private int mOffset;
	private int mOffsetGoal;
	private int mFlingVelocity;
	int mPlayStartMsec;
	int mPlayEndMsec;
	private boolean mTouchDragging;
	private float mDensity;

	private void buildGUI() {
		record = 0;
		mKeyDown = false;
		loadGui();

		finishOpeningSoundFile();
	}

	private void loadGui() {

		mWaveformView = (WaveformView) findViewById(R.id.waveform);
		mWaveformView.setListener(this);

		mWaveformView.setSoundFile();
		mWaveformView.recomputeHeights(mDensity);

		updateDisplay();
	}

	private void finishOpeningSoundFile() {
		mWaveformView.recomputeHeights(mDensity);

		mTouchDragging = false;
//		mWaveformView.setZoomLevel(0);
		mOffset = 0;
		mOffsetGoal = 0;
		mFlingVelocity = 0;
		resetPositions();

		updateDisplay();
	}

	private void resetPositions() {
		mStartPos = mWaveformView.secondsToPixels(0.0);
		mEndPos = mWaveformView.secondsToPixels(1000 * 60 * 60);
	}

	private synchronized void updateDisplay() {
		if (record == 1) {
			int now = Integer.parseInt(String.valueOf(currentTimePlay));
			int frames = mWaveformView.millisecsToPixels(now);
			// DebugLog.loge("frames: "+frames);
			mWaveformView.setPlayback(frames);
			setOffsetGoalNoUpdate(frames - mWidth / 2);
			if (now >= mPlayEndMsec) {
				handlePause();
			}
			if (!mTouchDragging) {
				int offsetDelta;

				if (mFlingVelocity != 0) {
					offsetDelta = mFlingVelocity / 30;
					if (mFlingVelocity > 80) {
						mFlingVelocity -= 80;
					} else if (mFlingVelocity < -80) {
						mFlingVelocity += 80;
					} else {
						mFlingVelocity = 0;
					}

					mOffset += offsetDelta;

					if (mOffset < 0) {
						mOffset = 0;
						mFlingVelocity = 0;
					}
					mOffsetGoal = mOffset;
				} else {
					offsetDelta = mOffsetGoal - mOffset;

					if (offsetDelta > 10)
						offsetDelta = offsetDelta / 10;
					else if (offsetDelta > 0)
						offsetDelta = 1;
					else if (offsetDelta < -10)
						offsetDelta = offsetDelta / 10;
					else if (offsetDelta < 0)
						offsetDelta = -1;
					else
						offsetDelta = 0;

					mOffset += offsetDelta;
				}
			}

			mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
			mWaveformView.invalidate();
		}
	}

	private void switchPlayPause() {
		// if (state == 1) {
		// play.setImageResource(android.R.drawable.ic_media_pause);
		// } else {
		// play.setImageResource(android.R.drawable.ic_media_play);
		// }
	}

	private void setOffsetGoalNoUpdate(int offset) {
		if (mTouchDragging) {
			return;
		}

		mOffsetGoal = offset;
		if (mOffsetGoal < 0)
			mOffsetGoal = 0;
	}

	private synchronized void handlePause() {
		// mWaveformView.setPlayback(-1);
		// time1 = time;
		record = 2;
		switchPlayPause();
	}

	private synchronized void handleStop() {
		mWaveformView.setPlayback(-1);
		resetPositions();
		switchPlayPause();
	}

	private synchronized void onPlay(int startPosition, int s) {
		if (s == 2) {
			handlePause();
			return;
		}
		if (s == 0) {
			handleStop();
			return;
		}

		try {
			mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
			if (startPosition < mStartPos) {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
			} else {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
			}

			record = 1;

			updateDisplay();
			switchPlayPause();
		} catch (Exception e) {
			return;
		}
	}

	@Override
	public void waveformFling(float vx) {
		mTouchDragging = false;
		mOffsetGoal = mOffset;
		mFlingVelocity = (int) (-vx);
		updateDisplay();
	}

	@Override
	public void waveformDraw() {
		mWidth = mWaveformView.getMeasuredWidth();
		// if (mOffsetGoal != mOffset && !mKeyDown)
		// updateDisplay();
		// else if (record == 1) {
		// updateDisplay();
		// } else if (mFlingVelocity != 0) {
		// updateDisplay();
		// }
		updateDisplay();
	}

}
