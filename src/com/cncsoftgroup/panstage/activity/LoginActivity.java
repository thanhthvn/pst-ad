package com.cncsoftgroup.panstage.activity;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.UserManager;
import com.cncsoftgroup.panstage.manager.UserManager.OnLoginAndRegisterResult;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.model.GraphUser;
import com.nmd.libs.DebugLog;

public class LoginActivity extends Activity implements OnClickListener {

	private Button bt_LoginFacebook;
	private TextView bt_Login_Register;
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private Context context;
	private LinearLayout lLayout;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		context=this;
		bt_LoginFacebook = (Button) findViewById(R.id.bt_LoginFacebook);
		bt_Login_Register = (TextView) findViewById(R.id.bt_Login_Register);
		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
		
		lLayout=(LinearLayout)findViewById(R.id.background_login);
		if(SharedPreference.loadRole(context).equals(getString(R.string.player))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_player_im_background));
		}else if(SharedPreference.loadRole(context).equals(getString(R.string.singer))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_singer_im_background));
		}else{
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_listener_im_background));
		}
		
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback,
						savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
			}
		}
		bt_Login_Register.setOnClickListener(this);
		bt_LoginFacebook.setOnClickListener(this);

		genKeyHash();
	}
@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_LoginFacebook:
			SharedPreference.hasLoginFacebook(context, false);
			login();
			break;
		case R.id.bt_Login_Register:
			startActivity(new Intent(LoginActivity.this, LoginRegisterLoginActivity.class));
			CacheData.getInstant().setLoginFacebook(false);
			overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			finish();
			break;
		}
	}

	private void genKeyHash() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.cncsoftgroup.panstage.activity", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (Exception e) {
			Log.d("KeyHash", "Error: " + e.getMessage().toString());
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Session.getActiveSession().addCallback(statusCallback);

	}

	@Override
	public void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
//		Log.e("CHECK", Session.getActiveSession().getAccessToken());
		if(!SharedPreference.isLoginFacebook(context)){
			new UserManager().login_facebook(Session.getActiveSession().getAccessToken(), SharedPreference.loadRole(LoginActivity.this), new OnLoginAndRegisterResult() {
				@Override
				public void onLoginAndRegisterMethod(boolean isSuccess) {
					if(isSuccess){
						SharedPreference.hasLoginFacebook(context, true);
						startActivity(new Intent(LoginActivity.this, HomePageActivity.class));
						finish();
					}
					else Utils.showToast(getApplicationContext(), "Login Failed");
				}
			});
			
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}


	private void login() {
		if (!SharedPreference.isLoginFacebook(context)) {
			final Session session = Session.getActiveSession();
			if (session != null && session.isOpened()) {
				
				CacheData.getInstant().setLoginFacebook(true);
				DebugLog.loge("ACCESS TOKEN: "+session.getAccessToken());
				
				 List<String> permissions = new ArrayList<String>();
				    permissions.add("email");
				    openActiveSession(this, true, new Session.StatusCallback() {
				        @SuppressWarnings("deprecation")
				     @Override
				        public void call(final Session session, SessionState state, Exception exception) {
				            if (session.isOpened()) {
				            	 Utils.showProgressDialog(context, "Login With Facebook....");
				                //make request to the /me API
				                Log.e("sessionopened", "true");
				                Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
				                    @Override
				                    public void onCompleted(GraphUser user, Response response) {
				                        if (user != null) {
				                        	new UserManager().login_facebook(session.getAccessToken(), SharedPreference.loadRoleRegister(LoginActivity.this), new OnLoginAndRegisterResult() {
				            					
				            					@Override
				            					public void onLoginAndRegisterMethod(boolean isSuccess) {
				            						if(isSuccess){
				            							Utils.dismissCurrentDialog();
				            							SharedPreference.hasLoginFacebook(context, true);
				            							startActivity(new Intent(LoginActivity.this, HomePageActivity.class));
				            							finish();
				            						}
				            						
				            						else {Utils.showToast(getApplicationContext(), "Login Failed");
				            						Utils.dismissCurrentDialog();
				            						}}
				            				});
				                        }
				                    }
				                });
				             }
				         }
				     }, permissions);
				
			} else {

				onClickLogin();
			}

		} else if (!CacheData.getInstant().getAcces_token().equals("")) {
//			new UserManager().login_facebook(CacheData.getInstant().getAcces_token(), SharedPreference.loadRole(LoginActivity.this), new OnLoginAndRegisterResult() {
//				@Override
//				public void onLoginAndRegisterMethod(boolean isSuccess) {
//					if(isSuccess){
//						startActivity(new Intent(LoginActivity.this, HomePageActivity.class));
//						finish();
//					}
//					else Utils.showToast(getApplicationContext(), "Login Failed");
//				}
//			});
			
		}
	}
	private static Session openActiveSession(Activity activity, boolean allowLoginUI, Session.StatusCallback callback, List<String> permissions) {
	    Session.OpenRequest openRequest = new Session.OpenRequest(activity).setPermissions(permissions).setCallback(callback);
	    Session session = new Session.Builder(activity).build();
	    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
	    	DebugLog.loge("TOKEN CHECK:\n"+SessionState.CREATED_TOKEN_LOADED);
	        Session.setActiveSession(session);
	        session.openForRead(openRequest);
	        return session;
	    }
	    return null;
	}

	private void onClickLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(this)
					.setCallback(statusCallback));
		} else {
			Session.openActiveSession(this, true, statusCallback);

		}
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			login();
		}
	}
}
