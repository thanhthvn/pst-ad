package com.cncsoftgroup.panstage.activity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import javazoom.jl.converter.Converter;
import javazoom.jl.converter.Converter.ProgressListener;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.Obuffer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.common.Utils.Keys;
import com.cncsoftgroup.panstage.soundvisualizer.CDrawer;
import com.cncsoftgroup.panstage.soundvisualizer.WaveformView;
import com.cncsoftgroup.panstage.view.LyricView;
import com.nmd.libs.DebugLog;
import com.nmd.libs.UtilLibs;
import com.nmd.libs.UtilLibs.OnExecuteMethod;
import com.uraroji.garage.android.lame.LameEncodeTask;
import com.uraroji.garage.android.lame.LameEncodeTask.OnEncodeResult;
import com.uraroji.garage.android.lame.SimpleLame;
import com.uraroji.garage.android.lame.SoxManager;
import com.uraroji.garage.android.lame.SoxManager.OnCombineMixResult;
import com.uraroji.garage.android.lame.SoxManager.OnConvertChFileResult;
import com.uraroji.garage.android.lame.SoxManager.OnPadFileResult;

public class JoinRecordActivity extends Activity implements OnClickListener, WaveformView.WaveformListener {
	static {
		System.loadLibrary("mp3lame");
	}

	boolean testDelay = false;

	private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".mp3";
	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private AudioRecord recorder = null;
	private int bufferSize = 0;
	private Thread recordingThread = null;
	private boolean isRecording = false;

	private MediaPlayer mPlayer_record = null;
	private MediaPlayer mPlayer = null;
	private SeekBar volumeSeekbar = null;
	private SeekBar seekbar_latency = null;
	private AudioManager audioManager = null;

	private ImageView bt_play, bt_record, bt_backward, bt_foward;
	private TextView tv_time_record, tv_time_play_record, tv_longtime_play, tv_record_name;
	ImageView bt_done;

	private int seekForwardTime = 5000;
	private int seekBackwardTime = 5000;

	private Handler mHandler = new Handler();

	private String name_record = "";
	private String file_name = "";
	boolean check_record = false;
	int play = 0, record = 0;

	private CDrawer.CDrawThread mDrawThread;
	private CDrawer mdrawer;

	private OnClickListener listener;
	private Boolean m_bStart = Boolean.valueOf(false);
	@SuppressWarnings("unused")
	private static short[] buffer_vusual;

	private SeekBar progressBarPlayRecord;
	private Context context;

	private LinearLayout lLayout_visual, latency_view_title, latency_view_bar;
	private String song_name;
	private int sid;
	private String url_mix = "";
	private Boolean isJoin = false;
	private Boolean isUpdate = false;
	TextView latecy_txt1, latecy_txt2, latency_text_1, latency_text_2;

	String full_name, authu_token;

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		try {
			View view = getCurrentFocus();
			boolean ret = super.dispatchTouchEvent(event);

			if (view instanceof EditText) {
				View w = getCurrentFocus();
				int scrcoords[] = new int[2];
				w.getLocationOnScreen(scrcoords);
				float x = event.getRawX() + w.getLeft() - scrcoords[0];
				float y = event.getRawY() + w.getTop() - scrcoords[1];

				if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
				}
			}
			return ret;
		} catch (Exception e) {
			return false;
		}
	}

	public JoinRecordActivity() {
		name_record = getFilenameRandom();
		file_name = Long.toString(System.currentTimeMillis());
	}

	private String getFilenameRandom() {
		return (Utils.path_Record_temp + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.voice_memos_join);
		context = this;

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mDensity = displaymetrics.density;
		buildGUI();

		full_name = SharedPreference.loadFullName(context);
		authu_token = SharedPreference.loadAuthu_token(context);

		lyric_layout = (FrameLayout) findViewById(R.id.lyric_layout);

		latecy_txt1 = (TextView) findViewById(R.id.latecy1);
		latecy_txt1.setOnClickListener(this);

		latecy_txt2 = (TextView) findViewById(R.id.latecy2);
		latecy_txt2.setOnClickListener(this);
		latecy_txt2.setText("   " + getString(R.string.lyric_join));

		if (testDelay) {
			latecy_txt1.setVisibility(View.VISIBLE);
			latecy_txt2.setVisibility(View.VISIBLE);
		} else {
			// latecy_txt1.setVisibility(View.GONE);
			// latecy_txt2.setVisibility(View.GONE);
		}

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		bt_play = (ImageView) findViewById(R.id.bt_play_voicememos);
		bt_record = (ImageView) findViewById(R.id.bt_record_voidmemos);
		bt_backward = (ImageView) findViewById(R.id.bt_backward_voicememos);
		bt_foward = (ImageView) findViewById(R.id.bt_forward_voidmemos);
		tv_time_record = (TextView) findViewById(R.id.tv_Time_Record);
		tv_longtime_play = (TextView) findViewById(R.id.tv_longtime_play_record);
		tv_time_play_record = (TextView) findViewById(R.id.tv_time_play_record);
		tv_record_name = (TextView) findViewById(R.id.tv_RecordName_voidmemos);
		bt_done = (ImageView) findViewById(R.id.bt_done_voicememos);

		progressBarPlayRecord = (SeekBar) findViewById(R.id.progressbar_play_record);
		isJoin = CacheData.getInstant().is_join_record();
		isUpdate = CacheData.getInstant().is_update_session();
		lLayout_visual = (LinearLayout) findViewById(R.id.ln_visual);
		latency_view_title = (LinearLayout) findViewById(R.id.latency_view_title);
		latency_view_bar = (LinearLayout) findViewById(R.id.latency_view_bar);

		// if(CacheData.getInstant().isJoinLyrics()){
		// latency_view_title.setVisibility(View.GONE);
		// latency_view_bar.setVisibility(View.GONE);
		// }else{
		latency_view_title.setVisibility(View.VISIBLE);
		latency_view_bar.setVisibility(View.VISIBLE);
		// }

		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

		if (isJoin || isUpdate) {
			if (CacheData.getInstant().isMix()) {
				Intent intent = getIntent();
				Bundle bundle = intent.getBundleExtra("session_media");

				song_name = bundle.getString("song_name");
				if (!CacheData.getInstant().isJoinLyrics()) {
					url_mix = CacheData.getInstant().getUrl_mix();
					DebugLog.loge("url_mix: \n" + url_mix);
					DebugLog.loge("duration: " + Utils.getDurationFile(CacheData.getInstant().getUrl_mix()));
					if (url_mix.equals("")) {
						onBackPressed();
						return;
					}
					sid = bundle.getInt("sid");
				}
				DebugLog.logd("=>>>>>>>" + sid);
				tv_record_name.setText(song_name);
			}
		} else {
			tv_record_name.setText(full_name);
		}

		buffer_vusual = new short[bufferSize];
		bt_done.setOnClickListener(this);
		bt_backward.setOnClickListener(this);
		bt_foward.setOnClickListener(this);
		bt_play.setOnClickListener(this);
		bt_record.setOnClickListener(this);
		initControls();
		editSeekbarPlay();
	}

	private void soundVisualr() {
		mdrawer = new CDrawer(context, null);
		lLayout_visual.addView(mdrawer);
		m_bStart = Boolean.valueOf(false);
	}

	public void setBuffer(short[] paramArrayOfShort) {
		mDrawThread = mdrawer.getThread();
		mDrawThread.setBuffer(paramArrayOfShort);
	}

	@Override
	protected void onPause() {
		if (mDrawThread != null) {
			mDrawThread.setRun(Boolean.valueOf(false));
			mDrawThread.SetSleeping(Boolean.valueOf(true));
			Boolean.valueOf(false);
		}
		super.onPause();
		if (recorder != null) {
			recorder.release();
			recorder = null;
		}
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
		if (CacheData.getInstant().isMix()) {
			CacheData.getInstant().setUrl_mix("");
		}
		CacheData.getInstant().setMix(false);
	}

	@Override
	protected void onRestart() {
		m_bStart = Boolean.valueOf(true);
		System.out.println("onRestart");
		super.onRestart();
	}

	private void runVisual() {

		int i = 0;
		while (true) {
			if (isRecording && (mdrawer.GetDead2().booleanValue())) {
				if (!m_bStart.booleanValue())
					mdrawer.Restart(Boolean.valueOf(true));
				mDrawThread.SetSleeping(Boolean.valueOf(false));
				m_bStart = Boolean.valueOf(false);
				return;
			}
			try {
				Thread.sleep(500L);
				System.out.println("Hang on..");
				i++;
				if (!mdrawer.GetDead2().booleanValue()) {
					System.out.println("mDrawer not DeAD!!");
					mdrawer.SetRun(Boolean.valueOf(false));
				}
				if (i <= 4)
					continue;
				mDrawThread.SetDead2(Boolean.valueOf(true));
			} catch (InterruptedException localInterruptedException) {
				localInterruptedException.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void run() {

		try {
			if (mDrawThread == null) {
				mDrawThread = mdrawer.getThread();
			}
			Context localContext = getApplicationContext();
			Display localDisplay = getWindowManager().getDefaultDisplay();
			Toast localToast = Toast.makeText(localContext, "Start Recording..", Toast.LENGTH_LONG);
			localToast.setGravity(48, 0, localDisplay.getHeight() / 8);
			localToast.show();
			mdrawer.setOnClickListener(listener);

		} catch (NullPointerException e) {
			Log.e("Main_Run", "NullPointer: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ====================Method Recording================================

	private void startRecording(final boolean b) {
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize * 2);

		isRecording = true;
		run();
		recorder.startRecording();
		runVisual();
		recordingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				writeAudioDataToFile(b);
			}
		}, "AudioRecorder Thread");

		recordingThread.start();

	}

//	SoxController soxController, soxController2;
	List<String> listFiles = Arrays.asList(Utils.path_FileA2, Utils.path_FileB2);

	private void stopRecording(boolean b) {
		if (recorder != null) {
			isRecording = false;

			recorder.stop();
			recorder.release();

			recorder = null;
			recordingThread = null;

			startMix();
		}
	}

	String latecy1 = "0";
	String latecy2 = "0";

	// TODO
	private void startMix() {
		play = 0;
		DebugLog.loge("---> isClickReplay: " + isClickReplay);
		if (isClickReplay) {
			if (mPlayer.isPlaying()) {
				bt_play.setImageResource(R.drawable.play_music_play);
				pausePlaying(true);
			}
		}
		if (CacheData.getInstant().isMix()) {
			latency_value_temp = SharedPreference.loadLatency(JoinRecordActivity.this);
			Utils.showProgressDialog(JoinRecordActivity.this, "Mixing sessions...");

			try {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						mp3ToWav();
					}
				}, 1000);

			} catch (Exception e) {
				if(e!=null) DebugLog.loge_online(e.getMessage());
			}

		}
	}

	private void mp3ToWav() {
		try {
			new Converter().convert(name_record, Utils.path_FileB1, new ProgressListener() {

				@Override
				public void readFrame(int arg0, Header arg1) {
				}

				@Override
				public void parsedFrame(int arg0, Header arg1) {
				}

				@Override
				public void decodedFrame(int arg0, Header arg1, Obuffer arg2) {
				}

				@Override
				public void converterUpdate(int arg0, int arg1, int arg2) {
					// DebugLog.loge("_Mix_" + "\n" + arg0 + "\n" + arg1 + "\n"
					// + arg2);
					if (arg2 > 1) {
						latecy1 = "0";
						if (testDelay) {
							padFile(CacheData.getInstant().getUrl_mix(), Utils.path_FileA2, String.valueOf((Double.parseDouble(latecy_txt1.getText().toString().replaceAll("ms", "")) / 1000)));
						} else {
							padFile(CacheData.getInstant().getUrl_mix(), Utils.path_FileA2, getLatency1());
						}
					}
				}

				@Override
				public boolean converterException(Throwable arg0) {
					return false;
				}
			});
		} catch (Exception e) {
			if(e!=null) DebugLog.loge_online(e.getMessage());
		}
	}
	
	void padFile(String input, String output, String lenth){
		SoxManager.padFile(JoinRecordActivity.this, input, output, lenth, new OnPadFileResult() {
			
			@Override
			public void onPadFileMethod(boolean isSuccess, String message) {
				if(isSuccess){
					if (testDelay) {
						convertChFile(Utils.path_FileB1, Utils.path_FileB2, String.valueOf((Double.parseDouble(latecy_txt2.getText().toString().replaceAll("ms", "")) / 1000)));
					} else {
						convertChFile(Utils.path_FileB1, Utils.path_FileB2, getLatency2());
					}
				}else{
					Utils.showToast(JoinRecordActivity.this, "error " + message);
					onBackPressed();
				}
			}
		});
	}
	
	void convertChFile(String input, String output, String lenth) {
		SoxManager.convertChFile(JoinRecordActivity.this, input, output, lenth, new OnConvertChFileResult() {
			
			@Override
			public void onConvertChFileMethod(boolean isSuccess, String message) {
				if(isSuccess){
					combineMix(listFiles, Utils.path_FileOut);
				}else{
					Utils.showToast(JoinRecordActivity.this, "error " + message);
					onBackPressed();
				}
			}
		});
	}
	
	void combineMix(List<String> files, String outFile) {
		SoxManager.combineMix(JoinRecordActivity.this, files, outFile, new OnCombineMixResult() {
			
			@Override
			public void onCombineMixMethod(boolean isSuccess, String message) {
				if(isSuccess){			
					wavToMp3();
				}else{
					Utils.showToast(JoinRecordActivity.this, "error " + message);
					onBackPressed();
				}
			}
		});
	}

	private short[] resize(short[] b) {
		short[] c = new short[b.length / 70];
		for (int i = 0; i < b.length / 70; i++) {
			c[i] = b[i];
		}
		return c;
	}

	int sample = 0;

	private void writeAudioDataToFile(boolean b) {
		try {
			String filename = name_record;
			short[] buffer = new short[RECORDER_SAMPLERATE * (16 / 8) * 1 * 5];
			byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];

			FileOutputStream os = null;
			try {
				os = new FileOutputStream(filename, b);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			SimpleLame.init(RECORDER_SAMPLERATE, 1, RECORDER_SAMPLERATE, 128);

			int read = 0;
			if (os != null) {
				while (isRecording) {
					read = recorder.read(buffer, 0, bufferSize);
					try {
						// track.write(buffer, 0, bufferSize);
					} catch (Exception e) {
					}
					setBuffer(resize(buffer));
					if (read < 0) {
					} else if (read == 0) {
					} else {
						int encResult = SimpleLame.encode(buffer, buffer, read, mp3buffer);
						sample = read;
						if (encResult < 0) {
						}
						if (encResult != 0) {
							try {
								os.write(mp3buffer, 0, encResult);
							} catch (IOException e) {
							}
						}
					}
				}
				mDrawThread.setRun(Boolean.valueOf(false));
				mDrawThread.SetSleeping(Boolean.valueOf(true));
				Boolean.valueOf(false);

				int flushResult = SimpleLame.flush(mp3buffer);
				if (flushResult < 0) {
				}
				if (flushResult != 0) {
					try {
						os.write(mp3buffer, 0, flushResult);
					} catch (IOException e) {
					}
				}

				try {
					os.close();
				} catch (IOException e) {
				}
			}
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}
	}

	private void wavToMp3() {
		String[] params = new String[] { Utils.path_FileOut.toString(), Utils.path_FileOutMp3.toString() };
		Utils.showProgressDialog(context, "Loading...");
		new LameEncodeTask(JoinRecordActivity.this, new OnEncodeResult() {

			@Override
			public void onEncodeMethod(boolean isSuccess, String message) {
				Utils.dismissCurrentDialog();
				if (isSuccess) {
					progressBarPlayRecord.setProgress(0);
					tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
					bt_play.setImageResource(R.drawable.play_music_play);
					if (mPlayer != null)
						mPlayer.stop();
					play = 0;

				} else {
					Utils.showToast(JoinRecordActivity.this, message);
				}
			}
		}).execute(params);
	}

	// ====================Method Recording End================================

	private void startPlayingRecord(String url, int time) {
		DebugLog.loge("song: " + url + "\ntime: " + time);
		destroyMediaPlayer(mPlayer);
//		mPlayer_record= getMediaPlayer(JoinRecordActivity.this);
		mPlayer_record = new MediaPlayer();
		
		try {
			mPlayer_record.reset();
			mPlayer_record.setDataSource(url);
			mPlayer_record.prepare();
			mPlayer_record.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					if (isRecording) {
						tv_time_record.setText(Utils.getDurationLongTime(0));
						stopPlaying(false);
						play = 0;
						DebugLog.loge("bt_record.performClick");
						bt_record.performClick();
						isFull = true;
					}

				}
			});
			mPlayer_record.start();
			isPausePlay = false;
			if (time > 0) {
				mPlayer_record.seekTo(time);
			}
			updateprogressBarPlayRecord();
		} catch (IOException e) {
			DebugLog.loge("prepare() failed");
		}
	}
	
	void destroyMediaPlayer(MediaPlayer mp){
//		if (mp != null) {
//			if(mp.isPlaying()) mp.stop();
//			mp.release();
//			mp = null;
//		}
	}

	private void startPlaying(String url) {
		DebugLog.loge("song: " + url);
		destroyMediaPlayer(mPlayer);
		mPlayer = new MediaPlayer();
//		mPlayer= getMediaPlayer(JoinRecordActivity.this);
//		DebugLog.loge("durration: "+Utils.getDurationFile(url));
		try {
			mPlayer.reset();
			mPlayer.setDataSource(url);
			mPlayer.prepare();
			mPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub

				}
			});
			mPlayer.start();
			isPausePlay = false;
			progressBarPlayRecord.setProgress(0);
			progressBarPlayRecord.setMax(100);
			if (!isRecording) {
				tv_longtime_play.setText(Utils.milliSecondsToTimer(mPlayer.getDuration()));
			}
			updateprogressBarPlayRecord();

		} catch (IOException e) {
			DebugLog.loge("prepare() failed");
		}
	}

	private void resumePlaying(boolean isReplay) {
		if (isReplay) {
			if (mPlayer != null) {
				if (!mPlayer.isPlaying()) {
					mPlayer.start();
					isPausePlay = false;
					updateprogressBarPlayRecord();
				}
				DebugLog.loge("resumePlaying");
			}
		} else {
			if (mPlayer_record != null) {
				if (!mPlayer_record.isPlaying()) {
					mPlayer_record.start();
					isPausePlay = false;
					if (!isFull && CacheData.getInstant().isMix()) {
						mPlayer_record.seekTo((int) currentTimePlay);
					}
					updateprogressBarPlayRecord();
				}
				DebugLog.loge("resumePlaying");
			}

		}
	}

	private int currentDurationPlaying = 0;

	private void pausePlaying(boolean isReplay) {
		if (isReplay) {
			if (mPlayer != null) {
				if (mPlayer.isPlaying()) {
					mPlayer.pause();
					isPausePlay = true;
				}
				DebugLog.loge("pausePlaying");
			}
		} else {
			if (mPlayer_record != null) {
				if (mPlayer_record.isPlaying()) {
					mPlayer_record.pause();
					isPausePlay = true;
				}
				DebugLog.loge("pausePlaying");
			}
		}
	}

	private void stopPlaying(boolean isReplay) {
		if (isReplay) {
			if (mPlayer != null) {
				mPlayer.stop();
				isPausePlay = true;
				mPlayer.release();
				mPlayer = null;
				DebugLog.loge("stopPlaying");
			}
		} else {
			if (mPlayer_record != null) {
				mPlayer_record.stop();
				isPausePlay = true;
				mPlayer_record.release();
				mPlayer_record = null;
				DebugLog.loge("stopPlaying");
			}
		}

	}

	boolean isFull = false;
	boolean isRecordAgain = false;
	long currentTimePlay = 0;
	// long delay;

	boolean isPausePlay = false;
	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
//			DebugLog.loge("isPausePlay: "+isPausePlay);
			if(isPausePlay) return;
			updateprogressBarPlayRecord();
			if (mPlayer != null) {
				if (mPlayer.isPlaying()) {
					if (!isRecording) {
						long totalDuration = mPlayer.getDuration();
						long currentDuration = mPlayer.getCurrentPosition();

						tv_time_play_record.setText(Utils.milliSecondsToTimer(currentDuration));

						int progress = (int) (Utils.getProgressPercentage(currentDuration, totalDuration));
						progressBarPlayRecord.setProgress(progress);
						if (progress == 100) {
							// DebugLog.loge(currentDuration+"\n"+progress);
							progressBarPlayRecord.setProgress(0);
							tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
							bt_play.setImageResource(R.drawable.play_music_play);
							mPlayer.stop();
							isPausePlay = true;
							play = 0;
							return;
						} else {
							if (currentDuration >= currentTimePlay) {
								bt_play.performClick();
								progressBarPlayRecord.setProgress(0);
								tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
								// bt_play.setImageResource(R.drawable.play_music_play);
								play = 4;
							}
						}
						if (CacheData.getInstant().isMix() && !isFull) {
							if (currentDuration >= currentDurationPlaying && currentDuration != 0 && currentDurationPlaying != 0) {
								play = 2;
								bt_play.setImageResource(R.drawable.play_music_play);
								pausePlaying(false);
								progressBarPlayRecord.setProgress(0);
								tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
								mPlayer.seekTo(0);
							}
						}
					}
				}
			}
			if (mPlayer_record != null) {
				if (mPlayer_record.isPlaying()) {
					if (isRecording) {
						if(mPlayer_record.getCurrentPosition()>0){
							currentTimePlay = mPlayer_record.getCurrentPosition();
						}
//						DebugLog.loge("-->" + currentTimePlay);
						// updateDisplay();
						tv_time_record.setText(Utils.milliSecondsToTimer(currentTimePlay));

					}
				}
			}
		}
	};

	public void updateprogressBarPlayRecord() {
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	Dialog dialog = null;

	private void showDialogDone() {
		dialog = new Dialog(context);
		Utils.showDialog3(JoinRecordActivity.this, dialog, getString(R.string.publish_artword), getString(R.string.upmix), "", "", "", new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (isUpdate && check_record) {
					updateSession(file_name);
				} else if (isJoin) {
					updateSession(file_name);
				} else {
					v.getContext().startActivity(new Intent(JoinRecordActivity.this, HomePageActivity.class));
					overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
					finish();
				}
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				bt_play.performClick();
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				v.getContext().startActivity(new Intent(JoinRecordActivity.this, HomePageActivity.class));
				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
				finish();
			}
		});
	}

	private void showDialogRecord() {
		DebugLog.loge("__" + name_record);
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_record);
		dialog.setTitle(getString(R.string.publish_artword));
		dialog.setCancelable(false);
		final EditText ed_name_record_dialog = (EditText) dialog.findViewById(R.id.ed_name_record_dialog);
		TextView tv_dialog_row_note = (TextView) dialog.findViewById(R.id.tv_dialog_row_note);
		TextView tv_dialog_row_newname = (TextView) dialog.findViewById(R.id.tv_dialog_row_newname);

		tv_dialog_row_note.setText(getString(R.string.recored_mix));
		tv_dialog_row_newname.setVisibility(View.GONE);
		ed_name_record_dialog.setVisibility(View.GONE);

		Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);

		bt_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isFull = false;
				dialog.cancel();
				if (mPlayer != null && mPlayer.isPlaying()) {
					mPlayer.stop();
					mPlayer.release();
					mPlayer = null;
				}
				onPlay(mWaveformView.secondsToPixels(0.0), 1);
				record = 1;
				check_record = false;
				bt_record.setImageResource(R.drawable.bt_record_start);
				bt_done.setEnabled(false);
				bt_backward.setEnabled(false);
				bt_foward.setEnabled(false);
				bt_play.setEnabled(false);
				currentTimePlay = 0;
				isRecordAgain = true;
				bt_play.setImageResource(R.drawable.play_music_play);
				tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
				progressBarPlayRecord.setProgress(0);
				startRecording(false);
				startPlayingRecord(CacheData.getInstant().getUrl_mix(), 0);
			}
		});

		bt_cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	private void updateSession(String name) {
		if (check_record) {
			CacheData.getInstant().setUploadSession(true);
			CacheData.getInstant().setSid(sid);
			CacheData.getInstant().setName_record(name);

			if (isJoin) {
				CacheData.getInstant().setJoin(true);
			}
			startActivity(new Intent(JoinRecordActivity.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			finish();
		} else {
			Utils.showToast(context, "Not Recording File!");
		}
	}

	private void initControls() {
		try {
			volumeSeekbar = (SeekBar) findViewById(R.id.seekbar_edit_volume);
			audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
					audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
				}
			});

			latency_text_1 = (TextView) findViewById(R.id.latency_text_1);
			latency_text_2 = (TextView) findViewById(R.id.latency_text_2);
			latency_text_1.setText("Vocal match");
			latency_text_2.setText("");
			latency_value = SharedPreference.loadLatency(JoinRecordActivity.this);
			seekbar_latency = (SeekBar) findViewById(R.id.seekbar_latency);
			seekbar_latency.setProgress(latency_value + 425);
			seekbar_latency.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
					if (!check_record || record == 1) {
						seekbar_latency.setProgress(SharedPreference.loadLatency(JoinRecordActivity.this) + 425);
						UtilLibs.showToast(JoinRecordActivity.this, getString(R.string.please_record));
						return;
					}
					if (check_record) {
						UtilLibs.delayRun(200, new OnExecuteMethod() {

							@Override
							public void executeMethod() {
								startMix();
							}
						});
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
					if (!check_record || record == 1) {
						seekbar_latency.setProgress(SharedPreference.loadLatency(JoinRecordActivity.this) + 425);
						return;
					}
					checkLatency(progress);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void editSeekbarPlay() {
		progressBarPlayRecord.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);
				if (play != 0) {
					int totalDuration = mPlayer.getDuration();
					int currentPosition = Utils.progressToTimer(seekBar.getProgress(), totalDuration);
					mPlayer.seekTo(currentPosition);

					updateprogressBarPlayRecord();
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				mHandler.removeCallbacks(mUpdateTimeTask);

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			}
		});
	}

	boolean checkShowWarning() {
		if (com.nmd.libs.SharedPreference.get(JoinRecordActivity.this, Keys.WARNING_JOIN.toString(), "0").equals("0")) {
			com.nmd.libs.SharedPreference.set(JoinRecordActivity.this, Keys.WARNING_JOIN.toString(), "1");
			return false;
		}
		return true;
	}

	Dialog showWarning = null;

	boolean isshow = false, isConverted = false, isClickReplay = false;

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_record_voidmemos:
			if (play == 1) {
				Utils.showToast(context, "please pause the player!");
				break;
			}
			if (!checkShowWarning()) {
				showWarning = new Dialog(JoinRecordActivity.this);
				Utils.showDialogOK(JoinRecordActivity.this, showWarning, "PanStage Music", "Use headphone to get the best recording sound", "OK", new OnClickListener() {

					@Override
					public void onClick(View v) {
						showWarning.dismiss();
						bt_record.performClick();
					}
				});
				return;
			}
			if (isJoin || isUpdate) {
				// if (!handleHeadphonesState() && record != 1) {
				// Utils.showToast(context, "please plug earphone!");
				// break;
				// }
			}
			if (CacheData.getInstant().isMix()) {
				if (isFull) {
					showDialogRecord();
					break;
				}
			}

			if (!isshow) {
				soundVisualr();
				isshow = true;
			}
			if (record == 0) {
				isClickReplay = false;
				DebugLog.loge("___startRecording___");
				if (mPlayer != null && mPlayer.isPlaying()) {
					mPlayer.stop();
				}
				record = 1;
				check_record = false;
				bt_record.setImageResource(R.drawable.bt_record_start);
				bt_done.setEnabled(false);
				bt_backward.setEnabled(false);
				bt_foward.setEnabled(false);
				bt_play.setEnabled(false);
				startRecording(false);
				onPlay(mWaveformView.secondsToPixels(0.0), 1);
				if (CacheData.getInstant().isMix()) {
					startPlayingRecord(CacheData.getInstant().getUrl_mix(), 0);
				}
			} else if (record == 1) {
				DebugLog.loge("___stopRecording___");
				stopRecording(false);
				onPlay(-1, 2);
				check_record = true;
				record = 2;
				play = 0;

				bt_record.setImageResource(R.drawable.bt_record);
				bt_play.setImageResource(R.drawable.play_music_play);

				bt_play.setEnabled(true);
				bt_done.setEnabled(true);

				if (isJoin) {
					pausePlaying(false);
				}
			} else {
				DebugLog.loge("___resumeRecording___");
				record = 1;
				bt_record.setImageResource(R.drawable.bt_record_start);

				bt_done.setEnabled(false);

				bt_play.setEnabled(false);
				onPlay(mWaveformView.secondsToPixels(currentTimePlay), 1);
				startRecording(true);
				if (isJoin) {
					resumePlaying(false);
				}
			}
			break;
		case R.id.bt_play_voicememos:
			if (record == 1) {
				UtilLibs.showToast(JoinRecordActivity.this, getString(R.string.please_record));
				return;
			}
			if (check_record) {
				if (play == 4)
					play = 0;
				if (play == 0) {
					isClickReplay = true;
					play = 1;
					if (isFull && CacheData.getInstant().isMix()) {
						record = 0;
						stopRecording(true);
					}
					bt_play.setImageResource(R.drawable.bt_pause);
					bt_backward.setEnabled(true);
					bt_foward.setEnabled(true);
					startPlaying(Utils.path_FileOutMp3);
					isPausePlay = false;
					updateprogressBarPlayRecord();
				} else if (play == 1) {
					play = 2;
					bt_play.setImageResource(R.drawable.play_music_play);
					pausePlaying(true);
					isPausePlay = true;
				} else if (play == 2) {
					play = 1;
					if (mPlayer != null) {
						mPlayer.start();
						bt_play.setImageResource(R.drawable.bt_pause);
					} else {
						startPlaying(Utils.path_FileOutMp3);
					}
					isPausePlay = false;
					updateprogressBarPlayRecord();
					isPausePlay = false;
				} else if (play == 3) {
					play = 1;
					if (isFull && CacheData.getInstant().isMix()) {
						record = 0;
						stopRecording(true);
					}
					bt_play.setImageResource(R.drawable.bt_pause);
					bt_backward.setEnabled(true);
					bt_foward.setEnabled(true);
					startPlaying(Utils.path_FileOutMp3);
					isPausePlay = false;
					updateprogressBarPlayRecord();
				}
			} else {
				UtilLibs.showToast(JoinRecordActivity.this, getString(R.string.please_record));
			}
			break;
		case R.id.bt_backward_voicememos:
			if (!check_record || record == 1) {
				UtilLibs.showToast(JoinRecordActivity.this, getString(R.string.please_record));
				return;
			}
			if (isJoin) {
				// sm.backward();
			}
			if (mPlayer != null && mPlayer.isPlaying()) {
				int currentPosition = mPlayer.getCurrentPosition();
				if (currentPosition + seekForwardTime <= mPlayer.getDuration()) {
					mPlayer.seekTo(currentPosition + seekForwardTime);
				} else {
					mPlayer.seekTo(mPlayer.getDuration());
				}
			}
			break;
		case R.id.bt_forward_voidmemos:
			if (!check_record || record == 1) {
				UtilLibs.showToast(JoinRecordActivity.this, getString(R.string.please_record));
				return;
			}
			if (isJoin) {
				// sm.forward();
			}
			if (mPlayer != null && mPlayer.isPlaying()) {
				int currentPosition2 = mPlayer.getCurrentPosition();
				if (currentPosition2 - seekBackwardTime >= 0) {
					mPlayer.seekTo(currentPosition2 - seekBackwardTime);
				} else {
					mPlayer.seekTo(0);
				}
			}
			break;
		case R.id.bt_done_voicememos:
			if (CacheData.getInstant().isMix()) {
				if (!isFull) {
					Display localDisplay = getWindowManager().getDefaultDisplay();
					Toast localToast = Toast.makeText(JoinRecordActivity.this, "Please finish the record", Toast.LENGTH_LONG);
					localToast.setGravity(48, 0, localDisplay.getHeight() / 8);
					localToast.show();
					return;
				}
			}
			stopRecording(true);
			showDialogDone();
			break;
		// TODO
		case R.id.latecy1:
			final Dialog dialog1 = new Dialog(JoinRecordActivity.this);
			Utils.showSetLatecy(dialog1, true, new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog1.dismiss();
					latecy_txt1.setText(CacheData.getInstant().getLatecy1() + "ms");
				}
			});
			break;

		case R.id.latecy2:

			showLyric();

			// final Dialog dialog2 = new Dialog(JoinRecordActivity.this);
			// Utils.showSetLatecy(dialog2, false, new OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// dialog2.dismiss();
			// latecy_txt2.setText(CacheData.getInstant().getLatecy2() +
			// "ms");
			// }
			// });
			break;

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (play != 0) {
			mPlayer = null;
		}
	}

	@Override
	public void onBackPressed() {
		Utils.dismissCurrentDialog();
		if (isShowLyric) {
			isShowLyric = false;
			Animation hide = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
			hide.setDuration(500);
			lyric_layout.startAnimation(hide);
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					lyric_layout.setVisibility(View.GONE);
					lyricView = null;
				}
			}, 495);
			return;
		}
		if (isRecording)
			return;
		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.stop();
			mPlayer = null;
			mHandler.removeCallbacks(mDrawThread);
		}
		if (isRecording) {
			stopRecording(true);
		}
		if (isFull) {
			showDialogDone();
		} else {
			startActivity(new Intent(JoinRecordActivity.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
			finish();
		}
	}

	int latency_value = 0, latency_value_temp = 0;

	void checkLatency(int value) {
		latency_value = value - 425;
		SharedPreference.saveLatency(JoinRecordActivity.this, latency_value);
		if (latency_value > 0) {
			latency_text_1.setText("Move vocal later: ");
			latency_text_2.setText(latency_value + "ms");
		} else {
			latency_text_1.setText("Move vocal earlier: ");
			latency_text_2.setText(Math.abs(latency_value) + "ms");
		}
	}

	String getLatency1() {
		String result = "0";
		if (latency_value > 0) {
			return result;
		}
		String temp = String.valueOf(Math.abs(latency_value));
		if (temp.length() == 1) {
			result = "0.00" + temp;
		} else if (temp.length() == 2) {
			result = "0.0" + temp;
		} else if (temp.length() == 3) {
			result = "0." + temp;
		}
		return result;
	}

	String getLatency2() {
		String result = "0";
		if (latency_value < 0) {
			return result;
		}
		String temp = String.valueOf(latency_value);
		if (temp.length() == 1) {
			result = "0.00" + temp;
		} else if (temp.length() == 2) {
			result = "0.0" + temp;
		} else if (temp.length() == 3) {
			result = "0." + temp;
		}
		return result;
	}

	@SuppressWarnings("deprecation")
	public Boolean handleHeadphonesState() {
		AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//		am.adjustStreamVolume(AudioManager.s, direction, flags);
		if (am.isWiredHeadsetOn()) {
			return true;
		} else {
			return false;
		}
	}

	// TODO
	private WaveformView mWaveformView;
	boolean mKeyDown;
	private int mWidth;
	private int mStartPos;
	private int mEndPos;
	private int mOffset;
	private int mOffsetGoal;
	private int mFlingVelocity;
	int mPlayStartMsec;
	int mPlayEndMsec;
	private boolean mTouchDragging;
	private float mDensity;

	private void buildGUI() {
		record = 0;
		mKeyDown = false;
		loadGui();

		finishOpeningSoundFile();
	}

	private void loadGui() {

		mWaveformView = (WaveformView) findViewById(R.id.waveform);
		mWaveformView.setListener(this);

		mWaveformView.setSoundFile();
		mWaveformView.recomputeHeights(mDensity);

		updateDisplays();
	}

	private void finishOpeningSoundFile() {
		mWaveformView.recomputeHeights(mDensity);

		mTouchDragging = false;
		mOffset = 0;
		mOffsetGoal = 0;
		mFlingVelocity = 0;
		resetPositions();

		updateDisplays();
	}

	private void resetPositions() {
		mStartPos = mWaveformView.secondsToPixels(0.0);
		mEndPos = mWaveformView.secondsToPixels(1000 * 60 * 60);
	}

	int cur_time = 0;

	private synchronized void updateDisplays() {
		if (record == 1) {
			int now = cur_time;
			int frames = mWaveformView.millisecsToPixels(now);
//			DebugLog.loge("frames: " + frames);
			mWaveformView.setPlayback(frames);
			setOffsetGoalNoUpdate(frames - mWidth / 2);
			if (now >= mPlayEndMsec) {
				handlePause();
			}
			if (!mTouchDragging) {
				int offsetDelta;

				if (mFlingVelocity != 0) {
					offsetDelta = mFlingVelocity / 30;
					if (mFlingVelocity > 80) {
						mFlingVelocity -= 80;
					} else if (mFlingVelocity < -80) {
						mFlingVelocity += 80;
					} else {
						mFlingVelocity = 0;
					}

					mOffset += offsetDelta;

					if (mOffset < 0) {
						mOffset = 0;
						mFlingVelocity = 0;
					}
					mOffsetGoal = mOffset;
				} else {
					offsetDelta = mOffsetGoal - mOffset;

					if (offsetDelta > 10)
						offsetDelta = offsetDelta / 10;
					else if (offsetDelta > 0)
						offsetDelta = 1;
					else if (offsetDelta < -10)
						offsetDelta = offsetDelta / 10;
					else if (offsetDelta < 0)
						offsetDelta = -1;
					else
						offsetDelta = 0;

					mOffset += offsetDelta;
				}
			}

			mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
			mWaveformView.invalidate();
		}
	}

	private void setOffsetGoalNoUpdate(int offset) {
		if (mTouchDragging) {
			return;
		}

		mOffsetGoal = offset;
		if (mOffsetGoal < 0)
			mOffsetGoal = 0;
	}

	private synchronized void handlePause() {
		// mWaveformView.setPlayback(-1);
		// time1 = time;
		record = 2;
	}

	private synchronized void handleStop() {
		mWaveformView.setPlayback(-1);
		resetPositions();
	}

	private synchronized void onPlay(int startPosition, int s) {
		if (s == 2) {
			handlePause();
			return;
		}
		if (s == 0) {
			handleStop();
			return;
		}

		try {
			mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
			if (startPosition < mStartPos) {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
			} else {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
			}

			record = 1;

			updateDisplays();
			startThread();
		} catch (Exception e) {
			return;
		}
	}

	@Override
	public void waveformFling(float vx) {
		mTouchDragging = false;
		mOffsetGoal = mOffset;
		mFlingVelocity = (int) (-vx);
		updateDisplays();
	}

	@Override
	public void waveformDraw() {
		mWidth = mWaveformView.getMeasuredWidth();
		updateDisplays();
	}

	Thread thread = null;

	void startThread() {
		if (thread == null) {
			thread = new Thread() {

				@Override
				public void run() {
					while (true) {
						if (record == 1) {
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									if (mPlayer_record != null) {
										cur_time = mPlayer_record.getCurrentPosition();
									}
									updateDisplays();
								}
							});
							try {
								thread.sleep(10);
							} catch (Exception e) {
								return;
							}
						}
					}
				}
			};
			thread.start();
		}
	}

	// TODO
	FrameLayout lyric_layout;
	LyricView lyricView = null;
	boolean isShowLyric = false;

	void showLyric() {
		if (isShowLyric) {
			return;
		} else {
			isShowLyric = true;
		}
		if (lyricView == null) {
			lyricView = new LyricView(JoinRecordActivity.this);
			lyric_layout.addView(lyricView);
		}
		Animation show = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
		show.setDuration(500);
		lyric_layout.setVisibility(View.VISIBLE);
		lyric_layout.startAnimation(show);
	}
	
	static MediaPlayer getMediaPlayer(Context context){

	    MediaPlayer mediaplayer = new MediaPlayer();

	    if (android.os.Build.VERSION.SDK_INT < 19) {
	        return mediaplayer;
	    }

	    try {
	        Class<?> cMediaTimeProvider = Class.forName( "android.media.MediaTimeProvider" );
	        Class<?> cSubtitleController = Class.forName( "android.media.SubtitleController" );
	        Class<?> iSubtitleControllerAnchor = Class.forName( "android.media.SubtitleController$Anchor" );
	        Class<?> iSubtitleControllerListener = Class.forName( "android.media.SubtitleController$Listener" );

	        Constructor constructor = cSubtitleController.getConstructor(new Class[]{Context.class, cMediaTimeProvider, iSubtitleControllerListener});

	        Object subtitleInstance = constructor.newInstance(context, null, null);

	        Field f = cSubtitleController.getDeclaredField("mHandler");

	        f.setAccessible(true);
	        try {
	            f.set(subtitleInstance, new Handler());
	        }
	        catch (IllegalAccessException e) {return mediaplayer;}
	        finally {
	            f.setAccessible(false);
	        }

	        Method setsubtitleanchor = mediaplayer.getClass().getMethod("setSubtitleAnchor", cSubtitleController, iSubtitleControllerAnchor);

	        setsubtitleanchor.invoke(mediaplayer, subtitleInstance, null);
	        //Log.e("", "subtitle is setted :p");
	    } catch (Exception e) {}

	    return mediaplayer;
	}
}
