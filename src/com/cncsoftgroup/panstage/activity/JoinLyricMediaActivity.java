package com.cncsoftgroup.panstage.activity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.soundvisualizer.CDrawer;
import com.cncsoftgroup.panstage.soundvisualizer.WaveformView;
import com.cncsoftgroup.panstage.view.LyricView;
import com.nmd.libs.DebugLog;
import com.uraroji.garage.android.lame.SimpleLame;

public class JoinLyricMediaActivity extends Activity implements OnClickListener , WaveformView.WaveformListener{
	static {
		System.loadLibrary("mp3lame");
	}
	
	boolean isTest = false;
	
	private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".mp3";
	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private AudioRecord recorder = null;
	private int bufferSize = 0;
	private Thread recordingThread = null;
	private boolean isRecording = false;

	private MediaPlayer mPlayer = null;
	private SeekBar volumeSeekbar = null;
	private AudioManager audioManager = null;

	private ImageView bt_play, bt_record, bt_backward, bt_foward;
	TextView tv_time_record, tv_time_play_record, tv_longtime_play, tv_record_name, latecy_txt2;
	ImageView bt_done;

	private int seekForwardTime = 5000;
	private int seekBackwardTime = 5000;

	private Handler mHandler = new Handler();

	private String name_record = "";
	boolean check_record = false;
	int play = 0, record = 0;

	private CDrawer.CDrawThread mDrawThread;
	private CDrawer mdrawer;

	private OnClickListener listener;
	private Boolean m_bStart = Boolean.valueOf(false);

	private SeekBar progressBarPlayRecord;
	private Context context;

	private LinearLayout lLayout_visual, ln_volume;
	
	String full_name,authu_token; 
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		try {
			View view = getCurrentFocus();
			boolean ret = super.dispatchTouchEvent(event);

			if (view instanceof EditText) {
				View w = getCurrentFocus();
				int scrcoords[] = new int[2];
				w.getLocationOnScreen(scrcoords);
				float x = event.getRawX() + w.getLeft() - scrcoords[0];
				float y = event.getRawY() + w.getTop() - scrcoords[1];

				if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
				}
			}
			return ret;
		} catch (Exception e) {
			return false;
		}
	}
	
	public JoinLyricMediaActivity() {
		name_record = getFilename();
		DebugLog.loge("name_record:\n"+name_record);
	}

	private String getFilename() {
		return (Utils.path_Record_temp+ "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.voice_memos_record);
		context = this;
		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		mDensity = displaymetrics.density;
		buildGUI();
		
		full_name=SharedPreference.loadFullName(context);
		authu_token=SharedPreference.loadAuthu_token(context);
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		bt_play = (ImageView) findViewById(R.id.bt_play_voicememos);
		bt_record = (ImageView) findViewById(R.id.bt_record_voidmemos);
		bt_backward = (ImageView) findViewById(R.id.bt_backward_voicememos);
		bt_foward = (ImageView) findViewById(R.id.bt_forward_voidmemos);

		tv_time_record = (TextView) findViewById(R.id.tv_Time_Record);
		tv_longtime_play = (TextView) findViewById(R.id.tv_longtime_play_record);
		tv_time_play_record = (TextView) findViewById(R.id.tv_time_play_record);
		tv_record_name = (TextView) findViewById(R.id.tv_RecordName_voidmemos);
		bt_done = (ImageView) findViewById(R.id.bt_done_voicememos);

		progressBarPlayRecord = (SeekBar) findViewById(R.id.progressbar_play_record);
		lLayout_visual = (LinearLayout) findViewById(R.id.ln_visual);
		ln_volume = (LinearLayout) findViewById(R.id.ln_volume);
		ln_volume.setVisibility(View.GONE);
		
		latecy_txt2 = (TextView) findViewById(R.id.latecy2);
		latecy_txt2.setOnClickListener(this);
		latecy_txt2.setText("   " + getString(R.string.lyric_join));
		latecy_txt2.setVisibility(View.VISIBLE);
		lyric_layout = (FrameLayout) findViewById(R.id.lyric_layout);
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				showLyric();
			}
		}, 500);
		
//		bufferSize = 240;
		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
		DebugLog.loge("bufferSize: "+bufferSize);
		
		tv_record_name.setText(full_name);

		((LinearLayout) findViewById(R.id.player1)).setVisibility(View.GONE);
		((LinearLayout) findViewById(R.id.player2)).setVisibility(View.GONE);
		
		bt_done.setOnClickListener(this);
		bt_backward.setOnClickListener(this);
		bt_foward.setOnClickListener(this);
		bt_play.setOnClickListener(this);
		bt_record.setOnClickListener(this);
		initControls();
		editSeekbarPlay();

		if (CacheData.getInstant().isMix()) {
			Intent intent = getIntent();
			Bundle bundle = intent.getBundleExtra("session_media");

			song_name = bundle.getString("song_name");
			sid = bundle.getInt("sid");
			DebugLog.loge("=>>>>>>>" + sid);
			tv_record_name.setText(song_name);
		}
	}
	
	String song_name = "";
	int sid = -1;

	private void soundVisualr() {
		mdrawer = new CDrawer(context, null);
		lLayout_visual.addView(mdrawer);
		m_bStart = Boolean.valueOf(false);

	}

	public void setBuffer(short[] paramArrayOfShort) {
		mDrawThread = mdrawer.getThread();
		mDrawThread.setBuffer(paramArrayOfShort);
	}

	@Override
	protected void onPause() {
		if (mDrawThread != null) {
			mDrawThread.setRun(Boolean.valueOf(false));
			mDrawThread.SetSleeping(Boolean.valueOf(true));
			Boolean.valueOf(false);
		}
		super.onPause();
		if (recorder != null) {
			recorder.release();
			recorder = null;
		}
		
		try {
			if(track != null){
	            track.release();
	            track= null;
			}				
		} catch (Exception e) {
		}
		

		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	int dur = 0;

	@Override
	protected void onRestart() {
		m_bStart = Boolean.valueOf(true);
		System.out.println("onRestart");
		super.onRestart();
	}

	private void runVisual() {

		int i = 0;
		while (true) {
			if (isRecording && (mdrawer.GetDead2().booleanValue())) {
				if (!m_bStart.booleanValue())
					mdrawer.Restart(Boolean.valueOf(true));
				mDrawThread.SetSleeping(Boolean.valueOf(false));
				m_bStart = Boolean.valueOf(false);
				return;
			}
			try {
				Thread.sleep(500L);
				System.out.println("Hang on..");
				i++;
				if (!mdrawer.GetDead2().booleanValue()) {
					System.out.println("mDrawer not DeAD!!");
					mdrawer.SetRun(Boolean.valueOf(false));
				}
				if (i <= 4)
					continue;
				mDrawThread.SetDead2(Boolean.valueOf(true));
			} catch (InterruptedException localInterruptedException) {
				localInterruptedException.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void run() {

		try {
			if (mDrawThread == null) {
				mDrawThread = mdrawer.getThread();
			}
			Context localContext = getApplicationContext();
			Display localDisplay = getWindowManager().getDefaultDisplay();
			Toast localToast = Toast.makeText(localContext, "Start Recording..", Toast.LENGTH_LONG);
			localToast.setGravity(48, 0, localDisplay.getHeight() / 8);
			localToast.show();
			mdrawer.setOnClickListener(listener);

		} catch (NullPointerException e) {
			Log.e("Main_Run", "NullPointer: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ====================Method Recording================================
	
	AudioTrack track = null;
	
	private void startRecording(final boolean b) {
		if(isTest){
			return;
		}
		setVolume(false);
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize * 2);
        track = new AudioTrack(AudioManager.STREAM_MUSIC, RECORDER_SAMPLERATE, AudioFormat.CHANNEL_OUT_MONO, RECORDER_AUDIO_ENCODING, bufferSize * 2, AudioTrack.MODE_STREAM);
        
		isRecording = true;
		run();
		recorder.startRecording();
		if(handleHeadphonesState()){
			try {
				setVolume(true);
				track.play();				
			} catch (Exception e) {
			}
		}
		runVisual();
		recordingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				writeAudioDataToFile(b);
			}
		}, "AudioRecorder Thread");

		recordingThread.start();

	}

	private void stopRecording(boolean b) {
		if(isTest){
			return;
		}
		if (recorder != null) {
			isRecording = false;

			recorder.stop();
			recorder.release();
			
			recorder = null;
			recordingThread = null;
			
			try {
				track.stop();
				track.release();
				track = null;					
			} catch (Exception e) {
			}
		}
	}
	
	private short[] resize(short[] b) {
		short[] c = new short[b.length / 70];
		for (int i = 0; i < b.length / 70; i++) {
			c[i] = b[i];
		}
		return c;
	}

	int sample = 0;
	private void writeAudioDataToFile(boolean b) {
		try {
			String filename = name_record;
			short[] buffer = new short[RECORDER_SAMPLERATE * (16 / 8) * 1 * 5];
			byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];

			FileOutputStream os = null;
			try {
				os = new FileOutputStream(filename, b);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			SimpleLame.init(RECORDER_SAMPLERATE, 1, RECORDER_SAMPLERATE, 128);

			int read = 0;
			if (os != null) {
				while (isRecording) {
					read = recorder.read(buffer, 0, bufferSize);
					try {
						track.write(buffer, 0, bufferSize);							
					} catch (Exception e) {
						// TODO: handle exception
					}

					setBuffer(resize(buffer));
					if (read < 0) {
					} else if (read == 0) {
					} else {
						int encResult = SimpleLame.encode(buffer, buffer, read, mp3buffer);
						sample = read;
						if (encResult < 0) {
						}
						if (encResult != 0) {
							try {
								os.write(mp3buffer, 0, encResult);
							} catch (IOException e) {
							}
						}
					}
				}
				mDrawThread.setRun(Boolean.valueOf(false));
				mDrawThread.SetSleeping(Boolean.valueOf(true));
				Boolean.valueOf(false);

				int flushResult = SimpleLame.flush(mp3buffer);
				if (flushResult < 0) {
				}
				if (flushResult != 0) {
					try {
						os.write(mp3buffer, 0, flushResult);
					} catch (IOException e) {
					}
				}

				try {
					os.close();
				} catch (IOException e) {
				}
			}			
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}
	}
	
	// ====================Method Recording End================================

	private void startPlaying(String url) {
		mPlayer = new MediaPlayer();
		try {
			mPlayer.reset();
			mPlayer.setDataSource(	url);
			mPlayer.prepare();
			mPlayer.start();
			progressBarPlayRecord.setProgress(0);
			progressBarPlayRecord.setMax(100);
			if(!isRecording){
//				UpdateprogressBarPlayRecord();
//				tv_time_record.setText(Utils.milliSecondsToTimer(mPlayer.getDuration()));				
			}
			dur = mPlayer.getDuration()/1000;	
			DebugLog.loge(mPlayer.getDuration() + "__" + currtime);

		} catch (IOException e) {
			Log.e("LOG_TAG", "prepare() failed");
		}
	}
	
//	private int currentDurationPlaying = 0;
	private void pausePlaying() {
		if(mPlayer!=null){
			if(mPlayer.isPlaying()){
				mPlayer.pause();
			}
			DebugLog.loge("pausePlaying");			
		}
	}
	
//	private void stopPlaying() {
//		if(mPlayer!=null){
//			mPlayer.stop();
//			mPlayer.release();
//			mPlayer = null;
//			DebugLog.loge("stopPlaying");			
//		}
//	}
	
	boolean status = false;
	Thread thread = null;
	long currtime = 0;
	long timer = 0;
	
	public void timeRecord(boolean statusR) {
		status = statusR;
		if(status){
			if(oldtime>0){
				timer = System.currentTimeMillis() - oldtime;
				oldtime = 0;
			}
			startTimeRecord();
		}else{
			
		}
	}
	
	long oldtime = 0;
	public void startTimeRecord() {
		if(thread == null){
			thread = new Thread() {

				@Override
				public void run() {
					try {
						while (true) {
							if(status){
								currtime = (System.currentTimeMillis() - timer);
								
								oldtime = currtime;
							}
							try {
								Thread.sleep(50);
							} catch (Exception e) {
								return;
							}
							if (currtime % 1000 >= 50) {
								runOnUiThread(new Runnable() {
									
									@Override
									public void run() {
										if(status){
											tv_time_record.setText(Utils.milliSecondsToTimer(currtime));
										}
									}
								});
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			if(!thread.isAlive()){
				thread.start();
			}
		}
	}

	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			if (mPlayer != null) {
				if(!isRecording){
					long totalDuration = mPlayer.getDuration();
					long currentDuration = mPlayer.getCurrentPosition();

					tv_time_play_record.setText(Utils.milliSecondsToTimer(currentDuration));

					int progress = (int) (Utils.getProgressPercentage(currentDuration, totalDuration));
					progressBarPlayRecord.setProgress(progress);
					mHandler.postDelayed(this, 100);
					if (progress == 100) {
						progressBarPlayRecord.setProgress(0);
						tv_time_play_record.setText(Utils.milliSecondsToTimer(0));
						bt_play.setImageResource(R.drawable.play_music_play);
						mPlayer.stop();
						play = 0;
						return;
					}
				}
			}
		}
	};

	public void UpdateprogressBarPlayRecord() {
		if(!isRecording){
			mHandler.postDelayed(mUpdateTimeTask, 100);			
		}
	}

//	private void showDialogDone() {
//		DebugLog.loge("__"+name_record);
//		final Dialog dialog = new Dialog(context);
//		dialog.setContentView(R.layout.custom_dialog_record);
//		dialog.setCancelable(false);
//		final EditText ed_name_record_dialog = (EditText) dialog.findViewById(R.id.ed_name_record_dialog);
//		TextView tv_dialog_row_note=(TextView)dialog.findViewById(R.id.tv_dialog_row_note);
//		
//		dialog.setTitle(getString(R.string.newsession));
//		tv_dialog_row_note.setText(getString(R.string.new_name));
//		
//		Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok_dialog);
//		Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel_dialog);
//		
//		ed_name_record_dialog.setText("");
//		
//		bt_ok.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				String name = ed_name_record_dialog.getText().toString();
//				if(name.equals("")){
//					Utils.showErrorNullOrEmpty(ed_name_record_dialog, "Name Record Not Null.");
//					return;
//				}else{
//					Utils.renameRecord(Utils.getFileName(name_record), name);
//				}
//				
//				dialog.dismiss();
//				v.getContext().startActivity(new Intent(JoinLyricMediaActivity.this, RecordMediaListActivity.class));
//				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
//				finish();
//			}
//		});
//
//		bt_cancel.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//				Utils.delete(name_record);
//				startActivity(new Intent(JoinLyricMediaActivity.this, RecordMediaListActivity.class));			
//				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
//				finish();
////				if(check_record){
////				}
////				v.getContext().startActivity(new Intent(RecordMediaActivity.this, RecordMediaListActivity.class));
////				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
////				finish();
//			}
//		});
//		try {
//			dialog.show();			
//		} catch (Exception e) {}
//	}
	
	Dialog dialog = null;
	private void showDialogDone() {
		dialog = new Dialog(context);
		Utils.showDialog2(JoinLyricMediaActivity.this, dialog, getString(R.string.publish_artword), getString(R.string.upmix), "", "", 
				new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (check_record) {
					updateSession(name_record);
				} else {
					v.getContext().startActivity(new Intent(JoinLyricMediaActivity.this, HomePageActivity.class));
					overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
					finish();
				}
			}
		}, new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				v.getContext().startActivity(new Intent(JoinLyricMediaActivity.this, HomePageActivity.class));
				overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
				finish();
			}
		});
	}
	
	private void updateSession(String name) {
		if (check_record) {
			CacheData.getInstant().setUploadSession(true);
			CacheData.getInstant().setSid(sid);
			CacheData.getInstant().setName_record(name);
			DebugLog.loge("sid:\n"+sid+"\nname:\n"+name);
			CacheData.getInstant().setJoin(true);
			startActivity(new Intent(JoinLyricMediaActivity.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
			finish();
		} else {
			Utils.showToast(context, "Not Recording File!");
		}
	}
	
	private void initControls() {
		try {
			volumeSeekbar = (SeekBar) findViewById(R.id.seekbar_edit_volume);
			audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			volumeSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
			volumeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
						@Override
						public void onStopTrackingTouch(SeekBar arg0) {
						}

						@Override
						public void onStartTrackingTouch(SeekBar arg0) {
						}

						@Override
						public void onProgressChanged(SeekBar arg0,
								int progress, boolean arg2) {
							audioManager.setStreamVolume(
									AudioManager.STREAM_MUSIC, progress, 0);
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void editSeekbarPlay() {
		progressBarPlayRecord.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						mHandler.removeCallbacks(mUpdateTimeTask);
						if (play != 0) {
							int totalDuration = mPlayer.getDuration();
							int currentPosition = Utils.progressToTimer(
									seekBar.getProgress(), totalDuration);
							mPlayer.seekTo(currentPosition);
							
//							if(isJoin){
//								sm.seekTo(currentPosition);
//							}
							
							UpdateprogressBarPlayRecord();
						}
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						mHandler.removeCallbacks(mUpdateTimeTask);

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
					}
				});
	}

	boolean isshow = false, isConverted = false;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_record_voidmemos:
			if (!isshow) {
				soundVisualr();
				isshow = true;
			}
			if (record == 0) {
				DebugLog.loge("___startRecording___");
				if (mPlayer != null && mPlayer.isPlaying()) {
					mPlayer.stop();
				}
				
				timer = System.currentTimeMillis();
				timeRecord(true);
				
				record = 1;
				check_record = false;
				bt_record.setImageResource(R.drawable.bt_record_start);
				bt_done.setEnabled(false);
				bt_backward.setEnabled(false);
				bt_foward.setEnabled(false);
				bt_play.setEnabled(false);
				startRecording(false);
				onPlay(mWaveformView.secondsToPixels(0.0), 1);
			} else if (record == 1) {
				DebugLog.loge("___stopRecording___");
				onPlay(-1 , 2);
				stopRecording(false);
				check_record = true;
				record = 2;
				play = 0;
				bt_record.setImageResource(R.drawable.bt_record);
				bt_play.setEnabled(true);
				bt_done.setEnabled(true);
				
				timeRecord(false);
			} else {
				DebugLog.loge("___resumeRecording___");
				record = 1;
				timeRecord(true);
				onPlay(mWaveformView.secondsToPixels(currtime) ,1);
				
				bt_record.setImageResource(R.drawable.bt_record_start);
				bt_play.setImageResource(R.drawable.play_music_play);

				bt_done.setEnabled(false);
				bt_play.setEnabled(false);

				startRecording(true);
			}
			break;
		case R.id.bt_play_voicememos:
			if (check_record) {
				if (play == 0) {
					play = 1;
					bt_play.setImageResource(R.drawable.bt_pause);
					bt_backward.setEnabled(true);
					bt_foward.setEnabled(true);
					startPlaying(name_record);
					
				} else if (play == 1) {
					play = 2;
					bt_play.setImageResource(R.drawable.play_music_play);
					pausePlaying();
				} else {
					play = 1;
					mPlayer.start();
					bt_play.setImageResource(R.drawable.bt_pause);
					
				}
			}
			break;
		case R.id.bt_backward_voicememos:
			if (mPlayer != null && mPlayer.isPlaying()) {
				int currentPosition = mPlayer.getCurrentPosition();
				if (currentPosition + seekForwardTime <= mPlayer.getDuration()) {
					mPlayer.seekTo(currentPosition + seekForwardTime);
				} else {
					mPlayer.seekTo(mPlayer.getDuration());
				}
			}
			break;
		case R.id.bt_forward_voidmemos:
			if (mPlayer != null && mPlayer.isPlaying()) {
				int currentPosition2 = mPlayer.getCurrentPosition();
				if (currentPosition2 - seekBackwardTime >= 0) {
					mPlayer.seekTo(currentPosition2 - seekBackwardTime);
				} else {
					mPlayer.seekTo(0);
				}
			}
			break;
		case R.id.bt_done_voicememos:
			stopRecording(true);
			showDialogDone();
			timer = 0;
			break;
		case R.id.latecy2 :
			showLyric();

			break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (play != 0) {
			mPlayer = null;
		}
	}

	@Override
	public void onBackPressed() {
		if(isRecording) return;
		if (isShowLyric) {
			isShowLyric = false;
			Animation hide = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
			hide.setDuration(500);
			lyric_layout.startAnimation(hide);
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					lyric_layout.setVisibility(View.GONE);
					lyricView = null;
				}
			}, 495);
			return;
		}
		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.stop();
			mPlayer = null;
			mHandler.removeCallbacks(mDrawThread);
		}
		if (isRecording) {
			stopRecording(true);
			timeRecord(false);
		}
		showDialogDone();
//		startActivity(new Intent(RecordMediaActivity.this, RecordMediaListActivity.class));			
//		overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
//		finish();

	}
	
	@SuppressWarnings("deprecation")
	public Boolean handleHeadphonesState(){
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

		if(am.isWiredHeadsetOn()) {
			return true;
		} else{
			return false;
		}
	}
	
	public void setVolume(boolean isPlayRecord){
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		int maxVol = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		
		if(isPlayRecord){
			int vol = am.getStreamVolume(AudioManager.STREAM_MUSIC);
			int setVol = vol/2;
			if(setVol<5) setVol = 5;
			DebugLog.loge("volume: "+vol);
			am.setStreamVolume(AudioManager.STREAM_MUSIC, setVol, 0);
			volumeSeekbar.setProgress(setVol);
		}else{
			am.setStreamVolume(AudioManager.STREAM_MUSIC, maxVol, 0);
			volumeSeekbar.setProgress(100);
		}
	}
	
	// TODO
		private WaveformView mWaveformView;
		boolean mKeyDown;
		private int mWidth;
		private int mStartPos;
		private int mEndPos;
		private int mOffset;
		private int mOffsetGoal;
		private int mFlingVelocity;
		int mPlayStartMsec;
		int mPlayEndMsec;
		private boolean mTouchDragging;
		private float mDensity;

		private void buildGUI() {
			record = 0;
			mKeyDown = false;
			loadGui();

			finishOpeningSoundFile();
		}

		private void loadGui() {

			mWaveformView = (WaveformView) findViewById(R.id.waveform);
			mWaveformView.setListener(this);

			mWaveformView.setSoundFile();
			mWaveformView.recomputeHeights(mDensity);

			updateDisplay();
		}

		private void finishOpeningSoundFile() {
			mWaveformView.recomputeHeights(mDensity);

			mTouchDragging = false;
			mOffset = 0;
			mOffsetGoal = 0;
			mFlingVelocity = 0;
			resetPositions();

			updateDisplay();
		}

		private void resetPositions() {
			mStartPos = mWaveformView.secondsToPixels(0.0);
			mEndPos = mWaveformView.secondsToPixels(1000 * 60 * 60);
		}

		private synchronized void updateDisplay() {
			if (record == 1) {
				int now = Integer.parseInt(String.valueOf(currtime));
				int frames = mWaveformView.millisecsToPixels(now);
//				DebugLog.loge("frames: "+frames);
				mWaveformView.setPlayback(frames);
				setOffsetGoalNoUpdate(frames - mWidth / 2);
				if (now >= mPlayEndMsec) {
					handlePause();
				}
				if (!mTouchDragging) {
					int offsetDelta;
	
					if (mFlingVelocity != 0) {
						offsetDelta = mFlingVelocity / 30;
						if (mFlingVelocity > 80) {
							mFlingVelocity -= 80;
						} else if (mFlingVelocity < -80) {
							mFlingVelocity += 80;
						} else {
							mFlingVelocity = 0;
						}
	
						mOffset += offsetDelta;
	
						if (mOffset < 0) {
							mOffset = 0;
							mFlingVelocity = 0;
						}
						mOffsetGoal = mOffset;
					} else {
						offsetDelta = mOffsetGoal - mOffset;
	
						if (offsetDelta > 10)
							offsetDelta = offsetDelta / 10;
						else if (offsetDelta > 0)
							offsetDelta = 1;
						else if (offsetDelta < -10)
							offsetDelta = offsetDelta / 10;
						else if (offsetDelta < 0)
							offsetDelta = -1;
						else
							offsetDelta = 0;
	
						mOffset += offsetDelta;
					}
				}
	
				mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
				mWaveformView.invalidate();
			}
		}

		private void switchPlayPause() {
//			if (state == 1) {
//				play.setImageResource(android.R.drawable.ic_media_pause);
//			} else {
//				play.setImageResource(android.R.drawable.ic_media_play);
//			}
		}

		private void setOffsetGoalNoUpdate(int offset) {
			if (mTouchDragging) {
				return;
			}

			mOffsetGoal = offset;
			if (mOffsetGoal < 0)
				mOffsetGoal = 0;
		}

		private synchronized void handlePause() {
//			mWaveformView.setPlayback(-1);
//			time1 = time;
			record = 2;
			switchPlayPause();
		}
		
//		private synchronized void handleStop() {
//			mWaveformView.setPlayback(-1);
//			time0 = 0;
//			time1 = 0;
//			time = 0;
//			state = 0;
//			switchPlayPause();
//		}

		private synchronized void onPlay(int startPosition, int s) {
			if (s == 2) {
				handlePause();
				return;
			}
//			if (s == 0) {
//				handleStop();
//				return;
//			}

			try {
				mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
				if (startPosition < mStartPos) {
					mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
				} else {
					mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
				}

				record = 1;

				updateDisplay();
				switchPlayPause();
			} catch (Exception e) {
				return;
			}
		}

	@Override
	public void waveformFling(float vx) {
		mTouchDragging = false;
		mOffsetGoal = mOffset;
		mFlingVelocity = (int) (-vx);
		updateDisplay();
	}

	@Override
	public void waveformDraw() {
		mWidth = mWaveformView.getMeasuredWidth();
//		if (mOffsetGoal != mOffset && !mKeyDown)
//			updateDisplay();
//		else if (record == 1) {
//			updateDisplay();
//		} else if (mFlingVelocity != 0) {
//		updateDisplay();
//		}		
		updateDisplay();
	}
	
	// TODO
		FrameLayout lyric_layout;
		LyricView lyricView = null;
		boolean isShowLyric = false;

		void showLyric() {
			if (isShowLyric) {
				return;
			} else {
				isShowLyric = true;
			}
			if (lyricView == null) {
				lyricView = new LyricView(JoinLyricMediaActivity.this);
				lyric_layout.addView(lyricView);
			}
			Animation show = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
			show.setDuration(500);
			lyric_layout.setVisibility(View.VISIBLE);
			lyric_layout.startAnimation(show);
		}
}
