package com.cncsoftgroup.panstage.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.soundvisualizer.WaveformView;
import com.nmd.libs.UtilLibs;

public class TestActivity extends Activity implements OnClickListener, WaveformView.WaveformListener {

	int width = 0;

	ImageView play, stop;
	TextView timeview;
	
	long time0 = 0, time1 = 0, time = 0;
	
	int state = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout._test);

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		width = displaymetrics.widthPixels;
		mDensity = displaymetrics.density;

		play = (ImageView) findViewById(R.id.play);
		stop = (ImageView) findViewById(R.id.stop);
		
		timeview = (TextView) findViewById(R.id.time);

		play.setOnClickListener(this);
		stop.setOnClickListener(this);
		
		timeview.setText("00:00");
		
		
		buildGUI();
	}

	Thread thread = null;
	
	void startThread(){
		if(thread == null){
			thread = new Thread() {
				
				@Override
				public void run() {
					while (true){
						if (state == 1) {
							time = System.currentTimeMillis() - time0 + time1;
							if (time % 1000 == 0) {
								runOnUiThread(new Runnable() {
									public void run() {
										timeview.setText(Utils.milliSecondsToTimer2(time));
									}
								});
							}
						}
					}
				}
			};
			thread.start();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.play :
				startThread();
				if (state == 0) {
					time0 = System.currentTimeMillis();
					onPlay(mWaveformView.secondsToPixels(0.0), 1);
				}else if (state == 1) {
					onPlay(-1 , 2);
				}else if (state == 2) {
					time0 = System.currentTimeMillis();
					onPlay(mWaveformView.secondsToPixels(time1) ,1);
				}
				UtilLibs.showToast(TestActivity.this, "state: "+state +"\ntime0: "+time0 +"\ntime1: "+time1 +"\ntime: "+time);
				break;
				
			case R.id.stop :
				onPlay(-1 , 0);
				break;

		}
	}

	// TODO

	private WaveformView mWaveformView;
	private boolean mKeyDown;
	private int mWidth;
	private int mStartPos;
	private int mEndPos;
	private int mOffset;
	private int mOffsetGoal;
	private int mFlingVelocity;
	int mPlayStartMsec;
	int mPlayEndMsec;
	private boolean mTouchDragging;
	private float mDensity;

	private void buildGUI() {
		state = 0;
		mKeyDown = false;

		loadGui();

		finishOpeningSoundFile();
	}

	private void loadGui() {

		mWaveformView = (WaveformView) findViewById(R.id.waveform);
		mWaveformView.setListener(this);

		mWaveformView.setSoundFile();
		mWaveformView.recomputeHeights(mDensity);

		updateDisplay();
	}

	private void finishOpeningSoundFile() {
		mWaveformView.recomputeHeights(mDensity);

		mTouchDragging = false;
//		mWaveformView.setZoomLevel(0);
		mOffset = 0;
		mOffsetGoal = 0;
		mFlingVelocity = 0;
		resetPositions();

		updateDisplay();
	}

	private void resetPositions() {
		mStartPos = mWaveformView.secondsToPixels(0.0);
		mEndPos = mWaveformView.secondsToPixels(1000 * 60 * 60);
	}

	private synchronized void updateDisplay() {
		if (state == 1) {
			int now = Integer.parseInt(String.valueOf(System.currentTimeMillis() - time0 + time1));
			int frames = mWaveformView.millisecsToPixels(now);
			mWaveformView.setPlayback(frames);
			setOffsetGoalNoUpdate(frames - mWidth / 2);
			
			if (now >= mPlayEndMsec) {
				handlePause();
			}
		}

		if (!mTouchDragging) {
			int offsetDelta;

			if (mFlingVelocity != 0) {
				offsetDelta = mFlingVelocity / 30;
				if (mFlingVelocity > 80) {
					mFlingVelocity -= 80;
				} else if (mFlingVelocity < -80) {
					mFlingVelocity += 80;
				} else {
					mFlingVelocity = 0;
				}

				mOffset += offsetDelta;

				if (mOffset < 0) {
					mOffset = 0;
					mFlingVelocity = 0;
				}
				mOffsetGoal = mOffset;
			} else {
				offsetDelta = mOffsetGoal - mOffset;

				if (offsetDelta > 10)
					offsetDelta = offsetDelta / 10;
				else if (offsetDelta > 0)
					offsetDelta = 1;
				else if (offsetDelta < -10)
					offsetDelta = offsetDelta / 10;
				else if (offsetDelta < 0)
					offsetDelta = -1;
				else
					offsetDelta = 0;

				mOffset += offsetDelta;
			}
		}

		mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
		mWaveformView.invalidate();
	}

	private void switchPlayPause() {
		if (state == 1) {
			play.setImageResource(android.R.drawable.ic_media_pause);
		} else {
			play.setImageResource(android.R.drawable.ic_media_play);
		}
	}

	private void setOffsetGoalNoUpdate(int offset) {
		if (mTouchDragging) {
			return;
		}

		mOffsetGoal = offset;
		if (mOffsetGoal < 0)
			mOffsetGoal = 0;
	}

	private synchronized void handlePause() {
//		mWaveformView.setPlayback(-1);
		time1 = time;
		state = 2;
		switchPlayPause();
	}
	
	private synchronized void handleStop() {
		mWaveformView.setPlayback(-1);
		time0 = 0;
		time1 = 0;
		time = 0;
		state = 0;
		switchPlayPause();
	}

	private synchronized void onPlay(int startPosition, int s) {
		if (s == 2) {
			handlePause();
			return;
		}
		if (s == 0) {
			handleStop();
			return;
		}

		try {
			mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
			if (startPosition < mStartPos) {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
			} else {
				mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
			}

			state = 1;

			updateDisplay();
			switchPlayPause();
		} catch (Exception e) {
			return;
		}
	}

	@Override
	public void waveformFling(float vx) {
		mTouchDragging = false;
		mOffsetGoal = mOffset;
		mFlingVelocity = (int) (-vx);
		updateDisplay();
	}
	
	public void waveformDraw() {
		mWidth = mWaveformView.getMeasuredWidth();
		if (mOffsetGoal != mOffset && !mKeyDown)
			updateDisplay();
		else if (state == 1) {
			updateDisplay();
		} else if (mFlingVelocity != 0) {
			updateDisplay();
		}
	}

}
