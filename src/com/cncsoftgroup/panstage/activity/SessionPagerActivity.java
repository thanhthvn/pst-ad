package com.cncsoftgroup.panstage.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cncsoftgroup.panstage.adapter.InfinitePagerAdapter;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.view.InfiniteViewPager;
import com.cncsoftgroup.panstage.view.SessionView;
import com.nmd.libs.DebugLog;

public class SessionPagerActivity extends Activity{
	public ArrayList<Session> arrList = new ArrayList<Session>();
	public static InfiniteViewPager pager;
	public int pagePos = -1;
	public PagerAdapter imagePagerAdapter;
	public SessionView sessionView;
	public int pageIntent = 1;
	public String urlAPI ="";
	public MediaPlayer media;
	public Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.session_pager);
//		Utils.showProgressDialog(SessionPagerActivity.this, getString(R.string.loading));
		Intent callerIntent = getIntent();
		Bundle bundle = callerIntent.getBundleExtra("myBundle");
		pageIntent = bundle.getInt("pageIntent");
		urlAPI = bundle.getString("urlAPI");
		
		if(CacheData.getInstant().getListSession()!=null){
			arrList = CacheData.getInstant().getListSession();
			if(arrList.size()>0){
				int fake_pos = bundle.getInt("position");
				pagePos = Utils.getRealPosition(fake_pos, arrList.size());
				DebugLog.loge(fake_pos+"-->"+pagePos);
				media = new MediaPlayer();
				CacheData.getInstant().setSid_update(arrList.get(pagePos).getSid());		
				
				pager = (InfiniteViewPager) findViewById(R.id.pager);
				imagePagerAdapter =  new InfinitePagerAdapter(new ImagePagerAdapter(arrList)); 
				pager.setAdapter(imagePagerAdapter);
				pager.setOnPageChangeListener(new OnPageChangeListener() {
					
					@Override
					public void onPageSelected(int postion) {
						pagePos = Utils.getRealPosition(postion, arrList.size());
						DebugLog.loge(postion+"-->"+pagePos);
						if(sessionView!=null){
							if(media!=null && handler !=null && sessionView.mUpdateTimeTask !=null){
								if (media.isPlaying()) {
									media.stop();
									media = null;
									handler.removeCallbacks(sessionView.mUpdateTimeTask);
									media = new MediaPlayer();
								}
								getCurrentPage().tv_speed_load.setText("");
//								if(getCurrentPage().downloadTask!=null && !getCurrentPage().isPlay){
//									DebugLog.loge("downloadTask cancel");
//									getCurrentPage().downloadTask.cancel(true);
//								}
								if(getCurrentPage().isPlay){
									DebugLog.loge("isPlay false");
									getCurrentPage().isPlay = false;
								}
//								getCurrentPage().download(arrList.get(pagePos).getUpload_url(), arrList.get(pagePos).getSid());
								getCurrentPage().playSong(pagePos, arrList.get(pagePos).getSid(), arrList.get(pagePos).getSong_name(), arrList.get(pagePos).getUpload_url());
								getCurrentPage().tv_speed_load.setText("Loading...");
								getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
								getCurrentPage().tv_Duration_Finish.setText(""+Utils.milliSecondsToTimer(0));
								getCurrentPage().tv_Duration_Playing.setText(""+Utils.milliSecondsToTimer(0));
								getCurrentPage().isBufferApart = false;
								getCurrentPage().isLoadDone = false;
								getCurrentPage().seekbarPrgress.setSecondaryProgress(0);
							}							
						}
					}
					
					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						
					}
					
					@Override
					public void onPageScrollStateChanged(int arg0) {
						
					}
				});
				
				pager.setCurrentItem(pagePos);
			}
		}
	}
	
	public void showView(){
		if(sessionView != null){
//			Utils.dismissCurrentDialog();
			sessionView.setVisibility(View.VISIBLE);
		}
	}
	
	public void hideView(){
		if(sessionView != null){
//			Utils.showProgressDialog(SessionPagerActivity.this, getString(R.string.loading));
			sessionView.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		sessionView.backpress();
		if(media!=null && handler !=null && sessionView.mUpdateTimeTask !=null){
			if (media.isPlaying()) {
				media.stop();
				media = null;
				handler.removeCallbacks(sessionView.mUpdateTimeTask);
			}			
		}

		if(pageIntent == -1){
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			finish();
			return;
		}
		
//		if(getCurrentPage().downloadTask!=null && !getCurrentPage().isPlay){
//			DebugLog.loge("downloadTask cancel");
//			getCurrentPage().downloadTask.cancel(true);
//		}
		
		startActivity(new Intent(SessionPagerActivity.this, HomePageActivity.class));
		overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
		finish();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;

		switch (requestCode) {
		case 1001:
			getCurrentPage().title_lyric_song.setText(CacheData.getInstant().getLyrics().getLyric_title());
			getCurrentPage().tv_lyric.setText(CacheData.getInstant().getLyrics().getLyric());
			break;

		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		CacheData.getInstant().setSessionDetail(true);
		DebugLog.loge("onResume");
		if(getCurrentPage() != null ){
			getCurrentPage().resume();
		}
		
		if(sessionView!=null){
			if(getCurrentPage().isLoadDone){
				if(media!=null && handler !=null && sessionView.mUpdateTimeTask !=null){
					if (media.isPlaying()) {
//						getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
//						media.pause();
						
					} else {
						if(!CacheData.getInstant().isAboutView()){
							CacheData.getInstant().setAboutView(false);
							handler.postDelayed(sessionView.mUpdateTimeTask, 100);
							getCurrentPage().img_Play_Pause.setImageResource(R.drawable.bt_pause);
							media.start();
						}
					}				
				}
			}else{
				getCurrentPage().playSong(pagePos, arrList.get(pagePos).getSid(), arrList.get(pagePos).getSong_name(), arrList.get(pagePos).getUpload_url());
				getCurrentPage().seekbarPrgress.setSecondaryProgress(0);
				getCurrentPage().tv_speed_load.setText("Loading...");
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		CacheData.getInstant().setSessionDetail(false);
		DebugLog.loge("onPause");
		if(sessionView!=null){
			if(getCurrentPage().isLoadDone){
				if(media!=null && handler !=null && sessionView.mUpdateTimeTask !=null){
					if (media.isPlaying()) {
						if(!CacheData.getInstant().isAboutView()){
							getCurrentPage().img_Play_Pause.setImageResource(R.drawable.play_music_play);
							media.pause();							
						}
					} else {
//						getCurrentPage().img_Play_Pause.setImageResource(R.drawable.bt_pause);
//						media.start();
					}				
				}
			}else{
				CacheData.getInstant().setLoading(false);
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		CacheData.getInstant().setAboutView(false);
		DebugLog.loge("onDestroy");
		super.onDestroy();
	}
	
	public SessionView getCurrentPage(){
		InfinitePagerAdapter adapter = (InfinitePagerAdapter) pager.getAdapter();
		return (SessionView) adapter.mCurrentView;
	}
	
	public class ImagePagerAdapter extends PagerAdapter {
		ArrayList<Session> arrList = new ArrayList<Session>();
		private LayoutInflater inflater;
		public View mCurrentView;
		
		ImagePagerAdapter(ArrayList<Session> arrList) {
			this.arrList = arrList;
			inflater = getLayoutInflater();
		}
		
	    @Override
	    public void setPrimaryItem(ViewGroup container, int position, Object object) {
	        mCurrentView = (View) object;
	    }

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return arrList.size();
		}

		@SuppressWarnings("static-access")
		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			sessionView = (SessionView) inflater.inflate(R.layout.homepage_creator, view, false);
			sessionView.creator_user_id = arrList.get(position).getCreator_user_id();
			sessionView.url_img = arrList.get(position).getCreator_user_avatar();
			sessionView.song_name = arrList.get(position).getSong_name();
			sessionView.lyric_title = arrList.get(position).getLyric_title();
			sessionView.lyric = arrList.get(position).getLyric();
			sessionView.full_name = arrList.get(position).getCreator_full_name();
			sessionView.role = arrList.get(position).getCreator_user_role();
			sessionView.role_description = arrList.get(position).getCreator_user_role_description();
			sessionView.media_url = arrList.get(position).getUpload_url();
			sessionView.isLocked = arrList.get(position).isLocked();
			sessionView.isLike = arrList.get(position).isIs_like();
			sessionView.sid = arrList.get(position).getSid();
			sessionView.duration = arrList.get(position).getDuration();
			sessionView.pageIntent = pageIntent;
			sessionView.urlAPI = urlAPI;
			sessionView.position = position;
			sessionView.view = sessionView;			
			sessionView.initView();
			
			if(SharedPreference.loadUid(SessionPagerActivity.this) == arrList.get(position).getCreator_user_id()){
				sessionView.img_Lock_Session.setVisibility(View.VISIBLE);
			}else{
				sessionView.img_Lock_Session.setVisibility(View.GONE);
			}
			
//			DebugLog.loge("\npagePos: " +pagePos + "\nposition: " + position);
			
			if(pagePos == position){
//				getCurrentPage().isDownloading = true;
//				sessionView.download(sessionView.media_url, sessionView.sid);
				sessionView.playSong(pagePos, sessionView.sid, sessionView.song_name, sessionView.media_url);
			}
			
			((ViewPager) view).addView(sessionView, 0);
			return sessionView;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}
	
	public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
        private int currentPage;

        @Override
        public void onPageSelected(int position) {
            currentPage = position;
        }

        public final int getCurrentPage() {
            return currentPage;
        }
	}    
}
