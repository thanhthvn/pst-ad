package com.cncsoftgroup.panstage.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cncsoftgroup.panstage.adapter.SessionAdapter;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.SessionManager;
import com.cncsoftgroup.panstage.manager.SessionManager.OnChanelUserSession;
import com.cncsoftgroup.panstage.view.LoadMoreListView;
import com.cncsoftgroup.panstage.view.LoadMoreListView.OnLoadMoreListener;
import com.nmd.libs.DebugLog;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class UserPageActivity extends Activity implements OnClickListener {

	ImageView imgAvatar, imgRole;
	TextView tvFullName, tvRole, tvDescription, img_Done;
	EditText etDescription;
	LinearLayout llDescription;
	LoadMoreListView lvUserPage;
	View avatar_view;
	int page = 1;
	boolean lladd = false;
	Session obj;

	ArrayList<Session> arrList;
	SessionAdapter adapter;

	ImageTagFactory imageTagFactory = ImageTagFactory.newInstance();
	ImageManager imageManager = AppController.getImageManager();
	int THUMB_IMAGE_SIZE = 160;

	String URL = Url.API_GET_USER_CHANNEL;

	String fullname, role, description, avata, authu_token;
	private int sid;
	private int uid;

	private Context context;

	private Boolean user_page;

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		View view = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (view instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_page);

		context = this;
		intitView();
		getvalue();
		imageTagFactory.setWidth(THUMB_IMAGE_SIZE);
		imageTagFactory.setHeight(THUMB_IMAGE_SIZE);
		imageTagFactory.setDefaultImageResId(R.drawable.no_ava);
		imageTagFactory.setErrorImageId(R.drawable.no_ava);
		imageTagFactory.setSaveThumbnail(true);
		imageTagFactory.usePreviewImage(THUMB_IMAGE_SIZE, THUMB_IMAGE_SIZE, true);

		tvFullName.setText(fullname);

		imgAvatar.setTag(imageTagFactory.build(avata, UserPageActivity.this));
		imageManager.getLoader().load(imgAvatar);

		if (SharedPreference.loadRole(UserPageActivity.this).equals("Writer")) {
			imgRole.setImageResource(R.drawable.user_page_icon_write);
			tvRole.setText("Writer");
		} else if (SharedPreference.loadRole(UserPageActivity.this).equals("Player")) {
			imgRole.setImageResource(R.drawable.user_page_icon_writer);
			tvRole.setText("Player");
		} else if (SharedPreference.loadRole(UserPageActivity.this).equals("Singer")) {
			imgRole.setImageResource(R.drawable.user_page_icon_mic);
			tvRole.setText("Singer");
		}

		if (description == null || description.equals("null")) {
			tvDescription.setText("");
			tvDescription.setHint("Description");
		} else {
			tvDescription.setText(description);
		}

		llDescription.setOnClickListener(this);
		img_Done.setOnClickListener(this);

		arrList = new ArrayList<Session>();
		getChanelUserSession(URL);
		lvUserPage.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				getMoreChanelUserSession(URL, page++);
			}
		});

	}
	private void getvalue() {
		user_page = CacheData.getInstant().isUserPage();
		if (user_page) {
			Intent intent = getIntent();
			Bundle bunder = intent.getBundleExtra("session_page");
			if (bunder != null) {
				authu_token = SharedPreference.loadAuthu_token(context);
				avata = bunder.getString("avatar");
				fullname = bunder.getString("full_name");
				sid = bunder.getInt("sid");
				uid = bunder.getInt("uid");
				description = bunder.getString("description");

				try {
					if (bunder.getInt("notMe") == 1) {
						img_Done.setVisibility(View.GONE);
					}
				} catch (Exception e) {
					DebugLog.loge(e.getMessage());
				}
			}

		} else {
			authu_token = SharedPreference.loadAuthu_token(context);
			avata = SharedPreference.loadAvata(context);
			fullname = SharedPreference.loadFullName(context);
			uid = SharedPreference.loadUid(context);
			description = SharedPreference.loadRoleDescription(context);
		}
	}

	private void intitView() {
		tvFullName = (TextView) findViewById(R.id.tvFullName);
		avatar_view = (View) findViewById(R.id.imgAvatarUserPage);
		imgRole = (ImageView) findViewById(R.id.imgRole);
		tvRole = (TextView) findViewById(R.id.tvRole);
		imgAvatar = (ImageView) avatar_view.findViewById(R.id.im_Home_Avata_List);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		img_Done = (TextView) findViewById(R.id.img_Done);
		lvUserPage = (LoadMoreListView) findViewById(R.id.lvDisplay);
		lvUserPage.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (arrList != null) {
					CacheData.getInstant().setListSession(arrList);
					obj = arrList.get(position);
					Intent intent = new Intent(context, SessionPagerActivity.class);
					Bundle bundle = new Bundle();

					bundle.putInt("position", position);

					int pageIntent = -1;
					bundle.putInt("pageIntent", pageIntent);

					String urlAPI = Url.API_LIKE_SESSION_BY_NEWEST;
					bundle.putString("urlAPI", urlAPI);

					intent.putExtra("myBundle", bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
				}
			}
		});
		llDescription = (LinearLayout) findViewById(R.id.llDescription);
	}

	private void getChanelUserSession(String url) {
		Utils.showProgressDialog(UserPageActivity.this, getString(R.string.loading));
		arrList = new ArrayList<Session>();
		new SessionManager().getChanelUserSession(authu_token, 1, 20, uid, url, new OnChanelUserSession() {

			@Override
			public void onChanelUserMethod(boolean isSuccess, ArrayList<Session> arrayList) {
				Utils.dismissCurrentDialog();
				if (isSuccess) {
					arrList.addAll(arrayList);
					adapter = new SessionAdapter(UserPageActivity.this, R.layout.homepage_item_list, arrList);
					lvUserPage.setAdapter(adapter);
				} else {
					Utils.showToast(UserPageActivity.this, "Get List Session Failed");
					CacheData.getInstant().setUserPage(false);
					Intent intent = getIntent();
					startActivity(intent);
					finish();
				}
			}
		});
	}

	private void getMoreChanelUserSession(String url, int page) {
		new SessionManager().getChanelUserSession(authu_token, page, 20, uid, url, new OnChanelUserSession() {

			@Override
			public void onChanelUserMethod(boolean isSuccess, ArrayList<Session> arrayList) {
				if (isSuccess) {
					arrList.addAll(arrayList);
					adapter.notifyDataSetChanged();
				} else {
					Utils.showToast(UserPageActivity.this, "Get List Session Failed");
				}
				lvUserPage.onLoadMoreComplete();
			}
		});
	}

	// Update User Description
	public interface OnUpdateUserDescription {
		void onUpdateUserDescriptionMethod(boolean isSuccess);
	}

	OnUpdateUserDescription callbackOnUpdateUserDescription;

	private String tag_update_user_description = "tag_update_user_description";

	public void updateUserDescription(String auth_token, String url, String description, OnUpdateUserDescription onUpdateUserDescription) {
		callbackOnUpdateUserDescription = onUpdateUserDescription;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("auth_token", auth_token);
			jsonParam.put("description", description);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST, url, jsonParam, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.logd("response: " + response.toString());
				try {
					boolean success = response.getBoolean("success");
					if (success) {
						callbackOnUpdateUserDescription.onUpdateUserDescriptionMethod(true);
					} else {
						callbackOnUpdateUserDescription.onUpdateUserDescriptionMethod(false);
					}

				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnUpdateUserDescription.onUpdateUserDescriptionMethod(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError response) {
				DebugLog.logd("Response: " + response.getMessage());
				callbackOnUpdateUserDescription.onUpdateUserDescriptionMethod(false);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_update_user_description);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_Done :
				DebugLog.loge("img_Done click");
				Utils.hiddenSofwareKeyboard(UserPageActivity.this, getCurrentFocus());
				if (!user_page) {
					if (lladd) {
						if (etDescription.getText().toString().equals("") || etDescription.getText().toString().equals(description)) {
							if (description.equals("")) {
								Utils.showErrorNullOrEmpty(etDescription, "Description is empty!");
								return;
							}
							if (etDescription.getText().toString().equals(description)) {
								onBackPressed();
								return;
							}
							llDescription.removeAllViews();
							tvDescription.setText(description);
							llDescription.addView(tvDescription);
						} else {
							updateUserDescription(authu_token, Url.API_UPDATE_DESCRIPTION, etDescription.getText().toString(), new OnUpdateUserDescription() {
								@Override
								public void onUpdateUserDescriptionMethod(boolean isSuccess) {
									if (isSuccess) {
										Utils.showToast(UserPageActivity.this, "Update Successfully");
										SharedPreference.saveRoleDescription(context, etDescription.getText().toString());
										llDescription.removeAllViews();
										tvDescription.setText(etDescription.getText().toString());
										llDescription.addView(tvDescription);
									} else {
										Utils.showToast(UserPageActivity.this, "Update Failed");
										llDescription.removeAllViews();
										tvDescription.setText(description);
										llDescription.addView(tvDescription);
									}
									onBackPressed();
								}
							});
						}
						lladd = false;
					} else
						onBackPressed();
				}
				break;
			case R.id.llDescription :
				if (!user_page) {
					etDescription = new EditText(UserPageActivity.this);
					etDescription.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					etDescription.setText(tvDescription.getText().toString());
					llDescription.removeAllViews();
					llDescription.addView(etDescription);
					lladd = true;
				}
				break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		finish();
		CacheData.getInstant().setUserPage(false);
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (!user_page) {
			startActivity(new Intent(UserPageActivity.this, HomePageActivity.class));
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			finish();
		} else {
			CacheData.getInstant().setUserPage(false);
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			finish();
		}
	}
}
