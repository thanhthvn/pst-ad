package com.cncsoftgroup.panstage.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.entity.mime.content.FileBody;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cncsoftgroup.panstage.adapter.Base64;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.UserManager;
import com.cncsoftgroup.panstage.manager.UserManager.OnLoginAndRegisterResult;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.model.GraphUser;
import com.nmd.libs.DebugLog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


public class LoginRegisterActivity extends Activity implements OnClickListener{
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	private TextView bt_Done, bt_login;
	EditText edt_email, edt_password, edt_fullname;
	ImageView avatar;
	String picturePath = "";
	String avatarEncode="";
	String pictureName = "";
	private static final int REQUIRED_SIZE=100;
	private Context context;
	private LinearLayout lLayout;
	protected static ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private Button register_with_facebook;
	
	TextView terms_of_use, privacy_policy;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		View view = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (view instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context=this;
		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
		
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback,
						savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this)
						.setCallback(statusCallback));
			}
		}
		if(SharedPreference.isAlreadyLoginFacebook(context)){
			loginFB();
			return;
		}
		
		setContentView(R.layout.login_register);
//		if(!SharedPreference.loadAuthu_token(context).equals("")){
//			startActivity(new Intent(LoginRegisterActivity.this, HomePageActivity.class));
//			finish();
//		}
		lLayout=(LinearLayout)findViewById(R.id.background_login_register);
		if(SharedPreference.loadRole(context).equals(getString(R.string.player))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_player_im_background));
		}else if(SharedPreference.loadRole(context).equals(getString(R.string.singer))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_singer_im_background));
		}else{
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_listener_im_background));
		}
		
		genKeyHash();
		
		bt_Done=(TextView)findViewById(R.id.login_Done);
		bt_login=(TextView)findViewById(R.id.bt_login);
		edt_email=(EditText)findViewById(R.id.edt_email);
		edt_password=(EditText)findViewById(R.id.edt_password);
		edt_fullname=(EditText)findViewById(R.id.edt_fullname);
		avatar = (ImageView)findViewById(R.id.avatar);
		register_with_facebook=(Button)findViewById(R.id.register_with_facebook);
		
		register_with_facebook.setOnClickListener(this);
		bt_Done.setOnClickListener(this);
		bt_login.setOnClickListener(this);
		avatar.setOnClickListener(this);
		
		options = new DisplayImageOptions.Builder()
		   .showImageOnLoading(R.drawable.ic_loading)
		   .showImageForEmptyUri(R.drawable.ic_error)
		  .showImageOnFail(R.drawable.ic_error)
		  .cacheOnDisc(true)
		  .bitmapConfig(Bitmap.Config.RGB_565)
		  .build();
		
		terms_of_use = (TextView) findViewById(R.id.terms_of_use); 
		privacy_policy = (TextView) findViewById(R.id.privacy_policy);
		
		terms_of_use.setOnClickListener(this); 
		privacy_policy.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_Done:
			String email = edt_email.getText().toString();
			String password = edt_password.getText().toString();
			String fullname = edt_fullname.getText().toString();
			if (!Utils.isNullOrEmpty(email)){
				if (Utils.validateEmail(email)){
					if (!Utils.isNullOrEmpty(password)){
						if(password.length() <6 || password.length() > 10){
							Utils.showErrorNullOrEmpty(edt_password, "Password must between 6 and 10!");
							return;
						}
						
						if (!Utils.isNullOrEmpty(fullname)){
							Utils.showProgressDialog(LoginRegisterActivity.this, context.getString(R.string.loading));
							CacheData.getInstant().setMessage("");
							new UserManager().register_NetworkService(fullname, email, password, SharedPreference.loadRoleRegister(LoginRegisterActivity.this), picturePath, new OnLoginAndRegisterResult() {
								
								@Override
								public void onLoginAndRegisterMethod(boolean isSuccess) {
									if (isSuccess){
										DebugLog.logd("Success!");
										Utils.dismissCurrentDialog();
										SharedPreference.saveEmailAndPassword(LoginRegisterActivity.this, "", "");
										startActivity(new Intent(LoginRegisterActivity.this, HomePageActivity.class));
										finish();
									} else {
										Utils.dismissCurrentDialog();
										if(CacheData.getInstant().getMessage().equals("")){
											Utils.showToast(LoginRegisterActivity.this, "Failed!");		
										}else if(CacheData.getInstant().getMessage().contains("has already been taken")){
											Utils.showToast(LoginRegisterActivity.this, "Email has already been taken!");
										}else{
											Utils.showToast(LoginRegisterActivity.this, CacheData.getInstant().getMessage());
										}
										DebugLog.loge("Failed!");
									}
								}
							});
						} else {
							Utils.showErrorNullOrEmpty(edt_fullname, "Fullname is null!");
						}
					} else {
						Utils.showErrorNullOrEmpty(edt_password, "Password is null!");
						if (Utils.isNullOrEmpty(fullname)){
							Utils.showErrorNullOrEmpty(edt_fullname, "Fullname is null!");
						}
					}
				} else {
					Utils.showErrorNullOrEmpty(edt_email, "Wrong format!");
				}
			} else {
				Utils.showErrorNullOrEmpty(edt_email, "Email is null!");
				if (Utils.isNullOrEmpty(password)){
					Utils.showErrorNullOrEmpty(edt_password, "Password is null!");
				}
				if (Utils.isNullOrEmpty(fullname)){
					Utils.showErrorNullOrEmpty(edt_fullname, "Fullname is null!");
				}}
			
			break;
			
		case R.id.bt_login:
			startActivity(new Intent(LoginRegisterActivity.this, LoginRegisterLoginActivity.class));
			overridePendingTransition(R.anim.push_right_in,R.anim.push_right_out);
			finish();
			break;
		
		case R.id.avatar:
			selectImage();
			break;
		case R.id.register_with_facebook:
			SharedPreference.hasLoginFacebook(context, false);
			loginFB();
//			startActivity(new Intent(LoginRegisterActivity.this, LoginActivity.class));
//			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//			finish();
			break;
			
		case R.id.terms_of_use:
			Intent i1 = new Intent(Intent.ACTION_VIEW);
			i1.setData(Uri.parse("http://www.pan-stage.com/terms-of-use.html"));
			startActivity(i1);
			break;
		case R.id.privacy_policy:
			Intent i2 = new Intent(Intent.ACTION_VIEW);
			i2.setData(Uri.parse("http://www.pan-stage.com/privacy-policy.html"));
			startActivity(i2);
			break;
	}}
	
	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(LoginRegisterActivity.this);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, 1001);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(Intent.createChooser(intent, "Select File"), 1002);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}
	
	boolean isFacebookclick = false;
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(isFacebookclick){
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
			if(!SharedPreference.isLoginFacebook(context)){
				new UserManager().login_facebook(Session.getActiveSession().getAccessToken(), SharedPreference.loadRole(LoginRegisterActivity.this), new OnLoginAndRegisterResult() {
					@Override
					public void onLoginAndRegisterMethod(boolean isSuccess) {
						if(isSuccess){
							SharedPreference.hasLoginFacebook(context, true);
							startActivity(new Intent(LoginRegisterActivity.this, HomePageActivity.class));
							finish();
						}
						else Utils.showToast(getApplicationContext(), "Login Failed");
					}
				});
			}			
		}
		
		if (resultCode == RESULT_OK) {
			if (requestCode == 1001) {
				File f = new File(Environment.getExternalStorageDirectory().toString());
				for (File temp : f.listFiles()) {
					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {

					File filePath=new File(f.getAbsolutePath());
					Bitmap bm=decodeFile(filePath);
					avatar.setImageBitmap(bm);
					avatar.setBackground(null);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bm.compress(Bitmap.CompressFormat.PNG, 70, stream);
					byte [] byte_arr = stream.toByteArray();
					avatarEncode = Base64.encodeBytes(byte_arr);
					DebugLog.loge("ENCODE AVATAR CAMERE: " + avatarEncode);

					String path = android.os.Environment.getExternalStorageDirectory()
							+ File.separator
							+ "Phoenix" + File.separator + "default";
					f.delete();
					OutputStream fOut = null;
					pictureName = String.valueOf(System.currentTimeMillis()) + ".jpg";
					File file = new File(path, pictureName);
					picturePath = path + "/" +  pictureName;
					try {
						fOut = new FileOutputStream(file);
						bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
						fOut.flush();
						fOut.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				DebugLog.loge("picturePath: " + picturePath);
			} else if (requestCode == 1002) {
				Uri selectedImageUri = data.getData();

				picturePath = getPath(selectedImageUri, LoginRegisterActivity.this);
				
				File file=new File(picturePath);
				Bitmap bm=decodeFile(file);
				avatar.setImageBitmap(bm);
				avatar.setBackground(null);
				
				
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				byte [] byte_arr = stream.toByteArray();
				avatarEncode = Base64.encodeBytes(byte_arr);
				 
				DebugLog.loge("ENCODE AVATAR: " + avatarEncode);
				DebugLog.loge("ENCODE FILE: " + new FileBody(file));
				
				DebugLog.loge("picturePath: " + picturePath);
			}
		}
	}
	
	private Bitmap decodeFile(File f){
	    try {

	    	BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

	        DebugLog.loge("SIZE: "+ o.outWidth+" x "+o.outHeight);

	        int scale=1;
	        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE){
	            scale*=2;
	           
	        }
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	       Bitmap bm= BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	        DebugLog.loge("RESIZE: "+o2.outWidth +" x "+o2.outHeight);
	        return bm;
	    } catch (FileNotFoundException e) {}
	    return null;
	}
	
	
	@SuppressWarnings("deprecation")
	public String getPath(Uri uri, Activity activity) {
		String[] projection = { MediaColumns.DATA };
		Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	     
	@Override
	public void onBackPressed() {
		startActivity(new Intent(LoginRegisterActivity.this, IntroActivity.class));
		finish();
	}
	
	private void genKeyHash() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.cncsoftgroup.panstage.activity", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:",
						android.util.Base64.encodeToString(md.digest(), android.util.Base64.DEFAULT));
			}
		} catch (Exception e) {
			Log.d("KeyHash", "Error: " + e.getMessage().toString());
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if(isFacebookclick){
			Session.getActiveSession().addCallback(statusCallback);			
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if(isFacebookclick){
			Session.getActiveSession().removeCallback(statusCallback);			
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(isFacebookclick){
			Session session = Session.getActiveSession();
			Session.saveSession(session, outState);			
		}
	}


	private void loginFB() {
		if (!SharedPreference.isLoginFacebook(context) || SharedPreference.isAlreadyLoginFacebook(context)) {
			isFacebookclick = true;
			final Session session = Session.getActiveSession();
			if (session != null && session.isOpened()) {
				
				CacheData.getInstant().setLoginFacebook(true);
				DebugLog.loge("ACCESS TOKEN: "+session.getAccessToken());
				
				 List<String> permissions = new ArrayList<String>();
				    permissions.add("email");
				    openActiveSession(this, true, new Session.StatusCallback() {
				        @SuppressWarnings("deprecation")
				     @Override
				        public void call(final Session session, SessionState state, Exception exception) {
				            if (session.isOpened()) {
				            	 Utils.showProgressDialog(context, "Login With Facebook....");
				                //make request to the /me API
				                Log.e("sessionopened", "true");
				                Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
				                    @Override
				                    public void onCompleted(GraphUser user, Response response) {
				                        if (user != null) {
				                        	new UserManager().login_facebook(session.getAccessToken(), SharedPreference.loadRoleRegister(LoginRegisterActivity.this), new OnLoginAndRegisterResult() {
				            					
				            					@Override
				            					public void onLoginAndRegisterMethod(boolean isSuccess) {
				            						if(isSuccess){
				            							Utils.dismissCurrentDialog();
				            							SharedPreference.hasLoginFacebook(context, true);
				            							SharedPreference.hasAlreadyLoginFacebook(context, true);
				            							startActivity(new Intent(LoginRegisterActivity.this, HomePageActivity.class));
				            							finish();
				            						}
				            						
				            						else {Utils.showToast(getApplicationContext(), "Login Failed");
				            						Utils.dismissCurrentDialog();
				            						}}
				            				});
				                        }
				                    }
				                });
				             }
				         }
				     }, permissions);
				
			} else {

				onClickLogin();
			}

		} else if (!CacheData.getInstant().getAcces_token().equals("")) {
//			new UserManager().login_facebook(CacheData.getInstant().getAcces_token(), SharedPreference.loadRole(LoginActivity.this), new OnLoginAndRegisterResult() {
//				@Override
//				public void onLoginAndRegisterMethod(boolean isSuccess) {
//					if(isSuccess){
//						startActivity(new Intent(LoginActivity.this, HomePageActivity.class));
//						finish();
//					}
//					else Utils.showToast(getApplicationContext(), "Login Failed");
//				}
//			});
			
		}
	}
	private static Session openActiveSession(Activity activity, boolean allowLoginUI, Session.StatusCallback callback, List<String> permissions) {
	    Session.OpenRequest openRequest = new Session.OpenRequest(activity).setPermissions(permissions).setCallback(callback);
	    Session session = new Session.Builder(activity).build();
	    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
	    	DebugLog.loge("TOKEN CHECK:\n"+SessionState.CREATED_TOKEN_LOADED);
	        Session.setActiveSession(session);
	        session.openForRead(openRequest);
	        return session;
	    }
	    return null;
	}

	private void onClickLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(this)
					.setCallback(statusCallback));
		} else {
			Session.openActiveSession(this, true, statusCallback);

		}
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if(isFacebookclick){
				loginFB();				
			}
		}
	}
}
