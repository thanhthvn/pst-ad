package com.cncsoftgroup.panstage.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.cncsoftgroup.panstage.common.SharedPreference;
import com.cncsoftgroup.panstage.common.Utils;
import com.cncsoftgroup.panstage.manager.UserManager;
import com.cncsoftgroup.panstage.manager.UserManager.OnLoginAndRegisterResult;
import com.nmd.libs.DebugLog;

public class LoginRegisterLoginActivity extends Activity implements OnClickListener {
	private TextView bt_Login, bt_register;
	EditText edt_email, edt_password;
	ToggleButton toggleButton;
	boolean isCheck = true;
	private Context context;
	private LinearLayout lLayout;
	
	TextView terms_of_use, privacy_policy;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		View view = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (view instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_register_login);
		context=this;
		
		lLayout=(LinearLayout)findViewById(R.id.background_login_register_login);
		if(SharedPreference.loadRole(context).equals(getString(R.string.player))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_player_im_background));
		}else if(SharedPreference.loadRole(context).equals(getString(R.string.singer))){
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_singer_im_background));
		}else{
			lLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.intro_listener_im_background));
		}
		
		bt_Login = (TextView) findViewById(R.id.register_Bt_Login);
		bt_register = (TextView) findViewById(R.id.bt_register);
		edt_email = (EditText) findViewById(R.id.edt_email);
		edt_password = (EditText) findViewById(R.id.edt_password);
		
		toggleButton =(ToggleButton) findViewById(R.id.toggleButton);
		
		bt_Login.setOnClickListener(this);
		bt_register.setOnClickListener(this);
		
		if(!SharedPreference.loadEmail(this).equals("")){
			edt_email.setText(SharedPreference.loadEmail(this));
			edt_password.setText(SharedPreference.loadPassword(this));
		}
		
		isCheck = true;
		
		toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				isCheck = isChecked;
			}
		});
		
		terms_of_use = (TextView) findViewById(R.id.terms_of_use); 
		privacy_policy = (TextView) findViewById(R.id.privacy_policy);
		
		terms_of_use.setOnClickListener(this); 
		privacy_policy.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.register_Bt_Login:
			final String email = edt_email.getText().toString();
			final String password = edt_password.getText().toString();
			if (!Utils.isNullOrEmpty(email)){
				if (Utils.validateEmail(email)){
					if (!Utils.isNullOrEmpty(password)){
						Utils.showProgressDialog(LoginRegisterLoginActivity.this, getString(R.string.loading));
						new UserManager().login(email, password, new OnLoginAndRegisterResult() {
							
							@Override
							public void onLoginAndRegisterMethod(boolean isSuccess) {
								if (isSuccess){
									DebugLog.logd("Success!");
									if (isCheck){
										SharedPreference.saveEmailAndPassword(LoginRegisterLoginActivity.this, email, password);
									} else {
										SharedPreference.saveEmailAndPassword(LoginRegisterLoginActivity.this, "", "");
									}
									Utils.dismissCurrentDialog();
									startActivity(new Intent(LoginRegisterLoginActivity.this, HomePageActivity.class));
									finish();
								} else {
									Utils.showToast(LoginRegisterLoginActivity.this, "Failed!");
									Utils.dismissCurrentDialog();
								}
							}
						});
					} else {
						Utils.showErrorNullOrEmpty(edt_password, "Password is null!");
					}
				} else {
					Utils.showErrorNullOrEmpty(edt_email, "Wrong format!");
				}
			} else {
				Utils.showErrorNullOrEmpty(edt_email, "Email is null!");
				if (Utils.isNullOrEmpty(password)){
					Utils.showErrorNullOrEmpty(edt_password, "Password is null!");
				}
			}
			
			
			break;
		case R.id.bt_register:
			startActivity(new Intent(LoginRegisterLoginActivity.this, LoginRegisterActivity.class));
			overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
			finish();
			break;
		case R.id.terms_of_use:
			Intent i1 = new Intent(Intent.ACTION_VIEW);
			i1.setData(Uri.parse("http://www.pan-stage.com/terms-of-use.html"));
			startActivity(i1);
			break;
		case R.id.privacy_policy:
			Intent i2 = new Intent(Intent.ACTION_VIEW);
			i2.setData(Uri.parse("http://www.pan-stage.com/privacy-policy.html"));
			startActivity(i2);
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		startActivity(new Intent(LoginRegisterLoginActivity.this, LoginRegisterActivity.class));
		finish();
	}
}
