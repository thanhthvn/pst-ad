package com.cncsoftgroup.panstage.common;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaMetadataRetriever;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cncsoftgroup.panstage.activity.R;
import com.cncsoftgroup.panstage.bean.Record;
import com.nmd.libs.DebugLog;
import com.nmd.libs.UtilityMain;

@SuppressLint({ "InlinedApi", "DefaultLocale" })
public class Utils {
	public static int deviceWidth;
	public static int deviceHeight;
	
	public static final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Panstage";
	public static final String path_Record = path + File.separator + "Record";
	public static final String path_Record_temp = path + File.separator + "RecordTemp";
	public static final String path_Mix = path + File.separator + "Mix";
	public static final String path_Download = path + File.separator + "Download";
	public static final String path_FileA1 =  path_Mix+ File.separator + "mixA1.wav";
	public static final String path_FileA2 =  path_Mix+ File.separator + "mixA2.wav";
	public static final String path_FileB1 =  path_Mix+ File.separator + "mixB1.wav";
	public static final String path_FileB2 =  path_Mix+ File.separator + "mixB2.wav";
	public static final String path_FileOut =  path_Mix+ File.separator + "mixed.wav";
	public static final String path_FileOutMp3 =  path_Mix+ File.separator + "mixed.mp3";
	
	public enum Keys {
		IS_UPLOAD,
		INTRO_NEW_HOMEPAGE,
		INTRO_NEW_RECORDLIST,
		INTRO_NEW_WRITER,
		WARNING_JOIN 
	}
	
	public static void upLog(Context context){
		UtilityMain.isRecordLog = true;
		UtilityMain.DEBUG_D = false;
		UtilityMain.CRASH_RECORD = true;
		UtilityMain.DEBUG_N = true;
		UtilityMain.BUILDDAY = "20150605";
		UtilityMain.isUpLog = true;
		UtilityMain.start(context);
	}

	public static int getTimeZoneInLocal() {
		TimeZone timeZone = TimeZone.getDefault();
		int timeZoneOffSet = timeZone.getRawOffset() / 1000 / 3600;
		DebugLog.logd("Utility getTimeZoneInLocal " + "time zome off set" + timeZoneOffSet);
		return timeZoneOffSet;
	}

	public static void showToast(Context context, String message) {
		if (!message.isEmpty()) {
			Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 230);
			toast.show();
		}
	}
	
	public static void dismissCurrentDialog() {
		try {
			if (CacheData.getInstant().getCurrentDialog() != null) {
				if (CacheData.getInstant().getCurrentDialog().isShowing()) {
					CacheData.getInstant().getCurrentDialog().dismiss();
				}
				CacheData.getInstant().setCurrentDialog(null);
			}
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}
	}

	public static void dismissCurrentDialog(Dialog dialog) {
		try {
			if (CacheData.getInstant().getCurrentDialog() != null) {
				if (CacheData.getInstant().getCurrentDialog().isShowing()) {
					CacheData.getInstant().getCurrentDialog().dismiss();
				}
				CacheData.getInstant().setCurrentDialog(null);
			}			
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}
	}

	public static void showProgressDialog(Context context, String message) {
		dismissCurrentDialog();
		ProgressDialog dialogProgress = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
		dialogProgress.setCancelable(false);
		dialogProgress.setMessage(message);
		try {
			dialogProgress.show();			
		} catch (Exception e) {
			DebugLog.loge(e.getMessage());
		}

		CacheData.getInstant().setCurrentDialog(dialogProgress);
	}

	public static void showProgressDialog(Context context, String message,
			android.content.DialogInterface.OnCancelListener cancelListener) {
		dismissCurrentDialog();
		ProgressDialog dialogProgress = new ProgressDialog(context,
				ProgressDialog.THEME_HOLO_LIGHT);
		dialogProgress.setCancelable(true);
		dialogProgress.setMessage(message);
		dialogProgress.show();
		dialogProgress.setOnCancelListener(cancelListener);

		CacheData.getInstant().setCurrentDialog(dialogProgress);
	}

	public static void showProgressDialogTimeout(final Context context,
			String message, Handler handler, Runnable runnable, long timeOut) {

		dismissCurrentDialog();
		final ProgressDialog dialogProgress = new ProgressDialog(context,
				ProgressDialog.THEME_HOLO_LIGHT);
		dialogProgress.setCancelable(false);
		dialogProgress.setMessage(message);
		dialogProgress.show();

		CacheData.getInstant().setCurrentDialog(dialogProgress);

		handler.postDelayed(runnable, timeOut);
	}

	public static void showProgressDialog(final Context context,
			String message, Handler handler, Runnable runnable, long timeOut,
			android.content.DialogInterface.OnCancelListener cancelListener) {

		dismissCurrentDialog();
		final ProgressDialog dialogProgress = new ProgressDialog(context,
				ProgressDialog.THEME_HOLO_LIGHT);
		dialogProgress.setCancelable(true);
		dialogProgress.setMessage(message);
		dialogProgress.show();
		dialogProgress.setOnCancelListener(cancelListener);

		CacheData.getInstant().setCurrentDialog(dialogProgress);

		handler.postDelayed(runnable, timeOut);
	}

	public static boolean isNullOrEmpty(String inputString) {
		if (inputString == null || inputString.isEmpty()) {
			return true;
		}
		return false;
	}

	public static void showErrorNullOrEmpty(final EditText editText,
			final String errorMessage) {
		editText.setError(errorMessage);
		editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View arg0, boolean arg1) {
				if (editText == null || editText.getText().toString().isEmpty()
						|| editText.getText().toString().equals("")) {
					editText.setError(errorMessage);
				}
			}
		});
	}

	public static void hiddenSofwareKeyboard(Context context, View view) {
		if (view != null) {
			try {
				EditText edt = (EditText) view;
				InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
			} catch (Exception e) {
				DebugLog.loge(e.getMessage());
			}
		}
	}

	// public static void showErrorMessageWithCheckInternetConnection(Context
	// context, String internetConnectionErrorMessage, String message) {
	// if (NetworkUtil.NETWORK_STATE == NetworkUtil.TYPE_NOT_CONNECTED ||
	// NetworkUtil.NETWORK_STATE == -1) {
	// Utils.showToast(context, internetConnectionErrorMessage);
	// } else {
	// Utils.showToast(context, message);
	// }
	// }

	public static boolean validateEmail(String email) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
//				("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
		if (pattern.matcher(email).matches()) {
			return true;
		}
		return false;
	}
	
	public static void renameRecord(String path, String oldname, String newname){
		File from = new File(path + File.separator, oldname+".mp3");
		File to = new File(path+ File.separator, newname+".mp3");
		from.renameTo(to);
	}
	
	public static void rename(String oldFile, String newFile){
		File from = new File(oldFile);
		File to = new File(newFile);
		from.renameTo(to);
	}

	public static void callEmailApplication(Context context, String email,
			String subject, String bodyText) {
		final Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("text/message");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, bodyText);
		context.startActivity(Intent.createChooser(emailIntent, "Email Client")
				.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
	}

	public static String getUrl(int pos) {
		String url = "";

		if(pos== 0){
			url = Url.API_LIKE_SESSION_BY_USERNAME;
		}else if(pos == 1){
			url = Url.API_LIKE_SESSION_BY_NEWEST;
		}else if(pos == 2){
			url = Url.API_LIKE_SESSION_BY_MOST_HEARD;
		}

		return url;
	}
	
	public static String getLastTokenKeyPhoto(String lstImageToken) {
		String fileName = "";

		int i = lstImageToken.lastIndexOf(';');
		if (i > 0) {
			fileName = lstImageToken.substring(i + 1);
		}

		return fileName;
	}

	public static String removeSpaces(String text) {
		String txt = null;
		txt = text.replace(" ", "");
		return txt;
	}

	public static String[] splitComme(String text) {
		String[] items = text.split(";");
		return items;
	}

	public static void deleteFile(String path) {
		File file = new File(path);
		DebugLog.logd("Upload start delete file " + path);
		if (file.exists()) {
			boolean result = file.delete();
			DebugLog.logd("Upload delete file " + result);
		}
	}

	public static void checkSocket(final Context context) {
		// manager = new AuthenManager();
		// manager.checkSocket(AccountDao.getAccount().getTokenKey(), new
		// OnCheckSocketResult() {
		//
		// @Override
		// public void onGetCheckSocketResultMethod(boolean isSuccess) {
		// if(isSuccess){
		// DebugLog.logd("status socket: ok");
		// }else{
		// DebugLog.loge("status socket: failed");
		// BroadcastClient broadcastClient = new BroadcastClient(context);
		// broadcastClient.start(AccountDao.getAccount().getTokenKey());
		// }
		// }
		// });
	}

	@SuppressLint("DefaultLocale")
	public static String getDuration(int time) {
		return String.format("%02d:%02d", (time % 3600) / 60, (time % 60));
	}

	public static String getDurationLongTime(int time) {
		return String.format("%02d:%02d:%02d", time / 3600, (time % 3600) / 60,
				(time % 60));
	}

	public static String milliSecondsToTimer(long milliseconds) {
		String finalTimerString = "";
		String minutesString = "";
		String secondsString = "";

		// Convert total duration into time
		int hours = (int) (milliseconds / (1000 * 60 * 60));
		int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
		int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
		// Add hours if there
		
		if (hours > 0) {
			if (hours < 10) {
				finalTimerString = "0" + hours + ":";
			}else{
				finalTimerString = hours + ":";				
			}
		}else{
			finalTimerString = "00" + ":";
		}
		
		if(minutes < 10){
			minutesString = "0" + minutes;
		} else {
			minutesString = "" + minutes;
		}

		// Prepending 0 to seconds if it is one digit
		if (seconds < 10) {
			secondsString = "0" + seconds;
		} else {
			secondsString = "" + seconds;
		}

		finalTimerString = finalTimerString + minutesString + ":" + secondsString;

		// return timer string
		return finalTimerString;
	}
	
	public static String milliSecondsToTimer2(long milliseconds) {
		String finalTimerString = "";
		String minutesString = "";
		String secondsString = "";

		// Convert total duration into time
		int hours = (int) (milliseconds / (1000 * 60 * 60));
		int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
		int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
		// Add hours if there
		
		if (hours > 0) {
			if (hours < 10) {
				finalTimerString = "0" + hours + ":";
			}else{
				finalTimerString = hours + ":";				
			}
		}else{
			finalTimerString = "";
		}
		
		if(minutes < 10){
			minutesString = "0" + minutes;
		} else {
			minutesString = "" + minutes;
		}

		// Prepending 0 to seconds if it is one digit
		if (seconds < 10) {
			secondsString = "0" + seconds;
		} else {
			secondsString = "" + seconds;
		}

		finalTimerString = finalTimerString + minutesString + ":" + secondsString;

		// return timer string
		return finalTimerString;
	}
	
	public static int getProgressPercentage(long currentDuration,
			long totalDuration) {
		Double percentage = (double) 0;

		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);

		// calculating percentage
		percentage = (((double) currentSeconds) / totalSeconds) * 100;

		// return percentage
		return percentage.intValue();
	}

	public static int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double) progress) / 100) * totalDuration);

		return currentDuration * 1000;
	}

	public static String milliSecondsToSeconds(long milliseconds) {
		int seconds = (int) (milliseconds / 1000) ;
		return Integer.toString(seconds);
	}
	
	public static void delete(String url){
		File file = new File(url);
		if(file!=null){
			file.delete();
		}
	}
	
	public static File[] listValidFiles(File file) {
	    return file.listFiles(new FilenameFilter() {

	        @Override
	        public boolean accept(File dir, String filename) {
	            File file2 = new File(dir, filename);
	            return (filename.contains(".mp3"))
	                    && !file2.isHidden()
	                    && !filename.startsWith(".");

	        }
	    });
	}
	
	@SuppressLint("SimpleDateFormat")
	public static ArrayList<Record> loadRecord(String path) {
		File f = new File(path);
		File[] files = listValidFiles(f);
		if (files != null) {
			ArrayList<Record> recordArray = new ArrayList<Record>();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				Record re = new Record();
				String name = file.getName();
				if(!name.contains("mixed.mp3")){
					int pos = name.lastIndexOf(".");
					if (pos > 0) {
						name = name.substring(0, pos);
					}
					
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
					
					if (name != null) {
						re.setName_record(name);
						re.setDate_create(sdf.format(file.lastModified()) + "");
						re.setFile_record(file.getPath());
						re.setPlayShow(false);
//						re.setLong_time("");
						recordArray.add(re);
					}
				}
			}
			return recordArray;
		} else
			return null;
	}
	
	public static String getFileNameAndExtension(String strUrl) {
		String fileName = "";

		int i = strUrl.lastIndexOf('/');
		if (i > 0) {
			fileName = strUrl.substring(i + 1);
		}

		return fileName;
	}
	
	public static String getExtension(String strUrl) {
		String fileName = "";

		int i = strUrl.lastIndexOf('.');
		if (i > 0) {
			fileName = strUrl.substring(i + 1);
		}

		return fileName;
	}
	
	public static int cycles = 10;
	
	public static int getRealPosition(int virtualPosition, int size){
		int pos = virtualPosition - (size*cycles);
		while (pos>size) {
			pos = pos - size;
		}
		while (pos<0) {
			pos = pos + size;
		}
		if (pos == size) {
			pos = 0;
		}
		 return pos;
	}
	
	public static String getFileName(String strUrl) {
		String fileName =  strUrl.substring(strUrl.lastIndexOf('/')+1, strUrl.lastIndexOf('.'));
		return fileName;
	}
	
	public static long getDurationFile(String filePath){
		MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
		metaRetriever.setDataSource(filePath);
		long dur = Long.parseLong(metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
		return dur;
	}
	
	public static void showSetLatecy(Dialog dialog, final boolean isFirst, View.OnClickListener onClickListener){
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog);
		
		TextView btn_Yes = (TextView) dialog.findViewById(R.id.btn_Yes);
		EditText edt_latecy = (EditText) dialog.findViewById(R.id.edt_latecy);
		
		edt_latecy.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(isFirst){
					CacheData.getInstant().setLatecy1(s.toString());					
				}else{
					CacheData.getInstant().setLatecy2(s.toString());
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		btn_Yes.setOnClickListener(onClickListener);
		dialog.show();
	}
	
	public static void showDialogOK(Context context, Dialog dialog, String title, String content, String txtBtn1, OnClickListener btn1click) {
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_ok);
		dialog.setCancelable(false);
		
		TextView txtTitle = (TextView) dialog.findViewById(R.id.title);
		TextView txtContent = (TextView) dialog.findViewById(R.id.content);
		Button btn1 = (Button) dialog.findViewById(R.id.btn1);
		
		if(!txtBtn1.equals("")) btn1.setText(txtBtn1);
		
		if(title.equals("")){
			txtTitle.setVisibility(View.GONE);
		}else{
			txtTitle.setText(title);
			txtTitle.setVisibility(View.VISIBLE);
		}
		
		txtContent.setText(content);
		
		btn1.setOnClickListener(btn1click);

		try {
			dialog.show();
		} catch (Exception e) {
		}
	}
	
	public static void showDialog2(Context context, Dialog dialog, String title, String content, String txtBtn1, String txtBtn2, OnClickListener btn1click, OnClickListener btn2click) {
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_2);
		dialog.setCancelable(false);
		
		TextView txtTitle = (TextView) dialog.findViewById(R.id.title);
		TextView txtContent = (TextView) dialog.findViewById(R.id.content);
		Button btn1 = (Button) dialog.findViewById(R.id.btn1);
		Button btn2 = (Button) dialog.findViewById(R.id.btn2);
		
		if(!txtBtn1.equals("")) btn1.setText(txtBtn1);
		if(!txtBtn2.equals("")) btn2.setText(txtBtn2);
		
		if(title.equals("")){
			txtTitle.setVisibility(View.GONE);
		}else{
			txtTitle.setText(title);
			txtTitle.setVisibility(View.VISIBLE);
		}
		
		txtContent.setText(content);
		
		btn1.setOnClickListener(btn1click);
		btn2.setOnClickListener(btn2click);

		try {
			dialog.show();
		} catch (Exception e) {
		}
	}
	
	public static void showDialog3(Context context, Dialog dialog, String title, String content, String txtBtn1, String txtBtn2, String txtBtn3, OnClickListener btn1click, OnClickListener btn2click, OnClickListener btn3click) {
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_3);
		dialog.setCancelable(false);
		
		TextView txtTitle = (TextView) dialog.findViewById(R.id.title);
		TextView txtContent = (TextView) dialog.findViewById(R.id.content);
		Button btn1 = (Button) dialog.findViewById(R.id.btn1);
		Button btn2 = (Button) dialog.findViewById(R.id.btn2);
		Button btn3 = (Button) dialog.findViewById(R.id.btn3);
		
		if(!txtBtn1.equals("")) btn1.setText(txtBtn1);
		if(!txtBtn2.equals("")) btn2.setText(txtBtn2);
		if(!txtBtn3.equals("")) btn3.setText(txtBtn3);
		
		if(title.equals("")){
			txtTitle.setVisibility(View.GONE);
		}else{
			txtTitle.setText(title);
			txtTitle.setVisibility(View.VISIBLE);
		}
		
		txtContent.setText(content);
		
		btn1.setOnClickListener(btn1click);
		btn2.setOnClickListener(btn2click);
		btn3.setOnClickListener(btn3click);

		try {
			dialog.show();
		} catch (Exception e) {
		}
	}
}
