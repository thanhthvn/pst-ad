package com.cncsoftgroup.panstage.common;

public class PanstageConst {
	public static final int FIRST_PAGE = 0;
	public static final int SECOND_PAGE = 1;
	public static final int THIRD_PAGE = 2;
	public static final int FOURTH_PAGE = 3;
	public static final int FIFTH_PAGE = 4;
}
