package com.cncsoftgroup.panstage.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SharedPreference {
	public static SharedPreferences sharedPreferences;

	public static String loadRole(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String role = "";
		try {
			role = sharedPreferences.getString("ACCOUNT_ROLE", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}
	
	
	
	public static void saveRole(Context context, String role) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("ACCOUNT_ROLE", role);
		editor.commit();
	}
	public static void saveRoleRegister(Context context, String role) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("ACCOUNT_ROLE", role);
		editor.commit();
	}
	public static String loadRoleRegister(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String role = "";
		try {
			role = sharedPreferences.getString("ACCOUNT_ROLE", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}
	
	public static String loadEmail(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String email = "";
		try {
			email = sharedPreferences.getString("ACCOUNT_EMAIL", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
	}
	
	public static String loadPassword(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String password = "";
		try {
			password = sharedPreferences.getString("ACCOUNT_PASSWORD", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return password;
	}

	public static void saveEmailAndPassword(Context context, String email, String password) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("ACCOUNT_EMAIL", email);
		editor.putString("ACCOUNT_PASSWORD", password);
		editor.commit();
	}
	
	public static boolean isLoginFacebook(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		boolean time = false;
		try {
			time = sharedPreferences.getBoolean("IS_LOGIN_FACEBOOK", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}
	
	public static void hasLoginFacebook(Context context, boolean time) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean("IS_ALREADY_LOGIN_FACEBOOK", time);
		editor.commit();
	}
	
	public static boolean isAlreadyLoginFacebook(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		boolean time = false;
		try {
			time = sharedPreferences.getBoolean("IS_ALREADY_LOGIN_FACEBOOK", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}
	
	public static void hasAlreadyLoginFacebook(Context context, boolean time) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putBoolean("IS_LOGIN_FACEBOOK", time);
		editor.commit();
	}
	
	public static int loadUid(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		int uid = 0;
		try {
			uid = sharedPreferences.getInt("UID", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uid;
	}
	
	public static void saveUid(Context context, int uid) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putInt("UID", uid);
		editor.commit();
	}
	
	public static String loadFullName(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String full_name = "";
		try {
			full_name = sharedPreferences.getString("FULL_NAME", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return full_name;
	}
	
	public static void saveFullName(Context context, String full_name) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("FULL_NAME", full_name);
		editor.commit();
	}
	
	public static String loadAvata(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String avatar = "";
		try {
			avatar = sharedPreferences.getString("AVATAR", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return avatar;
	}
	
	public static void saveAvatar(Context context, String avatar) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("AVATAR", avatar);
		editor.commit();
	}
	
	public static String loadRoleDescription(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String role_description = "";
		try {
			role_description = sharedPreferences.getString("ROLE_DESCRIPTION", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role_description;
	}
	
	public static void saveRoleDescription(Context context, String role_description) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("ROLE_DESCRIPTION", role_description);
		editor.commit();
	}
	
	public static String loadAuthu_token(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String authu_token = "";
		try {
			authu_token = sharedPreferences.getString("AUTHU_TOKEN", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return authu_token;
	}
	
	public static void saveAuthu_token(Context context, String role) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("AUTHU_TOKEN", role);
		editor.commit();
	}
	
	public static int loadLatency(Context context) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		try {
			return sharedPreferences.getInt("LATENCY", 0);
		} catch (Exception e) {
			return 0;
		}
	}
	
	public static void saveLatency(Context context, int latency) {
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putInt("LATENCY", latency);
		editor.commit();
	}
	
}
