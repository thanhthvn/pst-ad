package com.cncsoftgroup.panstage.common;

public class Url {
public static final String DOMAIN = "http://54.193.32.207/api/v1/";
	
	public static final String API_CREATE_SESSION = DOMAIN + "create_session.json";
	
	public static final String API_GET_SESSION_DETAILS_V2 = DOMAIN + "get_session_detail_v2.json";
	
	public static final String API_GET_SESSION_DETAILS = DOMAIN + "get_session_detail.json";
	
	public static final String API_GET_USER_CHANNEL = DOMAIN + "get_user_chanel.json";
	
	public static final String API_JOIN_SESSION = DOMAIN + "join_session.json";
	
	public static final String API_LIKE_SESSION = DOMAIN + "like_session.json";
	
	public static final String API_LIKE_SESSION_BY_NEWEST = DOMAIN + "list_session_by_newest.json";
	
	public static final String API_LIKE_SESSION_BY_MOST_HEARD = DOMAIN + "list_session_by_most_heard.json";
	
	public static final String API_LIKE_SESSION_BY_SONG_NAME = DOMAIN + "list_session_by_song_name.json";
	
	public static final String API_LIKE_SESSION_BY_USERNAME = DOMAIN + "list_session_by_username.json";
	
	public static final String API_LOGIN_FACEBOOK = DOMAIN + "login_fb.json";
	
	public static final String API_LOGIN = DOMAIN + "login.json";
	
	public static final String API_LOGOUT = DOMAIN + "logout.json";
	
	public static final String API_REGISTRATION = DOMAIN + "registration.json";
	
	public static final String API_UPDATE_LISTERNER_COUNT = DOMAIN + "update_listener_count.json";
	
	public static final String API_UPDATE_LYRICS = DOMAIN + "update_lyric.json";
	
	public static final String API_UPDATE_SESSION = DOMAIN + "update_session.json";
	
	public static final String API_UPDATE_DESCRIPTION = DOMAIN + "update_description.json";
	
	public static final String API_LOCK_SESSION = DOMAIN + "lock_session.json";
	
	public static final String API_UNLOCK_SESSION = DOMAIN + "unlock_session.json";
}
