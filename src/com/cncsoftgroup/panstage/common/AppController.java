package com.cncsoftgroup.panstage.common;

import org.acra.annotation.ReportsCrashes;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.cache.LruBitmapCache;

@ReportsCrashes(
        formUri = "https://infinite-plateau-4155.herokuapp.com/reports"
    )

public class AppController extends Application {

	public static final String TAG = "Fanstage";

	private RequestQueue mRequestQueue;

	private static AppController mInstance;
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressWarnings("unused")
	@Override
	public void onCreate() {
		mInstance = this;
		if (Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
		}
		super.onCreate();
		
//		ACRA.init(this);
		
		Utils.upLog(getApplicationContext());
		initImageLoader(getApplicationContext());
		thumbnailImageManagerSettings();
	}

	public static void initImageLoader(Context context) {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).memoryCache(new WeakMemoryCache())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

	public static synchronized AppController getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}

	/**
	 * It is possible to keep a static reference across the application of the
	 * image loader.
	 */
	private static ImageManager imageManager;

	private void thumbnailImageManagerSettings() {
		imageManager = new ImageManager(this, new SettingsBuilder()
		// .withCacheManager(new LruBitmapCache(this))
				.withCacheManager(new LruBitmapCache(this, 25)).build(this));
	}

	/**
	 * There are different settings that you can use to customize the usage of
	 * the image loader for your application.
	 */
	@SuppressWarnings("unused")
	private void verboseImageManagerSettings() {
		SettingsBuilder settingsBuilder = new SettingsBuilder();

		// You can force the urlConnection to disconnect after every call.
		settingsBuilder.withDisconnectOnEveryCall(true);

		// We have different types of cache, check cache package for more info
		settingsBuilder.withCacheManager(new LruBitmapCache(this));

		// You can set a specific read timeout
		settingsBuilder.withReadTimeout(30000);

		// You can set a specific connection timeout
		settingsBuilder.withConnectionTimeout(30000);

		// You can disable the multi-threading ability to download image
		settingsBuilder.withAsyncTasks(false);

		// You can set a specific directory for caching files on the sdcard
		// settingsBuilder.withCacheDir(new File("/something"));

		// Setting this to false means that file cache will use the url without
		// the query part
		// for the generation of the hashname
		settingsBuilder.withEnableQueryInHashGeneration(false);

		LoaderSettings loaderSettings = settingsBuilder.build(this);
		imageManager = new ImageManager(this, loaderSettings);
	}

	/**
	 * Convenient method of access the imageLoader
	 */
	public static ImageManager getImageManager() {
		return imageManager;
	}
}