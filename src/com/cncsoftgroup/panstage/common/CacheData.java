package com.cncsoftgroup.panstage.common;

import java.util.ArrayList;

import android.app.ProgressDialog;

import com.cncsoftgroup.panstage.bean.Lyrics;
import com.cncsoftgroup.panstage.bean.Record;
import com.cncsoftgroup.panstage.bean.Session;
import com.nmd.libs.DebugLog;

public class CacheData {
	private static CacheData instant = null;

	ArrayList<Session> listSession = new ArrayList<Session>();

	private Record record = null;

	private boolean isLoading = false;

	private boolean isLoadMore = false;

	private int page_position = 1;

	private int duration = -1;

	private int sid_join = -1;

	private int audio_session = -1;

	private boolean is_invite = false;

	private boolean isIntroActivity = false;

	private boolean is_Done = false;

	private boolean isMix = false;

	private boolean isJoin = false;

	private boolean isJoinLyrics = false;

	private boolean isCreateSession = false;

	private boolean isUploadSession = false;

	private int page_tab;

	private String name_record = "";

	private String latecy1 = "0";

	private String latecy2 = "0";

	private Lyrics lyrics = new Lyrics();

	private String message = "";

	private String acces_token = "";

	private String url_upload = "";

	private boolean aboutView = false;

	private String auth_token = "";

	private int currSid = -1;

	private int uid = -1;

	private int sid = -1;

	private String lyric;

	private Boolean is_join_record = false;

	private Boolean is_update_session = false;

	private int sid_update;

	private Boolean userPage = false;

	private boolean isHomeActivity = false;

	private String url_mix = "";

	private String url_test = "";

	private boolean isSessionDetail = false;

	public int getPage_tab() {
		return page_tab;
	}

	public void setPage_tab(int page_tab) {
		this.page_tab = page_tab;
	}

	public Boolean isUserPage() {
		return userPage;
	}

	public void setUserPage(Boolean userPage) {
		this.userPage = userPage;
	}

	public int getSid_update() {
		return sid_update;
	}

	public void setSid_update(int sid_update) {
		this.sid_update = sid_update;
	}

	public Boolean is_update_session() {
		return is_update_session;
	}

	public void setIs_update_session(Boolean is_update_session) {
		this.is_update_session = is_update_session;
	}

	public Boolean is_join_record() {
		return is_join_record;
	}

	public void setIs_join_record(Boolean is_join_record) {
		this.is_join_record = is_join_record;
	}

	public String getLyric() {
		return lyric;
	}

	public void setLyric(String lyric) {
		this.lyric = lyric;
	}

	private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	private String role_description;

	public String getRole_description() {
		return role_description;
	}

	public void setRole_description(String role_description) {
		this.role_description = role_description;
	}

	private String full_name = "";

	private ProgressDialog currentDialog = null;

	private String avatar = "";

	public static CacheData getInstant() {
		if (instant == null) {
			instant = new CacheData();
		}
		return instant;
	}

	public String getAcces_token() {
		return acces_token;
	}

	public void setAcces_token(String acces_token) {
		this.acces_token = acces_token;
	}

	public String getAuth_token() {
		return auth_token;
	}

	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public ProgressDialog getCurrentDialog() {
		return currentDialog;
	}

	public void setCurrentDialog(ProgressDialog currentDialog) {
		this.currentDialog = currentDialog;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	private boolean isLoginFacebook;

	public boolean isLoginFacebook() {
		return isLoginFacebook;
	}

	public void setLoginFacebook(boolean isLoginFacebook) {
		this.isLoginFacebook = isLoginFacebook;
	}

	public ArrayList<Session> getListSession() {
		return listSession;
	}

	public void setListSession(ArrayList<Session> listSession) {
		this.listSession = listSession;
	}

	public int getPage_position() {
		return page_position;
	}

	public void setPage_position(int page_position) {
		this.page_position = page_position;
	}

	public int getAudio_session() {
		return audio_session;
	}

	public void setAudio_session(int audio_session) {
		this.audio_session = audio_session;
	}

	public boolean isLoading() {
		return isLoading;
	}

	public void setLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}

	public boolean isIs_invite() {
		return is_invite;
	}

	public void setIs_invite(boolean is_invite) {
		this.is_invite = is_invite;
	}

	public String getUrl_upload() {
		return url_upload;
	}

	public void setUrl_upload(String url_upload) {
		this.url_upload = url_upload;
	}

	public boolean isMix() {
		return isMix;
	}

	public void setMix(boolean isMix) {
		this.isMix = isMix;
	}

	public boolean isLoadMore() {
		return isLoadMore;
	}

	public void setLoadMore(boolean isLoadMore) {
		this.isLoadMore = isLoadMore;
	}

	public Record getRecord() {
		return record;
	}

	public void setRecord(Record record) {
		this.record = record;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public boolean isUploadSession() {
		DebugLog.loge("_get_:" + isUploadSession);
		return isUploadSession;
	}

	public void setUploadSession(boolean isUploadSession) {
		DebugLog.loge("_set_:" + isUploadSession);
		this.isUploadSession = isUploadSession;
	}

	public boolean isCreateSession() {
		return isCreateSession;
	}

	public void setCreateSession(boolean isCreateSession) {
		this.isCreateSession = isCreateSession;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public boolean isJoin() {
		return isJoin;
	}

	public void setJoin(boolean isJoin) {
		this.isJoin = isJoin;
	}

	public String getName_record() {
		return name_record;
	}

	public void setName_record(String name_record) {
		this.name_record = name_record;
	}

	public boolean isHomeActivity() {
		return isHomeActivity;
	}

	public void setHomeActivity(boolean isHomeActivity) {
		this.isHomeActivity = isHomeActivity;
	}

	public String getUrl_mix() {
		return url_mix;
	}

	public void setUrl_mix(String url_mix) {
		this.url_mix = url_mix;
	}

	public boolean isSessionDetail() {
		return isSessionDetail;
	}

	public void setSessionDetail(boolean isSessionDetail) {
		this.isSessionDetail = isSessionDetail;
	}

	public void setAboutView(boolean aboutView) {
		this.aboutView = aboutView;
	}

	public boolean isAboutView() {
		return aboutView;
	}

	public int getCurrSid() {
		return currSid;
	}

	public void setCurrSid(int currSid) {
		this.currSid = currSid;
	}

	public boolean isIs_Done() {
		return is_Done;
	}

	public void setIs_Done(boolean is_Done) {
		this.is_Done = is_Done;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getSid_join() {
		return sid_join;
	}

	public void setSid_join(int sid_join) {
		this.sid_join = sid_join;
	}

	public String getLatecy1() {
		return latecy1;
	}

	public void setLatecy1(String latecy1) {
		this.latecy1 = latecy1;
	}

	public String getLatecy2() {
		return latecy2;
	}

	public void setLatecy2(String latecy2) {
		this.latecy2 = latecy2;
	}

	public boolean isIntroActivity() {
		return isIntroActivity;
	}

	public void setIntroActivity(boolean isIntroActivity) {
		this.isIntroActivity = isIntroActivity;
	}

	public String getUrl_test() {
		return url_test;
	}

	public void setUrl_test(String url_test) {
		this.url_test = url_test;
	}

	public Lyrics getLyrics() {
		return lyrics;
	}

	public void setLyrics(Lyrics lyrics) {
		this.lyrics = lyrics;
	}

	public boolean isJoinLyrics() {
		return isJoinLyrics;
	}

	public void setJoinLyrics(boolean isJoinLyrics) {
		this.isJoinLyrics = isJoinLyrics;
	}

}
