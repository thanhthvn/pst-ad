package com.cncsoftgroup.panstage.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.nmd.libs.DebugLog;

/**
 * Class contain all method that used to connect to web services.
 * 
 * @author NIW
 * 
 */
public class NetworkService {
	
	private static String DOMAIN = "http://54.193.32.207/api/v1/";
	
	private static String API_REGISTRATION = DOMAIN + "registration.json";
	
	private static String API_CREATE_SESSION = DOMAIN + "create_session.json";
	
	private static String API_UPDATE_SESSION = DOMAIN + "update_session.json";
	
//	private static String API_GET_SESSION_DETAILS_V2 = DOMAIN + "get_session_detail_v2.json";
//	
//	private static String API_GET_SESSION_DETAILS = DOMAIN + "get_session_detail.json";
//	
//	private static String API_GET_USER_CHANNEL = DOMAIN + "get_user_chanel.json";
//	
//	private static String API_JOIN_SESSION = DOMAIN + "join_session.json";
//	
//	private static String API_LIKE_SESSION = DOMAIN + "like_session.json";
//	
//	private static String API_LIKE_SESSION_BY_NEWEST = DOMAIN + "list_session_by_newest.json";
//	
//	private static String API_LIKE_SESSION_BY_MOST_HEARD = DOMAIN + "list_session_by_most_heard.json";
//	
//	private static String API_LIKE_SESSION_BY_SONG_NAME = DOMAIN + "list_session_by_song_name.json";
//	
//	private static String API_LIKE_SESSION_BY_USERNAME = DOMAIN + "list_session_by_username.json";
//	
//	private static String API_LOGIN_FACEBOOK = DOMAIN + "login_fb.json";
//	
//	private static String API_LOGIN = DOMAIN + "login.json";
//	
//	private static String API_LOGOUT = DOMAIN + "logout.json";
//	
//	private static String API_UPDATE_LISTERNER_COUNT = DOMAIN + "update_listener_count.json";
//	
//	private static String API_UPDATE_LYRICS = DOMAIN + "update_lyric.json";
//	
//	private static String API_UPDATE_DESCRIPTION = DOMAIN + "update_description.json";
//	
//	private static String API_LOCK_SESSION = DOMAIN + "lock_session.json";
//	
//	private static String API_UNLOCK_SESSION = DOMAIN + "unlock_session.json";
//	
//	private final String LOG_TAG = "Span";
//	
//	private JSONObject getResponseData(HttpURLConnection urlc)
//			throws IOException, JSONException {
//		// start request
//		urlc.connect();
//		int result = urlc.getResponseCode();
//		if (result == HttpURLConnection.HTTP_OK) {
//			StringBuilder sb = new StringBuilder();
//			BufferedReader br = new BufferedReader(new InputStreamReader(
//					urlc.getInputStream(), "utf-8"));
//
//			String line = null;
//			while ((line = br.readLine()) != null) {
//				sb.append(line + "\n");
//			}
//			br.close();
//			JSONObject jsonResult = new JSONObject(sb.toString());
//			return jsonResult;
//		} else {
//			Log.e(LOG_TAG, "Connection return with result " + result);
//			return null;
//		}
//	}
	
//	private HttpURLConnection createConnectionWithURL(String strUrl)
//			throws IOException {
//		URL url = new URL(strUrl);
//		HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
//		return urlc;
//	}

//	private void configURLConnection(HttpURLConnection urlc)
//			throws ProtocolException {
//		urlc.setRequestMethod("POST");
//		urlc.setDoOutput(true);
//		urlc.setDoInput(true);
//		urlc.setUseCaches(false);
//		urlc.setAllowUserInteraction(false);
//		urlc.setConnectTimeout(20000);
//		urlc.setRequestProperty("Content-Type","application/json");   
//	}
	
//	private void writeParamAPI(HttpURLConnection urlc, JSONObject jsonParam)
//			throws IOException, UnsupportedEncodingException {
//		OutputStreamWriter out = new OutputStreamWriter(urlc.getOutputStream());
//		out.write(URLEncoder.encode(jsonParam.toString(),"UTF-8"));
//		out.flush ();
//		out.close();
//	}
	
	public JSONObject registration(String full_name, String email, 
			String password, String role, File avatar) {
		String execute = "";
		DefaultHttpClient mHttpClient;
		try {
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");
			mHttpClient = new DefaultHttpClient(params);

			HttpPost httppost = new HttpPost(API_REGISTRATION);
			httppost.addHeader("Charset", "UTF-8");
			
			MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

			multipartEntity.addPart("user[full_name]", new StringBody(full_name, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[email]", new StringBody(email, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[password]", new StringBody(password, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[role]", new StringBody(role));
			multipartEntity.addPart("avatar", new FileBody(avatar));
			httppost.setEntity(multipartEntity);
			
			DebugLog.logd("register: {API: " + httppost.getURI()  + " }");

			execute = mHttpClient.execute(httppost,
					new ResponseHandler<String>() {
						@Override
						public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
							HttpEntity r_entity = response.getEntity();
							String responseString = EntityUtils.toString(r_entity);
							return responseString;
						}
					});
			
			JSONObject jsonResult = new JSONObject(execute);
			if (jsonResult != null) {
				DebugLog.logd("register result: " + jsonResult.toString());
			}
			
			return jsonResult;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject registration(String full_name, String email, 
			String password, String role) {
		String execute = "";
		DefaultHttpClient mHttpClient;
		try {
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");
			mHttpClient = new DefaultHttpClient(params);

			HttpPost httppost = new HttpPost(API_REGISTRATION);
			httppost.addHeader("Charset", "UTF-8");
			
			MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

			multipartEntity.addPart("user[full_name]", new StringBody(full_name, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[email]", new StringBody(email, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[password]", new StringBody(password, Charset.forName("UTF-8")));
			multipartEntity.addPart("user[role]", new StringBody(role));
			httppost.setEntity(multipartEntity);
			
			DebugLog.logd("register: {API: " + httppost.getURI() + " }");

			execute = mHttpClient.execute(httppost,
					new ResponseHandler<String>() {
						@Override
						public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
							HttpEntity r_entity = response.getEntity();
							String responseString = EntityUtils.toString(r_entity);
							return responseString;
						}
					});
			
			JSONObject jsonResult = new JSONObject(execute);
			if (jsonResult != null) {
				DebugLog.logd("register result: " + jsonResult.toString());
			}
			
			return jsonResult;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject createSession(String auth_token, String session_name, String duration, File upload_file, String lyric, String lyric_title) {
		String execute = "";
		DefaultHttpClient mHttpClient;
		try {
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");
			mHttpClient = new DefaultHttpClient(params);

			HttpPost httppost = new HttpPost(API_CREATE_SESSION);
			httppost.addHeader("Charset", "UTF-8");
			MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			DebugLog.loge("duration: "+duration);
			
			multipartEntity.addPart("auth_token", new StringBody(auth_token));
			multipartEntity.addPart("session_name", new StringBody(session_name, Charset.forName("UTF-8")));
			multipartEntity.addPart("duration", new StringBody(duration));
			multipartEntity.addPart("upload_file", new FileBody(upload_file));
			multipartEntity.addPart("lyric", new StringBody(lyric, Charset.forName("UTF-8")));
			multipartEntity.addPart("lyric_title",new StringBody(lyric_title, Charset.forName("UTF-8")));
			httppost.setEntity(multipartEntity);
			
			DebugLog.logd("register: {API: " + httppost.getURI()  + " }");

			execute = mHttpClient.execute(httppost,
					new ResponseHandler<String>() {
						@Override
						public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
							HttpEntity r_entity = response.getEntity();
							String responseString = EntityUtils.toString(r_entity);
							return responseString;
						}
					});
			
			JSONObject jsonResult = new JSONObject(execute);
			if (jsonResult != null) {
				DebugLog.logd("uploadAvatarImage result: " + jsonResult.toString());
			}
			
			return jsonResult;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONObject updateSession(String auth_token, String sid, 
			String duration, File upload_file) {
		String execute = "";
		DefaultHttpClient mHttpClient;
		try {
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");
			mHttpClient = new DefaultHttpClient(params);

			HttpPost httppost = new HttpPost(API_UPDATE_SESSION);
			httppost.addHeader("Charset", "UTF-8");
			MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			DebugLog.loge("duration: "+duration);
			
			multipartEntity.addPart("auth_token", new StringBody(auth_token));
			multipartEntity.addPart("sid", new StringBody(sid));
			multipartEntity.addPart("duration", new StringBody(duration));
			multipartEntity.addPart("upload_file", new FileBody(upload_file));
			httppost.setEntity(multipartEntity);
			
			DebugLog.logd("register: {API: " + httppost.getURI()  + " }");

			execute = mHttpClient.execute(httppost,
					new ResponseHandler<String>() {
						@Override
						public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
							HttpEntity r_entity = response.getEntity();
							String responseString = EntityUtils.toString(r_entity);
							return responseString;
						}
					});
			
			JSONObject jsonResult = new JSONObject(execute);
			if (jsonResult != null) {
				DebugLog.logd("uploadFile result: " + jsonResult.toString());
			}
			
			return jsonResult;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
//	/* Get Session Details V2 FUNCTIONS */
//	public JSONObject getSessionDetailsV2(String auth_token, String sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_GET_SESSION_DETAILS_V2);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Get session details V2 error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "API Get Session Details V2 : " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//			
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "Get session details V2 with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "Get session details V2 with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "Get session details V2 with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Get Session Details FUNCTIONS */
//	public JSONObject getSessionDetails(String auth_token, int sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_GET_SESSION_DETAILS);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Get session details error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "API Get Session Details : " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "Get session details with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "Get session details with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "Get session details with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Get User Channel FUNCTIONS */
//	public JSONObject getUsersChannel(String auth_token, int page, int size, int uid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_GET_USER_CHANNEL);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Get user channel error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("page", page);
//			jsonParam.put("size", size);
//			jsonParam.put("uid", uid);
//			
//			Log.i(LOG_TAG, "API Get User Channel : " + auth_token + ";" + page + ";" 
//					+ size + ";" + uid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "Get user channel with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "Get user channel  with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "Get user channel with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Join A Session FUNCTIONS */
//	public JSONObject joinSession(String auth_token, int sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_JOIN_SESSION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Join session error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "API Join Session : " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Join Session with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Join Session with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Join Session with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//
//	/* Like session FUNCTIONS */
//	public JSONObject likeSession(String auth_token, int sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LIKE_SESSION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Like session error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "API Like Session : " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Like Session with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Like Session with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Like Session with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Like session by newest FUNCTIONS */
//	public JSONObject likeSessionByNewest(String auth_token, int page, int size) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LIKE_SESSION_BY_NEWEST);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Like session by newest error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("page", page);
//			jsonParam.put("size", size);
//			
//			Log.i(LOG_TAG, "API Like Session by newest: " + auth_token + ";" + page + ";" + size);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Like Session by newest with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Like Session by newest with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Like Session by newest with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//		
//	/* Like session by most heard FUNCTIONS */
//	public JSONObject likeSessionByMostHeard(String auth_token, int page, int size) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LIKE_SESSION_BY_MOST_HEARD);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Like session by most heard error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("page", page);
//			jsonParam.put("size", size);
//			
//			Log.i(LOG_TAG, "API Like Session by most heard: " + auth_token + 
//					";" + page + ";" + size);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Like Session by most heard with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Like Session by most heard with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Like Session by most heard with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Like session by song name FUNCTIONS */
//	public JSONObject likeSessionBySongName(String auth_token, int page, int size) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LIKE_SESSION_BY_SONG_NAME);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Like session by song name error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("page", page);
//			jsonParam.put("size", size);
//			
//			Log.i(LOG_TAG, "API Like Session by song name: " + auth_token + 
//					";" + page + ";" + size);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Like Session by song name with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Like Session by song name with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Like Session by song name with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//		
//	/* Like session by user name FUNCTIONS */
//	public JSONObject likeSessionByUserName(String auth_token, int page, int size) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LIKE_SESSION_BY_USERNAME);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Like session by user name error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("page", page);
//			jsonParam.put("size", size);
//			
//			Log.i(LOG_TAG, "API Like Session by user name: " + auth_token + 
//					";" + page + ";" + size);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Like Session by user name with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Like Session by user name with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Like Session by user name with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Login Facebook FUNCTIONS */
//	public JSONObject loginFacebook(String access_token, String role) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LOGIN_FACEBOOK);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Login facebook error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("access_token", access_token);
//			jsonParam.put("role", role);
//			
//			Log.i(LOG_TAG, "API Login facebook: " + access_token + ";" + role);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Login facebook with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Login facebook with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Login facebook with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Login FUNCTIONS */
//	public JSONObject login(String email, String password) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LOGIN);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Login error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			
//			String jsonContent = 
//					"{ \"email\" : \"" + email + "\",\"password\":\"" + password + "\"}";
//			
//			jsonParam.put("user", jsonContent);
//			
//			Log.i(LOG_TAG, "API Login: " + jsonContent);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Login with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Login with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Login with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Logout FUNCTIONS */
//	public JSONObject logout(String auth_token) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LOGOUT);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Logout error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			
//			Log.i(LOG_TAG, "API Logout: " + auth_token);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Logout with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Logout with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Logout with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Registration FUNCTIONS */
//	
//	
//
//	
//	
//	
//	
//	
//	/* Update Listener Count FUNCTIONS */
//	public JSONObject UpdateListenerCount(String auth_token, int sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_UPDATE_LISTERNER_COUNT);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Update listener count error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "API Update listener count: " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Update listener count with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Update listener count with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Login facebook with exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Update Lyrics FUNCTIONS */
//	public JSONObject UpdateLyrics(String auth_token, int sid, String lyric_title, String lyric) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_UPDATE_LYRICS);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Update lyrics error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			jsonParam.put("lyric_title", lyric_title);
//			jsonParam.put("lyric", lyric);
//			
//			Log.i(LOG_TAG, "API Update lyrics: " + auth_token + ";" + sid + ";" 
//					+ lyric_title + ";" + lyric);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Update lyrics with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Update lyrics with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API lyrics exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Update Session FUNCTIONS */
//	public JSONObject UpdateSession(String auth_token, int sid, int duration, String upload_file) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_UPDATE_SESSION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Update session error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			jsonParam.put("duration", duration);
//			jsonParam.put("upload_file", upload_file);
//			
//			Log.i(LOG_TAG, "API Update session: " + auth_token + ";" + sid + ";" 
//					+ duration + ";" + upload_file);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Update session with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Update session with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API session exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Update User Description FUNCTIONS */
//	public JSONObject UpdateUserDescription(String auth_token, String description) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_UPDATE_DESCRIPTION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Update description error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			String userContent = "{\"description\": \"" + description + "\"}";
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("user", userContent);
//			
//			Log.i(LOG_TAG, "API Update description: " + auth_token + ";" + userContent);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Update description with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Update description with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API update description exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Update lock session FUNCTIONS */
//	public JSONObject lockSession(String auth_token, String sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_LOCK_SESSION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Lock sesion error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "Lock Session: " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Lock Session with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Lock Session with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Lock Session exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
//	
//	/* Update unlock session FUNCTIONS */
//	public JSONObject unlockSession(String auth_token, String sid) {
//		HttpURLConnection urlc = null;
//
//		try {
//			urlc = createConnectionWithURL(API_UNLOCK_SESSION);
//			if (urlc == null) {
//				Log.e(LOG_TAG, "Unlock sesion error when connection");
//				return null;
//			}
//			configURLConnection(urlc);
//
//			JSONObject jsonParam = new JSONObject();
//			jsonParam.put("auth_token", auth_token);
//			jsonParam.put("sid", sid);
//			
//			Log.i(LOG_TAG, "Unlock Session: " + auth_token + ";" + sid);
//
//			// write parameter to request
//			writeParamAPI(urlc, jsonParam);
//
//			JSONObject result = getResponseData(urlc);
//			if (result != null) {
//				Log.i(LOG_TAG, "API Unlock Session with result: " + result.toString());
//			} else {
//				Log.e(LOG_TAG, "API Unlock Session with null result: ");
//			}
//
//			return result;
//		} catch (Exception ex) {
//			Log.e(LOG_TAG, "API Unlock Session exception result: " + 
//					ex.getMessage().toString());
//		} finally {
//			if (urlc != null)
//				urlc.disconnect();
//		}
//
//		return null;
//	}
}
