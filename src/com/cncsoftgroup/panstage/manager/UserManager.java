package com.cncsoftgroup.panstage.manager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.service.NetworkService;
import com.nmd.libs.DebugLog;

@SuppressWarnings("unused")
public class UserManager {
	private String tag_register = "tag_register";
	private String tag_login = "tag_login";
	private String tag_login_facebook = "tag_login_facebook";
	private String tag_logout = "tag_logout";

	public interface OnLoginAndRegisterResult {
		void onLoginAndRegisterMethod(boolean isSuccess);
	}

	OnLoginAndRegisterResult callbackOnLoginAndRegisterResult;

	public void register_NetworkService(String full_name, String email, String password, String role, final String avatar,
			OnLoginAndRegisterResult onLoginAndRegisterResult) {
		callbackOnLoginAndRegisterResult = onLoginAndRegisterResult;

		final NetworkService service = new NetworkService();
		new AsyncTask<String, String, JSONObject>() {

			@Override
			protected JSONObject doInBackground(String... params) {
				JSONObject json =new JSONObject();
				if(avatar.equals("")){
					json = service.registration(params[0], params[1], params[2], params[3]);
				}else{
					json = service.registration(params[0], params[1], params[2], params[3], new File(params[4])); 					
				}
				return json;
			}

			protected void onPostExecute(JSONObject result) {
				if (result != null) {
					try {
						boolean success = result.getBoolean("success");
						if (success) {

							JSONObject data = result.getJSONObject("data");
							String auth_token, full_name, avatar;
							int uid;

							if (data.has("auth_token")) {
								auth_token = data.getString("auth_token");
								CacheData.getInstant()
										.setAuth_token(auth_token);
							}

							if (data.has("uid")) {
								uid = data.getInt("uid");
								CacheData.getInstant().setUid(uid);
							}

							if (data.has("full_name")) {
								full_name = data.getString("full_name");
								CacheData.getInstant().setFull_name(full_name);
							}
							
							if (data.has("avatar_url")) {
								avatar = data.getString("avatar_url");
								CacheData.getInstant().setAvatar(avatar);
							}

							callbackOnLoginAndRegisterResult
									.onLoginAndRegisterMethod(true);
						} else {
							CacheData.getInstant().setMessage(result.getString("data"));
							callbackOnLoginAndRegisterResult
									.onLoginAndRegisterMethod(false);
						}
					} catch (JSONException exception) {
						exception.printStackTrace();
						callbackOnLoginAndRegisterResult
								.onLoginAndRegisterMethod(false);
					}
				} else {
					DebugLog.logd("result return null");
					callbackOnLoginAndRegisterResult
							.onLoginAndRegisterMethod(false);
				}
			}
		}.execute(full_name, email, password, role, avatar);

	}

	public void login(String email, String password,
			OnLoginAndRegisterResult onLoginAndRegisterResult) {

		callbackOnLoginAndRegisterResult = onLoginAndRegisterResult;

		JSONObject jsonParam = new JSONObject();
		JSONObject jsonContent = new JSONObject();

		try {
			jsonContent.put("email", email);
			jsonContent.put("password", password);

			jsonParam.put("user", jsonContent);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST,
				Url.API_LOGIN, jsonParam, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						DebugLog.logd("response: " + response.toString());

						try {
							boolean success = response.getBoolean("success");
							if (success) {

								JSONObject data = response
										.getJSONObject("data");
								String auth_token, full_name, role, avatar, role_description;
								int uid;

								if (data.has("auth_token")) {
									auth_token = data.getString("auth_token");
									CacheData.getInstant().setAuth_token(auth_token);
								}

								if (data.has("uid")) {
									uid = data.getInt("uid");
									CacheData.getInstant().setUid(uid);
								}

								if (data.has("full_name")) {
									full_name = data.getString("full_name");
									CacheData.getInstant().setFull_name(full_name);
								}
								if (data.has("role")) {
									role = data.getString("role");
									CacheData.getInstant().setRole(role);
								}
								if (data.has("role_description")) {
									role_description = data
											.getString("role_description");
									CacheData.getInstant().setRole_description(
											role_description);
								}
								if (data.has("avatar_url")) {
									avatar = data.getString("avatar_url");
									CacheData.getInstant().setAvatar(avatar);
								}
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(true);
							} else {
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(false);
							}
						} catch (JSONException e) {
							e.printStackTrace();
							callbackOnLoginAndRegisterResult
									.onLoginAndRegisterMethod(false);
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						DebugLog.loge("response: " + error.getMessage());
						callbackOnLoginAndRegisterResult
								.onLoginAndRegisterMethod(false);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_login);
	}

	public void login_facebook(String access_token, String role,
			OnLoginAndRegisterResult onLoginAndRegisterResult) {

		callbackOnLoginAndRegisterResult = onLoginAndRegisterResult;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("access_token", access_token);
			jsonParam.put("role", role);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST,
				Url.API_LOGIN_FACEBOOK, jsonParam,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						DebugLog.logd("response: " + response.toString());

						try {
							boolean success = response.getBoolean("success");
							if (success) {

								JSONObject data = response
										.getJSONObject("data");
								String auth_token, full_name, role, avatar, role_description;
								int uid;

								if (data.has("auth_token")) {
									auth_token = data.getString("auth_token");
									CacheData.getInstant().setAuth_token(
											auth_token);
								}

								if (data.has("uid")) {
									uid = data.getInt("uid");
									CacheData.getInstant().setUid(uid);
								}

								if (data.has("full_name")) {
									full_name = data.getString("full_name");
									CacheData.getInstant().setFull_name(
											full_name);
								}
								if (data.has("role")) {
									role = data.getString("role");
									CacheData.getInstant().setRole(role);
								}
								if (data.has("role_description")) {
									role_description = data
											.getString("role_description");
									CacheData.getInstant().setRole_description(
											role_description);
								}
								if (data.has("avatar_url")) {
									avatar = data.getString("avatar_url");
									CacheData.getInstant().setAvatar(avatar);
								}
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(true);
							} else {
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(false);
							}
						} catch (JSONException e) {
							e.printStackTrace();
							callbackOnLoginAndRegisterResult
									.onLoginAndRegisterMethod(false);
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						DebugLog.loge("response: " + error.getMessage());
						callbackOnLoginAndRegisterResult
								.onLoginAndRegisterMethod(false);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq,
				tag_login_facebook);
	}

	public void logout(String auth_token,
			OnLoginAndRegisterResult onLoginAndRegisterResult) {

		callbackOnLoginAndRegisterResult = onLoginAndRegisterResult;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("auth_token", auth_token);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.DELETE,
				Url.API_LOGOUT, jsonParam, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						DebugLog.logd("response: " + response.toString());

						try {
							boolean success = response.getBoolean("success");
							if (success) {
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(true);
							} else {
								callbackOnLoginAndRegisterResult
										.onLoginAndRegisterMethod(false);
							}
						} catch (JSONException e) {
							e.printStackTrace();
							callbackOnLoginAndRegisterResult
									.onLoginAndRegisterMethod(false);
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						DebugLog.loge("response: " + error.getMessage());
						callbackOnLoginAndRegisterResult
								.onLoginAndRegisterMethod(false);
					}
				}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_logout);
	}

}
