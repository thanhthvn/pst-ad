package com.cncsoftgroup.panstage.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cncsoftgroup.panstage.bean.Session;
import com.cncsoftgroup.panstage.bean.SessionDetail;
import com.cncsoftgroup.panstage.common.AppController;
import com.cncsoftgroup.panstage.common.CacheData;
import com.cncsoftgroup.panstage.common.Url;
import com.cncsoftgroup.panstage.service.NetworkService;
import com.nmd.libs.DebugLog;

public class SessionManager {

	public interface OnSessionResult {
		void onSessionMethod(boolean isSuccess, ArrayList<Session> arraylist);
	}

	OnSessionResult callbackOnSessionResult;

	public interface OnCreateSessionResult {
		void onCreateSessionMethod(boolean isSuccess, int song_id);
	}
	OnCreateSessionResult callbackOnCreateSessionResult;

	private String tag_get_list_session = "tag_get_list_session";
	private String tag_create_session = "tag_create_session";

	public void getListSession(String auth_token, int page, int size, String url, OnSessionResult onSessionResult) {

		callbackOnSessionResult = onSessionResult;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("auth_token", auth_token);
			jsonParam.put("page", page);
			jsonParam.put("size", size);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, url, jsonParam, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.logd("response: " + response.toString());

				try {
					boolean success = response.getBoolean("success");
					if (success) {

						JSONArray jsonArray = response.getJSONArray("data");
						ArrayList<Session> arraylist = convertListFromJson(jsonArray);

						if (arraylist != null) {
							callbackOnSessionResult.onSessionMethod(true, arraylist);
						} else {
							callbackOnSessionResult.onSessionMethod(false, null);
						}

					} else {
						callbackOnSessionResult.onSessionMethod(false, null);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnSessionResult.onSessionMethod(false, null);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// DebugLog.logd("response: " + error.getMessage());
				callbackOnSessionResult.onSessionMethod(false, null);
			}
		}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_get_list_session);
	}

	public static Session convertItemInfoFromJson(JSONObject tmpObject) {
		Session obj = new Session();
		try {
			if (tmpObject.has("id")) {
				obj.setSid(tmpObject.getInt("id"));
			}
			if (tmpObject.has("creator_user_id")) {
				obj.setCreator_user_id(tmpObject.getInt("creator_user_id"));
			}
			if (tmpObject.has("song_name")) {
				obj.setSong_name(tmpObject.getString("song_name"));
			}
			if (tmpObject.has("lyric_title")) {
				obj.setLyric_title(tmpObject.getString("lyric_title"));
			}
			if (tmpObject.has("listeners")) {
				obj.setListeners(tmpObject.getInt("listeners"));
			}
			if (tmpObject.has("creator_full_name")) {
				obj.setCreator_full_name(tmpObject.getString("creator_full_name"));
			}
			if (tmpObject.has("likes")) {
				obj.setLikes(tmpObject.getInt("likes"));
			}
			if (tmpObject.has("is_like")) {
				obj.setIs_like(tmpObject.getBoolean("is_like"));
			}
			if (tmpObject.has("duration")) {
				obj.setDuration(tmpObject.getInt("duration"));
			}
			if (tmpObject.getJSONObject("creator_user").has("avatar_url")) {
				obj.setCreator_user_avatar(tmpObject.getJSONObject("creator_user").getString("avatar_url"));
			}
			if (tmpObject.has("locked")) {
				obj.setLocked(tmpObject.getBoolean("locked"));
			}
			if (tmpObject.has("lyric")) {
				obj.setLyric(tmpObject.getString("lyric"));
			}
			if (tmpObject.getJSONObject("creator_user").has("role")) {
				obj.setCreator_user_role(tmpObject.getJSONObject("creator_user").getString("role"));
			}
			if (tmpObject.getJSONObject("creator_user").has("role_description")) {
				obj.setCreator_user_role_description(tmpObject.getJSONObject("creator_user").getString("role_description"));
			}
			if (tmpObject.getJSONObject("upload").has("url")) {
				obj.setUpload_url(tmpObject.getJSONObject("upload").getString("url"));
			}
		} catch (Exception exception) {
			DebugLog.loge("Get list  exeption: " + exception.getMessage());
		}
		return obj;
	}

	public static ArrayList<Session> convertListFromJson(JSONArray jsonArray) {
		ArrayList<Session> resultList = new ArrayList<Session>();
		Session obj;
		JSONObject tmpObject;

		int size = jsonArray.length();
		for (int i = 0; i < size; i++) {
			try {
				tmpObject = jsonArray.getJSONObject(i);
				obj = convertItemInfoFromJson(tmpObject);
				resultList.add(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}

	public void createSession(String auth_token, String session_name, int duration, String upload_file, String lyric_title, String lyric, OnCreateSessionResult onCreateSessionResult) {

		callbackOnCreateSessionResult = onCreateSessionResult;

		JSONObject jsonParam = new JSONObject();

		File file = new File(upload_file);
		FileBody fileBody = new FileBody(file);
		try {
			jsonParam.put("auth_token", auth_token);
			jsonParam.put("session_name", session_name);
			jsonParam.put("duration", duration);
			jsonParam.put("upload_file", fileBody);
			jsonParam.put("lyric", lyric);
			jsonParam.put("lyric_title", lyric_title);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		DebugLog.loge("param: " + jsonParam);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, Url.API_CREATE_SESSION, jsonParam, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.loge("response: " + response.toString());

				try {
					boolean success = response.getBoolean("success");
					if (success) {

						JSONObject data = response.getJSONObject("data");
						@SuppressWarnings("unused")
						String auth_token;
						@SuppressWarnings("unused")
						int uid, id = -1;

						if (data.has("auth_token")) {
							auth_token = data.getString("auth_token");
						}

						if (data.has("uid")) {
							uid = data.getInt("uid");
						}

						if (data.has("id")) {
							id = data.getInt("id");
						}
						callbackOnCreateSessionResult.onCreateSessionMethod(true, id);
					} else {
						callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				DebugLog.logd("response: " + error.getMessage());
				callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
			}
		}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_create_session);
	}

	// Get list user chanel
	public interface OnChanelUserSession {
		void onChanelUserMethod(boolean isSuccess, ArrayList<Session> arrayList);
	}

	OnChanelUserSession callbackChanelUserSessionResult;
	private String tag_get_list_user_chanel = "tag_get_list_user_chanel";

	public void getChanelUserSession(String auth_token, int page, int size, int uid, String url, OnChanelUserSession onChanelUserSessionResult) {

		callbackChanelUserSessionResult = onChanelUserSessionResult;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("auth_token", auth_token);
			jsonParam.put("page", page);
			jsonParam.put("size", size);
			jsonParam.put("uid", uid);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest joRequest = new JsonObjectRequest(Method.POST, url, jsonParam, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.logd("Response: " + response.toString());

				try {
					boolean success = response.getBoolean("success");
					if (success) {
						JSONArray jsonArray = response.getJSONArray("data");
						ArrayList<Session> arrayList = convertListFromJson(jsonArray);

						if (arrayList != null) {
							callbackChanelUserSessionResult.onChanelUserMethod(true, arrayList);
						} else {
							callbackChanelUserSessionResult.onChanelUserMethod(false, null);
						}
					} else {
						callbackChanelUserSessionResult.onChanelUserMethod(false, null);
					}

				} catch (JSONException e) {
					e.printStackTrace();
					callbackChanelUserSessionResult.onChanelUserMethod(false, null);
				}

			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				DebugLog.logd("Response: " + error.getMessage());
				callbackChanelUserSessionResult.onChanelUserMethod(false, null);
			}

		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}

		};
		AppController.getInstance().addToRequestQueue(joRequest, tag_get_list_user_chanel);
	}

	public void createSessionRecord(String auth_token, String session_name, String duration, String upload_file, String lyric, String lyric_title, OnCreateSessionResult onCreateSessionResult) {

		callbackOnCreateSessionResult = onCreateSessionResult;

		final NetworkService service = new NetworkService();
		new AsyncTask<String, String, JSONObject>() {

			@Override
			protected JSONObject doInBackground(String... params) {
				return service.createSession(params[0], params[1], params[2], new File(params[3]), params[4], params[5]);
			}

			protected void onPostExecute(JSONObject result) {
				if (result != null) {
					try {
						boolean success = result.getBoolean("success");
						if (success) {
							DebugLog.loge("result: " + result);
							CacheData.getInstant().setUrl_upload(result.getJSONObject("data").getJSONObject("upload").getString("url"));
							callbackOnCreateSessionResult.onCreateSessionMethod(true, result.getJSONObject("data").getInt("id"));
						} else {
							callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
						}
					} catch (JSONException exception) {
						exception.printStackTrace();
						callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
					}
				} else {
					DebugLog.logd("result return null");
					callbackOnCreateSessionResult.onCreateSessionMethod(false, -1);
				}
			}
		}.execute(auth_token, session_name, duration, upload_file, lyric, lyric_title);
	}

	// update session
	public interface OnUpdateSession {
		void OnUpdateSessionMethod(boolean success);
	}

	OnUpdateSession callbackOnUpdateSessionResult;

	public void updateSessionRecord(String auth_token, String sid, String duration, String upload_file, OnUpdateSession onUpdateSessionResult) {

		callbackOnUpdateSessionResult = onUpdateSessionResult;

		final NetworkService service = new NetworkService();
		new AsyncTask<String, String, JSONObject>() {

			@Override
			protected JSONObject doInBackground(String... params) {
				return service.updateSession(params[0], params[1], params[2], new File(params[3]));
			}

			protected void onPostExecute(JSONObject result) {
				if (result != null) {
					try {
						boolean success = result.getBoolean("success");
						if (success) {

							callbackOnUpdateSessionResult.OnUpdateSessionMethod(true);
						} else {
							callbackOnUpdateSessionResult.OnUpdateSessionMethod(false);
						}
					} catch (JSONException exception) {
						exception.printStackTrace();
						callbackOnUpdateSessionResult.OnUpdateSessionMethod(false);
					}
				} else {
					DebugLog.logd("result return null");
					callbackOnUpdateSessionResult.OnUpdateSessionMethod(false);
				}
			}
		}.execute(auth_token, sid, duration, upload_file);
	}

	// update Lyric

	public interface OnLyricSession {
		void OnUpdateLyricMethod(boolean success);
	}

	OnLyricSession callbackOnLyricSessionResult;
	private String tag_update_lyric_session = "tag_update_lyric_session";

	public void updateLyricRecord(String auth_token, String sid, String lyric_title, String lyric, OnLyricSession onLyricSessionResult) {

		callbackOnLyricSessionResult = onLyricSessionResult;

		JSONObject jsonParam = new JSONObject();

		try {
			jsonParam.put("auth_token", auth_token);
			jsonParam.put("sid", sid);
			jsonParam.put("lyric_title", lyric_title);
			jsonParam.put("lyric", lyric);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST, Url.API_UPDATE_LYRICS, jsonParam, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.logd("response: " + response.toString());

				try {
					boolean success = response.getBoolean("success");
					if (success) {
						callbackOnLyricSessionResult.OnUpdateLyricMethod(true);
					} else {
						callbackOnLyricSessionResult.OnUpdateLyricMethod(false);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnLyricSessionResult.OnUpdateLyricMethod(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				DebugLog.loge("response: " + error.getMessage());
				callbackOnLyricSessionResult.OnUpdateLyricMethod(false);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};

		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_update_lyric_session);
	}

	// Like Session
	public interface OnUserLikeSession {
		void OnUserLikeSessionMethod(boolean success);
	}

	OnUserLikeSession callbackOnUserLikeSessionResult;
	private String tag_like_session = "tag_like_session";

	public void Like_Join_Lock_Session(String auth_token, int sid, String url, OnUserLikeSession onUserLikeSessionResult) {
		callbackOnUserLikeSessionResult = onUserLikeSessionResult;

		JSONObject jsonParams = new JSONObject();

		try {
			jsonParams.put("auth_token", auth_token);
			jsonParams.put("sid", sid);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest joRequest = new JsonObjectRequest(Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				DebugLog.logd("Response: " + response.toString());
				try {
					boolean success = response.getBoolean("success");

					if (success) {
						callbackOnUserLikeSessionResult.OnUserLikeSessionMethod(true);
					} else {
						callbackOnUserLikeSessionResult.OnUserLikeSessionMethod(false);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnUserLikeSessionResult.OnUserLikeSessionMethod(false);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				DebugLog.logd("Response: " + error.getMessage());
				callbackOnUserLikeSessionResult.OnUserLikeSessionMethod(false);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return headers;
			}
		};
		AppController.getInstance().addToRequestQueue(joRequest, tag_like_session);
	}

	// Get session details
	public interface OnSessionDetails {
		void OnSessionDetailsMethod(boolean isSuccess, Session session);
	}

	OnSessionDetails callbackOnSessionDetailsResult;
	private String tag_get_session_details = "tag_get_session_details";

	public void getSessionDetails(String auth_token, int sid, String url, OnSessionDetails onSessionDetailsResults) {
		callbackOnSessionDetailsResult = onSessionDetailsResults;

		JSONObject jsonParams = new JSONObject();

		try {
			jsonParams.put("auth_token", auth_token);
			jsonParams.put("sid", sid);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonRequest = new JsonObjectRequest(Method.POST, url, jsonParams, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				try {
					boolean success = response.getBoolean("success");
					if (success) {
						JSONObject jObject = response.getJSONObject("data");
						JSONObject jObject2 = jObject.getJSONObject("currentSession");
						Session session = convertSessionDetailsFromJson(jObject2);

						if (session != null) {
							callbackOnSessionDetailsResult.OnSessionDetailsMethod(true, session);
						} else {
							callbackOnSessionDetailsResult.OnSessionDetailsMethod(false, null);
						}
					} else {
						callbackOnSessionDetailsResult.OnSessionDetailsMethod(false, null);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					callbackOnSessionDetailsResult.OnSessionDetailsMethod(false, null);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				callbackOnSessionDetailsResult.OnSessionDetailsMethod(false, null);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return super.getHeaders();
			}
		};
		AppController.getInstance().addToRequestQueue(jsonRequest, tag_get_session_details);

	}

	public static Session convertSessionDetailsFromJson(JSONObject tmpObject) {
		Session obj = new Session();
		try {
			if (tmpObject.has("id")) {
				obj.setSid(tmpObject.getInt("id"));
			}
			if (tmpObject.has("creator_user_id")) {
				obj.setCreator_user_id(tmpObject.getInt("creator_user_id"));
			}
			if (tmpObject.has("song_name")) {
				obj.setSong_name(tmpObject.getString("song_name"));
			}
			if (tmpObject.has("lyric_title")) {
				obj.setLyric_title(tmpObject.getString("lyric_title"));
			}
			if (tmpObject.has("listeners")) {
				obj.setListeners(tmpObject.getInt("listeners"));
			}
			if (tmpObject.has("creator_full_name")) {
				obj.setCreator_full_name(tmpObject.getString("creator_full_name"));
			}
			if (tmpObject.has("likes")) {
				obj.setLikes(tmpObject.getInt("likes"));
			}
			if (tmpObject.has("duration")) {
				obj.setDuration(tmpObject.getInt("duration"));
			}
			if (tmpObject.getJSONObject("creator_user").has("avatar_url")) {
				obj.setCreator_user_avatar(tmpObject.getJSONObject("creator_user").getString("avatar_url"));
			}
			if (tmpObject.has("locked")) {
				obj.setLocked(tmpObject.getBoolean("locked"));
			}
			if (tmpObject.has("lyric")) {
				obj.setLyric(tmpObject.getString("lyric"));
			}
			if (tmpObject.getJSONObject("creator_user").has("role")) {
				obj.setCreator_user_role(tmpObject.getJSONObject("creator_user").getString("role"));
			}
			if (tmpObject.getJSONObject("creator_user").has("role_description")) {
				obj.setCreator_user_role_description(tmpObject.getJSONObject("creator_user").getString("role_description"));
			}
			if (tmpObject.getJSONObject("upload").has("url")) {
				obj.setUpload_url(tmpObject.getJSONObject("upload").getString("url"));
			}
		} catch (Exception exception) {
			DebugLog.loge("Get list  exeption: " + exception.getMessage());
		}
		return obj;
	}

	public static Session convertSessionJoin(JSONObject tmpObject) {
		Session obj = new Session();
		try {
			if (tmpObject.has("uid")) {
				obj.setUid(tmpObject.getInt("uid"));
			}
			if (tmpObject.has("full_name")) {
				obj.setFull_name(tmpObject.getString("full_name"));
			}
			if (tmpObject.has("song_name")) {
				obj.setSong_name(tmpObject.getString("song_name"));
			}
			if (tmpObject.has("email")) {
				obj.setEmail(tmpObject.getString("email"));
			}
			if (tmpObject.has("role")) {
				obj.setRole(tmpObject.getString("role"));
			}
			if (tmpObject.has("role_description")) {
				obj.setRole_description(tmpObject.getString("role_description"));
			}
			if (tmpObject.has("avatar_url")) {
				obj.setAvatar_url(tmpObject.getString("avatar_url"));
			}
		} catch (Exception exception) {
			DebugLog.loge("Get list  exeption: " + exception.getMessage());
		}
		return obj;
	}

	public static ArrayList<Session> convertListFromJoin(JSONArray jsonArray) {
		ArrayList<Session> resultList = new ArrayList<Session>();
		Session obj;
		JSONObject tmpObject;

		int size = jsonArray.length();
		for (int i = 0; i < size; i++) {
			try {
				tmpObject = jsonArray.getJSONObject(i);
				obj = convertSessionJoin(tmpObject);
				resultList.add(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}

	public interface OnJoinSessionResult {
		void OnJoinSessionResultMethod(boolean isSuccess, ArrayList<Session> arraylist, SessionDetail obj);
	}
	OnJoinSessionResult OnBackOnJoinSessionResult;

	private String tag_get_join_session = "tag_get_join_session";

	public void getJoinSessionDetails(String auth_token, int sid, OnJoinSessionResult onJoinSessionResult) {
		OnBackOnJoinSessionResult = onJoinSessionResult;

		JSONObject jsonParams = new JSONObject();

		try {
			jsonParams.put("auth_token", auth_token);
			jsonParams.put("sid", sid);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest jsonRequest = new JsonObjectRequest(Method.POST, Url.API_GET_SESSION_DETAILS_V2, jsonParams, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				try {
					boolean success = response.getBoolean("success");
					if (success) {

						JSONObject jsonData = response.getJSONObject("data");
						JSONObject currentSession = jsonData.getJSONObject("currentSession");

						JSONObject jsonCreatorUser = currentSession.getJSONObject("creator_user");
						JSONArray jsonArray = currentSession.getJSONArray("join_user");

						ArrayList<Session> arraylist = convertListFromJoin(jsonArray);
						SessionDetail obj = new SessionDetail();
						if (currentSession.has("locked")) {
							obj.setLocked(currentSession.getBoolean("locked"));
						}
						if (currentSession.has("lyric_title")) {
							obj.setLyric_title(currentSession.getString("lyric_title"));
						}
						if (currentSession.has("lyric")) {
							obj.setLyric(currentSession.getString("lyric"));
						}

						if (jsonCreatorUser.has("uid")) {
							obj.setCreate_user_uid(jsonCreatorUser.getInt("uid"));
						}
						if (jsonCreatorUser.has("full_name")) {
							obj.setCreate_user_full_name(jsonCreatorUser.getString("full_name"));
						}
						if (jsonCreatorUser.has("email")) {
							obj.setCreate_user_email(jsonCreatorUser.getString("email"));
						}
						if (jsonCreatorUser.has("role")) {
							obj.setCreate_user_role(jsonCreatorUser.getString("role"));
						}
						if (jsonCreatorUser.has("role_description")) {
							obj.setCreate_user_role_description(jsonCreatorUser.getString("role_description"));
						}
						if (jsonCreatorUser.has("avatar_url")) {
							obj.setCreate_user_avatar_url(jsonCreatorUser.getString("avatar_url"));
						}

						if (arraylist != null) {
							OnBackOnJoinSessionResult.OnJoinSessionResultMethod(true, arraylist, obj);
						} else {
							OnBackOnJoinSessionResult.OnJoinSessionResultMethod(true, null, obj);
						}

					} else {
						OnBackOnJoinSessionResult.OnJoinSessionResultMethod(false, null, null);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					OnBackOnJoinSessionResult.OnJoinSessionResultMethod(false, null, null);
				}
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				OnBackOnJoinSessionResult.OnJoinSessionResultMethod(false, null, null);
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> headers = new HashMap<String, String>();
				headers.put("Content-Type", "application/json");
				return super.getHeaders();
			}
		};
		AppController.getInstance().addToRequestQueue(jsonRequest, tag_get_join_session);

	}
}
