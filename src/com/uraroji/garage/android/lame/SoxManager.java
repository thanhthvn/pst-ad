package com.uraroji.garage.android.lame;

import java.io.File;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;

import com.cncsoftgroup.panstage.common.Utils;
import com.nmd.libs.DebugLog;
import com.uraroji.garage.android.lame.ShellUtils.ShellCallback;

public class SoxManager {
	public interface OnConvertFileResult {
		void onConvertFileMethod(boolean isSuccess, String message);
	}

	static OnConvertFileResult callbackOnConvertFileResult;

	public static void convertFile(Context context, String input, String output, String temp, final OnConvertFileResult callback) {
		callbackOnConvertFileResult = callback;
		try {
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= 21) {
				SoxControllerLP controllerLP = new SoxControllerLP(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onConvertFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onConvertFileMethod(false, "error " + exitValue);
						}
					}
				});
				controllerLP.convertFile(input, output, temp);
			}else{
				SoxController soxController = new SoxController(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onConvertFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onConvertFileMethod(false, "error " + exitValue);
						}
					}
				});
				soxController.convertFile(input, output, temp);
			}
		} catch (Exception e) {
			callback.onConvertFileMethod(false, "error ");
		}
	}
	
	//TODO
	public interface OnConvertChFileResult {
		void onConvertChFileMethod(boolean isSuccess, String message);
	}

	static OnConvertChFileResult callbackOnConvertChFileResult;

	public static void convertChFile(Context context, String input, String output, String lenth, final OnConvertChFileResult callback) {
		callbackOnConvertChFileResult = callback;
		try {
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= 21) {
				SoxControllerLP controllerLP = new SoxControllerLP(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onConvertChFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onConvertChFileMethod(false, "error " + exitValue);
						}
					}
				});
				controllerLP.convertChFile(input, output, lenth);
			}else{
				SoxController soxController = new SoxController(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onConvertChFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onConvertChFileMethod(false, "error " + exitValue);
						}
					}
				});
				soxController.convertChFile(input, output, lenth);
			}
		} catch (Exception e) {
			callback.onConvertChFileMethod(false, "error ");
		}
	}
	
	//TODO
	public interface OnCombineMixResult {
		void onCombineMixMethod(boolean isSuccess, String message);
	}

	static OnCombineMixResult callbackOnCombineMixResult;

	public static void combineMix(Context context, List<String> files, String outFile, final OnCombineMixResult callback) {
		callbackOnCombineMixResult = callback;
		try {
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= 21) {
				SoxControllerLP controllerLP = new SoxControllerLP(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onCombineMixMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onCombineMixMethod(false, "error " + exitValue);
						}
					}
				});
				controllerLP.combineMix(files, outFile);
			}else{
				SoxController soxController = new SoxController(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onCombineMixMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onCombineMixMethod(false, "error " + exitValue);
						}
					}
				});
				soxController.combineMix(files, outFile);
			}
		} catch (Exception e) {
			callback.onCombineMixMethod(false, "error ");
		}
	}
	
	//TODO
	public interface OnPadFileResult {
		void onPadFileMethod(boolean isSuccess, String message);
	}

	static OnPadFileResult callbackOnPadFileResult;

	public static void padFile(Context context, String input, String output, String lenth, final OnPadFileResult callback) {
		callbackOnPadFileResult = callback;
		try {
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			if (currentapiVersion >= 21) {
				SoxControllerLP controllerLP = new SoxControllerLP(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onPadFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onPadFileMethod(false, "error " + exitValue);
						}
					}
				});
				controllerLP.padFile(input, output, lenth);
			}else{
				SoxController soxController = new SoxController(context, new File(""), new ShellCallback() {
					
					@Override
					public void shellOut(String shellLine) {
						DebugLog.loge("shellLine:\n" + shellLine);
					}
					
					@Override
					@SuppressLint("UseValueOf")
					public void processComplete(int exitValue) {
						Utils.dismissCurrentDialog();
						DebugLog.loge("exitValue:" + exitValue);
						if (exitValue == 0) {
							callback.onPadFileMethod(true, "");
							return;
						}
						if (exitValue != 0) {
							callback.onPadFileMethod(false, "error " + exitValue);
						}
					}
				});
				soxController.padFile(input, output, lenth);
			}
		} catch (Exception e) {
			callback.onPadFileMethod(false, "error ");
		}
	}
	
	//TODO
}
