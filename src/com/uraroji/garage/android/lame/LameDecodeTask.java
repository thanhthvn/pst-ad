package com.uraroji.garage.android.lame;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.os.AsyncTask;

import com.cncsoftgroup.panstage.activity.R;
import com.nmd.libs.DebugLog;

public class LameDecodeTask extends AsyncTask<String, Void, Void> {
	public interface OnEncodeResult {
		void onEncodeMethod(boolean isSuccess, String message);
	}

	OnEncodeResult callbackOnEncodeResult;
	
	Context context;
    private File input;
    private File output;
    private Decoder lame;
    private int errorCode;

    public LameDecodeTask(Context context, OnEncodeResult onEncodeResult) {
    	this.context = context;
    	callbackOnEncodeResult= onEncodeResult;
        errorCode = 0;
    }

    @Override
    protected void onPreExecute() {
    	
    }

    @Override
    protected Void doInBackground(String... params) {
        input = new File(params[0]);
        output = new File(params[1]);
        lame = new Decoder(input, output);

        try {
            lame.initialize();
        } catch (IOException e) {
            // input is not an mp3 file or could not create file
            errorCode = Constants.LAME_ERROR_INIT_DECODER;
        }
        if (errorCode == 0) {
            try {
                lame.decode();
            } catch (IOException e) {
                // failed to read pcm data/failed to write mp3 data
                errorCode = Constants.LAME_ERROR_DECODE_IO;
            }
        }

        lame.cleanup();
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        switch (errorCode) {
        case 0:
            DebugLog.loge(context.getString(R.string.lame_decode_end_msg));
            callbackOnEncodeResult.onEncodeMethod(true, context.getString(R.string.lame_decode_end_msg));
            break;
        case Constants.LAME_ERROR_INIT_DECODER:
        	DebugLog.loge(context.getString(R.string.lame_decode_error_init_decoder_msg));
        	callbackOnEncodeResult.onEncodeMethod(false, context.getString(R.string.lame_decode_error_init_decoder_msg));
            break;
        case Constants.LAME_ERROR_DECODE_IO:
        	DebugLog.loge(context.getString(R.string.lame_decode_error_decode_io_msg));
        	callbackOnEncodeResult.onEncodeMethod(false, context.getString(R.string.lame_decode_error_decode_io_msg));
            break;
        default:
        }
    }
}