package com.uraroji.garage.android.lame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.sourceforge.lame.Lame;
import android.content.Context;
import android.os.AsyncTask;

import com.cncsoftgroup.panstage.activity.R;
import com.nmd.libs.DebugLog;

public class LameEncodeTask extends AsyncTask<String, Void, Void> {
	public interface OnEncodeResult {
		void onEncodeMethod(boolean isSuccess, String message);
	}

	OnEncodeResult callbackOnEncodeResult;
	
	Context context;
	private File input;
    private File output;
    private Encoder lame;
    private int errorCode;

    public LameEncodeTask(Context context, OnEncodeResult onEncodeResult) {
    	this.context = context;
    	callbackOnEncodeResult= onEncodeResult;
        errorCode = 0;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Void doInBackground(String... params) {
        input = new File(params[0]);
        output = new File(params[1]);
        lame = new Encoder(input, output);

        try {
            lame.initialize();
            lame.setPreset(Lame.LAME_PRESET_STANDARD);
        } catch (FileNotFoundException e) {
            // couldn't create our in/out files
            errorCode = Constants.LAME_ERROR_FILE_CREATE;
        } catch (IOException e) {
            // input is not a wave file
            errorCode = Constants.LAME_ERROR_FILE_TYPE;
        }
        if (errorCode == 0) {
            try {
                lame.encode();
            } catch (IOException e) {
                errorCode = Constants.LAME_ERROR_ENCODE_IO;
            }
        }

        lame.cleanup();
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        switch (errorCode) {
        case 0:
//        	DebugLog.loge(context.getString(R.string.lame_encode_end_msg));
        	callbackOnEncodeResult.onEncodeMethod(true, context.getString(R.string.lame_encode_end_msg));
            break;
        case Constants.LAME_ERROR_FILE_CREATE:
        	DebugLog.loge(context.getString(R.string.lame_encode_error_file_create_msg));
        	callbackOnEncodeResult.onEncodeMethod(false, context.getString(R.string.lame_encode_error_file_create_msg));
            break;
        case Constants.LAME_ERROR_FILE_TYPE:
        	DebugLog.loge(context.getString(R.string.lame_encode_error_file_type_msg));
        	callbackOnEncodeResult.onEncodeMethod(false, context.getString(R.string.lame_encode_error_file_type_msg));
            break;
        case Constants.LAME_ERROR_ENCODE_IO:
        	DebugLog.loge(context.getString(R.string.lame_encode_error_encode_io_msg));
        	callbackOnEncodeResult.onEncodeMethod(false, context.getString(R.string.lame_encode_error_encode_io_msg));
            break;
        default:
        }
    }
}